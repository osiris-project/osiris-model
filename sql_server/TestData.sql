--use zentom_mssql;

DECLARE @employee_id UNIQUEIDENTIFIER;
DECLARE @enterprise_id UNIQUEIDENTIFIER;
DECLARE @tenant_id UNIQUEIDENTIFIER;
DECLARE @sector_id UNIQUEIDENTIFIER;
DECLARE @service_id UNIQUEIDENTIFIER;
DECLARE @person_id UNIQUEIDENTIFIER;
DECLARE @role_id UNIQUEIDENTIFIER;
DECLARE @tmp_id UNIQUEIDENTIFIER;
declare @legal_role_addr_id uniqueidentifier = (select id from organization.address_type where name = 'legal');
declare @serv_recept_addr_id uniqueidentifier = (select id from organization.address_type where name = 'service_reception');
declare @supply_addr_id uniqueidentifier = (select id from organization.address_type where name = 'supply');
declare @address_id uniqueidentifier;
declare @commune_id uniqueidentifier = (SELECT id FROM [location].commune WHERE name = 'Santiago');
declare @account_id uniqueidentifier;
declare @authorized_account_id uniqueidentifier;
declare @module_id uniqueidentifier;

-- #TEST_DATA
-- #Fake

-- Exempt sector = 602220, affect sector = 132010

-- #Proveedor1
EXECUTE organization.add_tenant
	'96950918',
	'Proveedor 1',
	'Proveedor',
	'',
	'',
	@id = @tenant_id out;

----------------- Enabled modules
set @module_id = (select id from system.module where name = 'document_reception');
execute system.add_tenant_module
	@module_id,
	@tenant_id;

set @module_id = (select id from system.module where name = 'document_emission');
execute system.add_tenant_module
	@module_id,
	@tenant_id;

set @module_id = (select id from system.module where name = 'document_reception_trace');
execute system.add_tenant_module
	@module_id,
	@tenant_id;

set @module_id = (select id from system.module where name = 'document_emission_signing');
execute system.add_tenant_module
	@module_id,
	@tenant_id;

set @module_id = (select id from system.module where name = 'accounting_voucher');
exec system.add_tenant_module
	@module_id,
	@tenant_id,
	1,
	'{"data":[{"name":"softlandAux","map_to":"customField1","display":"Softland Aux"},{"name":"softlandDocumentType","map_to":"customField2","display":"Tipo Doc. Softland"},{"name":"softlandRefDocumentType","map_to":"customField3","display":"Tipo Doc. Ref. Softland"}]}',
	'1.0.0';
set @module_id = (select id from system.module where name = 'accounting_voucher_export');
exec system.add_tenant_module
	@module_id,
	@tenant_id,
	1,
	'{"export":{"content_type":"text/plain","charset":"utf-8","format":"taxId;number;date;description;accountingAccount;customField1;documentType;customField2;folioNumber;emissionDate;expiryDate;refDoctype;customField3;refFolioNumber;costsCenter;debt;asset;netAmount;fixedNetAsset;exemptAmount;taxAmount;nrTaxAmount;total;lineDescription"}}',
	'1.0.0';

----------------- Folios -------------------
--execute taxation.set_tenant_folios @tenant_id, '33';
--execute taxation.set_tenant_folios @tenant_id, '34';
--execute taxation.set_tenant_folios @tenant_id, '61';
--execute taxation.set_tenant_folios @tenant_id, '56';
exec taxation.add_folio_range 1, 200, '33', 1, '2016-03-01', @tenant_id;
exec taxation.add_folio_range 1, 200, '34', 1, '2016-03-01', @tenant_id;
exec taxation.add_folio_range 1, 200, '61', 1, '2016-03-01', @tenant_id;
exec taxation.add_folio_range 1, 200, '56', 1, '2016-03-01', @tenant_id;


----------------- Settings -------------------
execute system.add_setting
	'signing_repository_folder_name',
	'Proveedor1',
	@tenant_id;

-- Signing provider setting:
-- provider=...    | Provider name
-- folder_name=... | The folder name under the provider root path.
execute system.add_setting
	'signing_provider',
	'provider=artikos_cl;folder_name=Proveedor1',
	@tenant_id;

execute system.add_setting
	'folio_range_alert',
	'90',
	@tenant_id;

select @tenant_id;

----------------- Address -------------------
execute organization.add_tenant_address
	@tenant_id,
	'Fake street 100, piso 10, oficina 20',
	'',
	@commune_id,
	@id = @address_id out;

execute organization.set_tenant_address_type
	@legal_role_addr_id,
	@address_id;

----------------- User Creation -------------------
EXECUTE system.add_person
	@first_name = 'Juan',
	@last_name = 'Perez',
	@national_id = '146408133',
	@id = @person_id OUT;

execute system.create_account
	'jperez@proveedor1.com',
	'fake@fake.com',
	0x7F074495BA09DABC6D4EEE19220C84BB7AAC86B20B8D94A7BB1AF2B09BBE1D3FF434AD8498255805EF28B7AD4FE75B842F490A915E0E7D8E1E973B2493A31EF8A4C597CCA8F1AEDCF2153A152464F6EB9D5D7A551AF976D5287F8272C2BFAE470A657578B8BD660ACF43D906C36ECE5F69168A83C9980472FD552B008D332B5A,
	0x0001000000FFFFFFFF01000000000000000C0200000054446F62656C696B2E5574696C732E50617373776F72642C2056657273696F6E3D312E302E353732352E32313930392C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D6E756C6C05010000001F446F62656C696B2E5574696C732E50617373776F72642E48617368496E666F0500000002686C02736C026963027373027370000000010008080808020000004000000040000000410000000603000000017C040000000B000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000,
	@person_id,
	@account_id out;

EXECUTE [system].add_role
	'administrator',
	'Administrador',
	'Tiene todos los permisos de la aplicaci�n',
	@tenant_id,
	@id = @role_id OUT;

execute system.authorize_account
	@tenant_id,
	@account_id,
	@authorized_account_id out;

EXECUTE [system].add_account_role
	@authorized_account_id,
	@role_id;

----------------- Comercial Activities --------------
insert into taxation.comercial_activity (name) values ('Tecnolog�a'), ('Agricultura'), ('Transporte'), ('Negocios'), ('Salud');
insert into taxation.tenant_comercial_activity (tenant_id, comercial_activity_id) values
(
	(select id from organization.tenant where tax_id = '96950918'),
	(select id from taxation.comercial_activity where name = 'Agricultura')
);

----------------- Sector | Affect -------------------
SET @sector_id = (SELECT id FROM taxation.economic_activity WHERE code = '132010');

select N'Inserting economic_activity_id and tenant_id', @sector_id, @tenant_id;
execute taxation.set_tenant_economic_activity @tenant_id, @sector_id;

INSERT INTO organization.product (name, unit_price, tenant_id) VALUES
('Producto Afecto 1', 5000, @tenant_id);

INSERT INTO organization.[service] (name, unit_price, tenant_id) VALUES
('Servicio Afecto 1', 400, @tenant_id);

-- ## Exempt
SET @sector_id = (SELECT id FROM taxation.economic_activity WHERE code = '602220');
execute taxation.set_tenant_economic_activity @tenant_id, @sector_id;

INSERT INTO organization.product (name, unit_price, tenant_id) VALUES
('Producto Exento 1', 300, @tenant_id);

INSERT INTO organization.[service] (name, unit_price, tenant_id) VALUES
('Servicio Exento 1', 100, @tenant_id);

-- ### Cliente1 ###
EXECUTE organization.add_enterprise
	'93083482',
	'Cliente 1',
	'Cliente',
	'',
	'',
	'000-0000',
	0,
	@tenant_id,
	@id = @enterprise_id out;

execute organization.add_enterprise_address @enterprise_id, 'Fake street1 200, piso 10, oficina 200', '', @commune_id, @id = @address_id out;
execute organization.set_enterprise_address_type @legal_role_addr_id, @address_id;
execute organization.set_enterprise_address_type @serv_recept_addr_id, @address_id;

declare @comercial_activity_id uniqueidentifier = (select id from taxation.comercial_activity where name = 'Salud');

execute taxation.set_enterprise_comercial_activity
	@enterprise_id,
	@comercial_activity_id;

set @comercial_activity_id = (select id from taxation.comercial_activity where name = 'Transporte');
execute taxation.set_enterprise_comercial_activity
	@enterprise_id,
	@comercial_activity_id;

-- ### Provider for Tenant 1 ###
execute organization.add_enterprise '71339521', 'Proveedor 2', 'Proveedor 2', 'prov2@p.com', '', '', 1, @tenant_id, @id = @enterprise_id out;

-- Tax Document Data

GO
SET IDENTITY_INSERT [taxation].[tax_document] ON 

GO
INSERT [taxation].[tax_document] ([id], [document_type], [folio_number], [document_number], [emission_point], [accounting_date], [accounting_time], [expire_date], [previous_debt_balance], [comments], [document_status], [tax_rate], [document_responsible], [default_payment_type_id], [dispatch_type_id], [reduction_type_id], [generation_date], [is_retained_extra_tax], [extra_tax_code], [extra_tax_rate], [total], [costs_center_id], [tenant_tax_id], [tenant_legal_name], [tenant_comercial_activity_name], [tenant_address], [tenant_commune], [tenant_city], [tenant_economic_activity_code], [tenant_id], [enterprise_id]) VALUES (N'dbf95607-5087-427a-b785-a59f00e9e404', N'33', 1, 1, N'localhost', CAST(N'2015-06-01' AS Date), NULL, NULL, CAST(0.0000 AS Decimal(13, 4)), N'', N'Pending', CAST(0.190 AS Decimal(4, 3)), N'Perez, Juan', NULL, NULL, NULL, CAST(N'2016-02-01T17:11:34.000' AS DateTime), 0, 0, CAST(0.000 AS Decimal(4, 3)), CAST(30226.0000 AS Decimal(13, 4)), NULL, N'96950918', N'Proveedor 1', N'Miner�a', N'Fake street 100, piso 10, oficina 20', N'Santiago', N'Santiago', N'132010', (select id from organization.tenant where tax_id = '96950918'), (select id from organization.enterprise where tax_id = '93083482'))
GO
INSERT [taxation].[tax_document] ([id], [document_type], [folio_number], [document_number], [emission_point], [accounting_date], [accounting_time], [expire_date], [previous_debt_balance], [comments], [document_status], [tax_rate], [document_responsible], [default_payment_type_id], [dispatch_type_id], [reduction_type_id], [generation_date], [is_retained_extra_tax], [extra_tax_code], [extra_tax_rate], [total], [costs_center_id], [tenant_tax_id], [tenant_legal_name], [tenant_comercial_activity_name], [tenant_address], [tenant_commune], [tenant_city], [tenant_economic_activity_code], [tenant_id], [enterprise_id]) VALUES (N'3214bd03-6ec3-464b-9e3f-a59f00e9e46f', N'34', 1, 2, N'localhost', CAST(N'2015-06-01' AS Date), NULL, NULL, CAST(0.0000 AS Decimal(13, 4)), N'', N'Pending', CAST(0.000 AS Decimal(4, 3)), N'Perez, Juan', NULL, NULL, NULL, CAST(N'2016-02-01T17:11:34.000' AS DateTime), 0, 0, CAST(0.000 AS Decimal(4, 3)), CAST(1000.0000 AS Decimal(13, 4)), NULL, N'96950918', N'Proveedor 1', N'Transporte', N'Fake street 100, piso 10, oficina 20', N'Santiago', N'Santiago', N'132010', (select id from organization.tenant where tax_id = '96950918'), (select id from organization.enterprise where tax_id = '93083482'))
GO
INSERT [taxation].[tax_document] ([id], [document_type], [folio_number], [document_number], [emission_point], [accounting_date], [accounting_time], [expire_date], [previous_debt_balance], [comments], [document_status], [tax_rate], [document_responsible], [default_payment_type_id], [dispatch_type_id], [reduction_type_id], [generation_date], [is_retained_extra_tax], [extra_tax_code], [extra_tax_rate], [total], [costs_center_id], [tenant_tax_id], [tenant_legal_name], [tenant_comercial_activity_name], [tenant_address], [tenant_commune], [tenant_city], [tenant_economic_activity_code], [tenant_id], [enterprise_id]) VALUES (N'e589c5f7-da18-42b7-88ea-a59f00ea9aba', N'61', 1, 3, N'localhost', CAST(N'2015-06-01' AS Date), NULL, NULL, CAST(0.0000 AS Decimal(13, 4)), N'', N'Pending', CAST(0.190 AS Decimal(4, 3)), N'Perez, Juan', NULL, NULL, NULL, CAST(N'2016-02-01T17:14:10.000' AS DateTime), 0, 0, CAST(0.000 AS Decimal(4, 3)), CAST(0.0000 AS Decimal(13, 4)), NULL, N'96950918', N'Proveedor 1', N'Miner�a', N'Fake street 100, piso 10, oficina 20', N'Santiago', N'Santiago', N'132010', (select id from organization.tenant where tax_id = '96950918'), (select id from organization.enterprise where tax_id = '93083482'))
GO
INSERT [taxation].[tax_document] ([id], [document_type], [folio_number], [document_number], [emission_point], [accounting_date], [accounting_time], [expire_date], [previous_debt_balance], [comments], [document_status], [tax_rate], [document_responsible], [default_payment_type_id], [dispatch_type_id], [reduction_type_id], [generation_date], [is_retained_extra_tax], [extra_tax_code], [extra_tax_rate], [total], [costs_center_id], [tenant_tax_id], [tenant_legal_name], [tenant_comercial_activity_name], [tenant_address], [tenant_commune], [tenant_city], [tenant_economic_activity_code], [tenant_id], [enterprise_id]) VALUES (N'a9ac1945-19c7-43d5-b1f1-a59f00ea9b09', N'61', 2, 4, N'localhost', CAST(N'2015-06-01' AS Date), NULL, NULL, CAST(0.0000 AS Decimal(13, 4)), N'', N'Pending', CAST(0.000 AS Decimal(4, 3)), N'Perez, Juan', NULL, NULL, NULL, CAST(N'2016-02-01T17:14:10.000' AS DateTime), 0, 0, CAST(0.000 AS Decimal(4, 3)), CAST(600.0000 AS Decimal(13, 4)), NULL, N'96950918', N'Proveedor 1', N'Miner�a', N'Fake street 100, piso 10, oficina 20', N'Santiago', N'Santiago', N'132010', (select id from organization.tenant where tax_id = '96950918'), (select id from organization.enterprise where tax_id = '93083482'))
GO
INSERT [taxation].[tax_document] ([id], [document_type], [folio_number], [document_number], [emission_point], [accounting_date], [accounting_time], [expire_date], [previous_debt_balance], [comments], [document_status], [tax_rate], [document_responsible], [default_payment_type_id], [dispatch_type_id], [reduction_type_id], [generation_date], [is_retained_extra_tax], [extra_tax_code], [extra_tax_rate], [total], [costs_center_id], [tenant_tax_id], [tenant_legal_name], [tenant_comercial_activity_name], [tenant_address], [tenant_commune], [tenant_city], [tenant_economic_activity_code], [tenant_id], [enterprise_id]) VALUES (N'efa1dfb2-74ef-423c-b6c8-a59f00ea9b2c', N'61', 3, 5, N'localhost', CAST(N'2015-06-01' AS Date), NULL, NULL, CAST(0.0000 AS Decimal(13, 4)), N'', N'Pending', CAST(0.190 AS Decimal(4, 3)), N'Perez, Juan', NULL, NULL, NULL, CAST(N'2016-02-01T17:14:10.000' AS DateTime), 0, 0, CAST(0.000 AS Decimal(4, 3)), CAST(30226.0000 AS Decimal(13, 4)), NULL, N'96950918', N'Proveedor 1', N'Miner�a', N'Fake street 100, piso 10, oficina 20', N'Santiago', N'Santiago', N'132010', (select id from organization.tenant where tax_id = '96950918'), (select id from organization.enterprise where tax_id = '93083482'))
GO
INSERT [taxation].[tax_document] ([id], [document_type], [folio_number], [document_number], [emission_point], [accounting_date], [accounting_time], [expire_date], [previous_debt_balance], [comments], [document_status], [tax_rate], [document_responsible], [default_payment_type_id], [dispatch_type_id], [reduction_type_id], [generation_date], [is_retained_extra_tax], [extra_tax_code], [extra_tax_rate], [total], [costs_center_id], [tenant_tax_id], [tenant_legal_name], [tenant_comercial_activity_name], [tenant_address], [tenant_commune], [tenant_city], [tenant_economic_activity_code], [tenant_id], [enterprise_id]) VALUES (N'4f23851f-4742-4302-90be-a59f00eaaccf', N'56', 1, 6, N'localhost', CAST(N'2015-06-01' AS Date), NULL, NULL, CAST(0.0000 AS Decimal(13, 4)), N'', N'Pending', CAST(0.190 AS Decimal(4, 3)), N'Perez, Juan', NULL, NULL, NULL, CAST(N'2016-02-01T17:14:25.000' AS DateTime), 0, 0, CAST(0.000 AS Decimal(4, 3)), CAST(41650.0000 AS Decimal(13, 4)), NULL, N'96950918', N'Proveedor 1', N'Miner�a', N'Fake street 100, piso 10, oficina 20', N'Santiago', N'Santiago', N'132010', (select id from organization.tenant where tax_id = '96950918'), (select id from organization.enterprise where tax_id = '93083482'))
GO
INSERT [taxation].[tax_document] ([id], [document_type], [folio_number], [document_number], [emission_point], [accounting_date], [accounting_time], [expire_date], [previous_debt_balance], [comments], [document_status], [tax_rate], [document_responsible], [default_payment_type_id], [dispatch_type_id], [reduction_type_id], [generation_date], [is_retained_extra_tax], [extra_tax_code], [extra_tax_rate], [total], [costs_center_id], [tenant_tax_id], [tenant_legal_name], [tenant_comercial_activity_name], [tenant_address], [tenant_commune], [tenant_city], [tenant_economic_activity_code], [tenant_id], [enterprise_id]) VALUES (N'149f6536-c6b3-4152-8162-a59f00eaad18', N'56', 2, 7, N'localhost', CAST(N'2015-06-01' AS Date), NULL, NULL, CAST(0.0000 AS Decimal(13, 4)), N'', N'Pending', CAST(0.190 AS Decimal(4, 3)), N'Perez, Juan', NULL, NULL, NULL, CAST(N'2016-02-01T17:14:25.000' AS DateTime), 0, 0, CAST(0.000 AS Decimal(4, 3)), CAST(30226.0000 AS Decimal(13, 4)), NULL, N'96950918', N'Proveedor 1', N'Miner�a', N'Fake street 100, piso 10, oficina 20', N'Santiago', N'Santiago', N'132010', (select id from organization.tenant where tax_id = '96950918'), (select id from organization.enterprise where tax_id = '93083482'))
GO
SET IDENTITY_INSERT [taxation].[tax_document] OFF
GO
INSERT [taxation].[taxdoc_client] ([id], [tax_id], [legal_name], [phone], [bank_account_number], comercial_activity) VALUES (N'dbf95607-5087-427a-b785-a59f00e9e404', N'93083482', N'Cliente 1', N'', N'000-0000', N'Miner�a')
GO
INSERT [taxation].[taxdoc_client] ([id], [tax_id], [legal_name], [phone], [bank_account_number], comercial_activity) VALUES (N'3214bd03-6ec3-464b-9e3f-a59f00e9e46f', N'93083482', N'Cliente 1', N'', N'000-0000', N'Servicios De Transporte A Turistas')
GO
INSERT [taxation].[taxdoc_client] ([id], [tax_id], [legal_name], [phone], [bank_account_number], comercial_activity) VALUES (N'e589c5f7-da18-42b7-88ea-a59f00ea9aba', N'93083482', N'Google Inc.', N'', N'000-0000', N'Miner�a')
GO
INSERT [taxation].[taxdoc_client] ([id], [tax_id], [legal_name], [phone], [bank_account_number], comercial_activity) VALUES (N'a9ac1945-19c7-43d5-b1f1-a59f00ea9b09', N'93083482', N'Google Inc.', N'', N'000-0000', N'Miner�a')
GO
INSERT [taxation].[taxdoc_client] ([id], [tax_id], [legal_name], [phone], [bank_account_number], comercial_activity) VALUES (N'efa1dfb2-74ef-423c-b6c8-a59f00ea9b2c', N'93083482', N'Google Inc.', N'', N'000-0000', N'Miner�a')
GO
INSERT [taxation].[taxdoc_client] ([id], [tax_id], [legal_name], [phone], [bank_account_number], comercial_activity) VALUES (N'4f23851f-4742-4302-90be-a59f00eaaccf', N'93083482', N'Google Inc.', N'', N'000-0000', N'Miner�a')
GO
INSERT [taxation].[taxdoc_client] ([id], [tax_id], [legal_name], [phone], [bank_account_number], comercial_activity) VALUES (N'149f6536-c6b3-4152-8162-a59f00eaad18', N'93083482', N'Google Inc.', N'', N'000-0000', N'Miner�a')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'7b16b223-4016-449b-8dda-a59f00e9e407', N'dbf95607-5087-427a-b785-a59f00e9e404', 1, N'Producto Afecto 1', CAST(5000.0000 AS Decimal(13, 4)), CAST(5.0000 AS Decimal(13, 4)), CAST(4750.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(29750.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'9219b3d8-cddc-4414-b1a8-a59f00e9e408', N'dbf95607-5087-427a-b785-a59f00e9e404', 2, N'Servicio Afecto 1', CAST(400.0000 AS Decimal(13, 4)), CAST(1.0000 AS Decimal(13, 4)), CAST(76.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(476.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit], tax_type) VALUES (N'2114faf9-6c3f-42fa-81de-a59f00e9e46f', N'3214bd03-6ec3-464b-9e3f-a59f00e9e46f', 1, N'Producto Exento 1', CAST(300.0000 AS Decimal(13, 4)), CAST(3.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(900.0000 AS Decimal(13, 4)), N'', N'', 1)
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit], tax_type) VALUES (N'efe39385-e511-4b93-9cef-a59f00e9e46f', N'3214bd03-6ec3-464b-9e3f-a59f00e9e46f', 2, N'Servicio Exento 1', CAST(100.0000 AS Decimal(13, 4)), CAST(1.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(100.0000 AS Decimal(13, 4)), N'', N'', 1)
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'0a5010e8-2cfb-46b5-9477-a59f00ea9abc', N'e589c5f7-da18-42b7-88ea-a59f00ea9aba', 1, N'Modifica el nombre del cliente', CAST(0.0000 AS Decimal(13, 4)), CAST(1.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'f8f46f6c-7ab9-4474-bbc1-a59f00ea9b09', N'a9ac1945-19c7-43d5-b1f1-a59f00ea9b09', 1, N'Producto Exento 1', CAST(300.0000 AS Decimal(13, 4)), CAST(2.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(600.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'dd781f01-b1b2-4933-969e-a59f00ea9b2c', N'efa1dfb2-74ef-423c-b6c8-a59f00ea9b2c', 1, N'Producto Afecto 1', CAST(5000.0000 AS Decimal(13, 4)), CAST(5.0000 AS Decimal(13, 4)), CAST(4750.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(29750.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'e8c71be2-5e86-451f-9d9c-a59f00ea9b2c', N'efa1dfb2-74ef-423c-b6c8-a59f00ea9b2c', 2, N'Servicio Afecto 1', CAST(400.0000 AS Decimal(13, 4)), CAST(1.0000 AS Decimal(13, 4)), CAST(76.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(476.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'434ac013-3e30-42da-abcd-a59f00eaacd2', N'4f23851f-4742-4302-90be-a59f00eaaccf', 1, N'Producto Afecto 1', CAST(5000.0000 AS Decimal(13, 4)), CAST(7.0000 AS Decimal(13, 4)), CAST(6650.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(41650.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'5155b994-2857-4d70-afb5-a59f00eaad18', N'149f6536-c6b3-4152-8162-a59f00eaad18', 2, N'Servicio Afecto 1', CAST(400.0000 AS Decimal(13, 4)), CAST(1.0000 AS Decimal(13, 4)), CAST(76.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(476.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[tax_document_line] ([id], [tax_document_id], [line_number], [text], [unit_price], [quantity], [tax_amount], [extra_tax_amount], [line_total], [additional_gloss], [measure_unit]) VALUES (N'a13e4c6e-c1dd-4393-b132-a59f00eaad18', N'149f6536-c6b3-4152-8162-a59f00eaad18', 1, N'Producto Afecto 1', CAST(5000.0000 AS Decimal(13, 4)), CAST(5.0000 AS Decimal(13, 4)), CAST(4750.0000 AS Decimal(13, 4)), CAST(0.0000 AS Decimal(13, 4)), CAST(29750.0000 AS Decimal(13, 4)), N'', N'')
GO
INSERT [taxation].[taxdoc_client_address] ([id], [line1], [line2], [commune], [city], [taxdoc_client_id]) VALUES (N'155b29c5-aada-4011-ba59-a59f00e9e408', N'Fake street1 200, piso 10, oficina 200', N'', N'Santiago', N'Santiago', N'dbf95607-5087-427a-b785-a59f00e9e404')
GO
INSERT [taxation].[taxdoc_client_address] ([id], [line1], [line2], [commune], [city], [taxdoc_client_id]) VALUES (N'6254bf68-e119-496e-a93d-a59f00e9e46f', N'Fake street1 200, piso 10, oficina 200', N'', N'Santiago', N'Santiago', N'3214bd03-6ec3-464b-9e3f-a59f00e9e46f')
GO
INSERT [taxation].[taxdoc_client_address] ([id], [line1], [line2], [commune], [city], [taxdoc_client_id]) VALUES (N'ab29fc3e-ba23-44f0-98e0-a59f00ea9abe', N'Fake street1 200, piso 10, oficina 200', N'', N'Santiago', N'Santiago', N'e589c5f7-da18-42b7-88ea-a59f00ea9aba')
GO
INSERT [taxation].[taxdoc_client_address] ([id], [line1], [line2], [commune], [city], [taxdoc_client_id]) VALUES (N'f22157fc-79eb-4107-b64d-a59f00ea9b09', N'Fake street1 200, piso 10, oficina 200', N'', N'Santiago', N'Santiago', N'a9ac1945-19c7-43d5-b1f1-a59f00ea9b09')
GO
INSERT [taxation].[taxdoc_client_address] ([id], [line1], [line2], [commune], [city], [taxdoc_client_id]) VALUES (N'ba04a8c0-50cc-43c9-8ec5-a59f00ea9b2c', N'Fake street1 200, piso 10, oficina 200', N'', N'Santiago', N'Santiago', N'efa1dfb2-74ef-423c-b6c8-a59f00ea9b2c')
GO
INSERT [taxation].[taxdoc_client_address] ([id], [line1], [line2], [commune], [city], [taxdoc_client_id]) VALUES (N'd6629a0d-0697-4f49-bc5f-a59f00eaacd4', N'Fake street1 200, piso 10, oficina 200', N'', N'Santiago', N'Santiago', N'4f23851f-4742-4302-90be-a59f00eaaccf')
GO
INSERT [taxation].[taxdoc_client_address] ([id], [line1], [line2], [commune], [city], [taxdoc_client_id]) VALUES (N'66ac3c3c-650a-4af1-bf1a-a59f00eaad18', N'Fake street1 200, piso 10, oficina 200', N'', N'Santiago', N'Santiago', N'149f6536-c6b3-4152-8162-a59f00eaad18')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'e9f930b1-76a6-4f98-b535-a59f00e9e409', (select id from organization.address_type where name = 'legal'), N'155b29c5-aada-4011-ba59-a59f00e9e408')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'f489482d-932b-48ed-924b-a59f00e9e46f', (select id from organization.address_type where name = 'legal'), N'6254bf68-e119-496e-a93d-a59f00e9e46f')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'2849464c-bf41-4319-9dbe-a59f00ea9abe', (select id from organization.address_type where name = 'legal'), N'ab29fc3e-ba23-44f0-98e0-a59f00ea9abe')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'e0b7c1b4-a0b3-44a6-b789-a59f00ea9b09', (select id from organization.address_type where name = 'legal'), N'f22157fc-79eb-4107-b64d-a59f00ea9b09')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'5044dc6c-975c-4151-a6d6-a59f00ea9b2c', (select id from organization.address_type where name = 'legal'), N'ba04a8c0-50cc-43c9-8ec5-a59f00ea9b2c')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'b95f9c10-b28e-4854-a6eb-a59f00eaacd4', (select id from organization.address_type where name = 'legal'), N'd6629a0d-0697-4f49-bc5f-a59f00eaacd4')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'528fb75a-432c-4c52-b8f9-a59f00eaad18', (select id from organization.address_type where name = 'legal'), N'66ac3c3c-650a-4af1-bf1a-a59f00eaad18')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'5a98a263-70b5-4c96-969c-a59f00e9e409', (select id from organization.address_type where name = 'service_reception'), N'155b29c5-aada-4011-ba59-a59f00e9e408')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'118672ea-4f35-42b6-9208-a59f00e9e46f', (select id from organization.address_type where name = 'service_reception'), N'6254bf68-e119-496e-a93d-a59f00e9e46f')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'eb3d0abe-2305-46cf-b08b-a59f00ea9abe', (select id from organization.address_type where name = 'service_reception'), N'ab29fc3e-ba23-44f0-98e0-a59f00ea9abe')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'440b28c6-6ca6-42e9-876b-a59f00ea9b09', (select id from organization.address_type where name = 'service_reception'), N'f22157fc-79eb-4107-b64d-a59f00ea9b09')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'902fda40-7bca-40e0-a481-a59f00ea9b2c', (select id from organization.address_type where name = 'service_reception'), N'ba04a8c0-50cc-43c9-8ec5-a59f00ea9b2c')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'fdb7934e-e6a9-4000-be7b-a59f00eaacd5', (select id from organization.address_type where name = 'service_reception'), N'd6629a0d-0697-4f49-bc5f-a59f00eaacd4')
GO
INSERT [taxation].[taxdoc_client_address_type] ([id], [address_type_id], [taxdoc_client_address_id]) VALUES (N'a2b7b7c8-7981-48c8-9e9b-a59f00eaad18', (select id from organization.address_type where name = 'service_reception'), N'66ac3c3c-650a-4af1-bf1a-a59f00eaad18')
GO

-- Credit Note References
insert into taxation.tax_document_reference (parent_id, reference_id, reference_comment, reference_reason) values (
	(select id from taxation.tax_document where document_type = '61' and folio_number = 1),
	(select id from taxation.tax_document where document_type = '34' and folio_number = 1),
	'Modifica el texto del documento',
	'AmendText');
insert into taxation.tax_document_reference (parent_id, reference_id, reference_comment, reference_reason) values (
	(select id from taxation.tax_document where document_type = '61' and folio_number = 2),
	(select id from taxation.tax_document where document_type = '34' and folio_number = 1),
	'Corrige montos - Elimina el Producto 1',
	'AmendValues');
insert into taxation.tax_document_reference (parent_id, reference_id, reference_comment, reference_reason) values (
	(select id from taxation.tax_document where document_type = '61' and folio_number = 3),
	(select id from taxation.tax_document where document_type = '33' and folio_number = 1),
	'Anulaci�n',
	'VoidDocument');

-- Debit Note References
insert into taxation.tax_document_reference (parent_id, reference_id, reference_comment, reference_reason) values (
	(select id from taxation.tax_document where document_type = '56' and folio_number = 1),
	(select id from taxation.tax_document where document_type = '33' and folio_number = 1),
	'Modifica la cantidad de Producto Afecto 1',
	'AmendValues');
insert into taxation.tax_document_reference (parent_id, reference_id, reference_comment, reference_reason) values (
	(select id from taxation.tax_document where document_type = '56' and folio_number = 2),
	(select id from taxation.tax_document where document_type = '61' and folio_number = 3),
	'Modifica el texto del documento',
	'VoidDocument');

-- EBill
update taxation.folio_range set current_folio = 2 where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '33';
-- Exempt EBill
update taxation.folio_range set current_folio = 2 where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '34';
-- Credit Note
update taxation.folio_range set current_folio = 4 where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '61';
-- Debit Note
update taxation.folio_range set current_folio = 3 where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '56';

-- Test data to force folio range alerts
update taxation.folio_range set current_folio = 190 where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '56';
update taxation.folio_range set current_folio = 4 where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '56';

update taxation.tax_document set
	exempt_total = 0.0000,
	affect_total = 25400.0000,
	total = 30226.0000
where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '33' and folio_number = 1;

update taxation.tax_document set
	exempt_total = 1000.0000,
	affect_total = 0.0000,
	total = 1000.0000
where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '34' and folio_number = 1;

update taxation.tax_document set
	exempt_total = 0.0000,
	affect_total = 35000.0000,
	total = 41650.0000
where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '56' and folio_number = 1;

update taxation.tax_document set
	exempt_total = 0.0000,
	affect_total = 25400.0000,
	total = 30226.0000
where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '56' and folio_number = 2;

update taxation.tax_document set
	exempt_total = 600.0000,
	affect_total = 0.0000,
	total = 600.0000
where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '61' and folio_number = 2;

update taxation.tax_document set
	exempt_total = 0.0000,
	affect_total = 25400.0000,
	total = 30226.0000
where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '61' and folio_number = 3;

declare @max int = 20;
declare @i int = 0;
declare @code varchar(2);
while(@i < @max)
begin
	if(@i < 10)
		set @code = CONCAT('0', @i);
	else
		set @code = @i;

	insert into accounting.accounting_account (tenant_id, code, description) values
	(
		(select id from organization.tenant where tax_id = '96950918'),
		CONCAT('20-00-0', @code),
		CONCAT('Cuenta Contable ', (@i+1))
	);
	set @i = @i + 1;
end;

set @i = 0;
while(@i < @max)
begin
	if(@i < 10)
		set @code = CONCAT('0', @i);
	else
		set @code = @i;

	insert into organization.costs_center (tenant_id, name, code, description) values
	(
		(select id from organization.tenant where tax_id = '96950918'),
		CONCAT('Centro de Costos', (@i+1)),
		CONCAT('17-01-0', @code),
		''
	);
	set @i = @i + 1;
end;