------------------------- Queries ---------------------------
declare @tenant_id uniqueidentifier = '';

SELECT this_.document_type, 
       this_.folio_number, 
       this_.accounting_date, 
       this_.document_status, 
       this_.id, 
       tc.legal_name, 
       tc.tax_id, 
       tc.id,
	   enterprise3_.legal_name as current_legal_name,
       sum(lines2_.line_total) AS total,
	   sum(lines2_.tax_amount) as total_tax_amount,
	   sum(lines2_.unit_price * lines2_.quantity) as netAmount
FROM   taxation.tax_document this_ 
       left JOIN taxation.tax_document_line lines2_ 
               ON this_.id = lines2_.tax_document_id 
       LEFT JOIN taxation.taxdoc_client tc 
              ON tc.id = this_.id 
       LEFT OUTER JOIN organization.enterprise enterprise3_ 
                    ON this_.enterprise_id = enterprise3_.id 
WHERE  this_.tenant_id = @tenant_id
GROUP  BY this_.folio_number,
          this_.accounting_date, 
          this_.document_status, 
          this_.id, 
          this_.document_type, 
          tc.legal_name, 
          tc.tax_id, 
          tc.id,
		  enterprise3_.legal_name;


select e.id, e.tax_id, e.legal_name, count(es.id) 'client_sectors' from organization.enterprise e
	left join taxation.enterprise_sector es on e.id = es.enterprise_id
	left join taxation.sector s on s.id = es.sector_id
group by e.id, e.tax_id, e.legal_name;

--------------------------------------------------------
SELECT
  this_.folio_number AS folio_number,
  this_.id AS id,
  this_.accounting_date AS accounting_date,
  this_.document_type AS document_type,
  this_.document_status AS document_status,
  client1_.legal_name AS client_legal_name,
  client1_.tax_id AS client_tax_id,
  client1_.id AS client_id,
  client1_.business_sector_name AS client_sector,
  CASE
    WHEN EXISTS (SELECT
        this_0_.id AS id
      FROM taxation.tax_document_reference this_0_
      WHERE (
      this_0_.reference_id = this_.id
      AND this_0_.reference_reason = 'VoidDocument')
      ORDER BY this_0_.id OFFSET 0 ROWS FETCH FIRST 10 ROWS ONLY) THEN 1
    ELSE 0
  END AS is_void,
  SUM(lines3_.tax_amount + lines3_.extra_tax_amount) AS total_taxes,
  SUM(lines3_.quantity * lines3_.unit_price) AS total_net_amount,
  SUM(lines3_.line_total) AS total
FROM taxation.tax_document this_
LEFT OUTER JOIN taxation.tax_document_reference referenced4_
  ON this_.id = referenced4_.reference_id
LEFT OUTER JOIN taxation.tax_document_line lines3_
  ON this_.id = lines3_.tax_document_id
LEFT OUTER JOIN organization.enterprise enterprise2_
  ON this_.enterprise_id = enterprise2_.id
LEFT OUTER JOIN taxation.taxdoc_client client1_
  ON this_.id = client1_.id
WHERE this_.tenant_id = ''
GROUP BY this_.folio_number,
         this_.id,
         this_.accounting_date,
         this_.document_type,
         this_.document_status,
         client1_.legal_name,
         client1_.tax_id,
         client1_.id,
         client1_.comercial_activity,
         this_.id
ORDER BY this_.folio_number DESC

SELECT this_.id, this_.name, this_.code, this_.is_subject_to_tax
FROM taxation.economic_activity this_
	left outer join taxation.economic_activity parent1_ on this_.economic_activity_group_id=parent1_.id
WHERE (parent1_.id = 'a6b4ea32-7e31-e511-82da-40167e223c96') ORDER BY this_.name asc

SELECT          this_.folio_number          AS folio_number, 
                this_.id                    AS y1_, 
                this_.accounting_date       AS y2_, 
                this_.document_type         AS y3_, 
                this_.document_status       AS y4_, 
                client1_.legal_name         AS y5_, 
                client1_.tax_id             AS y6_, 
                client1_.id                 AS y7_, 
                client1_.comercial_activity AS y8_, 
                this_.id                                           AS y10_, 
                sum(lines3_.tax_amount + lines3_.extra_tax_amount) AS y11_, 
                sum(lines3_.quantity   * lines3_.unit_price)       AS y12_, 
                sum(lines3_.line_total)                            AS y13_, ( 
                CASE 
                                WHEN lines3_.tax_type = 1 THEN sum(lines3_.quantity * lines3_.unit_price)
                                ELSE 0 
                END) AS exempt_amount,
				(case when lines3_.tax_type = 0 then sum((lines3_.quantity * lines3_.unit_price) + lines3_.tax_amount + lines3_.extra_tax_amount) else 0 end) as affect_amount
FROM            taxation.tax_document this_ 
LEFT OUTER JOIN taxation.tax_document_reference referenced4_ 
ON              this_.id=referenced4_.reference_id 
LEFT OUTER JOIN taxation.tax_document_line lines3_ 
ON              this_.id=lines3_.tax_document_id 
LEFT OUTER JOIN organization.enterprise enterprise2_ 
ON              this_.enterprise_id=enterprise2_.id 
LEFT OUTER JOIN taxation.taxdoc_client client1_ 
ON              this_.id=client1_.id 
WHERE           this_.tenant_id = '5a7e62be-faf4-e511-80ca-0800273e4440' 
GROUP BY        this_.folio_number, 
                this_.id, 
                this_.accounting_date, 
                this_.document_type, 
                this_.document_status, 
                client1_.legal_name, 
                client1_.tax_id, 
                client1_.id, 
                client1_.comercial_activity, 
                this_.id,
				lines3_.tax_type
ORDER BY        this_.folio_number

select * from taxation.tax_document_line
where tax_type = 0;

select td.id,
	td.folio_number,
	td.document_type,
	sum(line.quantity * line.unit_price) as net_amount
from taxation.tax_document td
	left outer join taxation.tax_document_line line on line.tax_document_id = td.id
where td.tenant_id = '5a7e62be-faf4-e511-80ca-0800273e4440'
group by td.id, td.folio_number, td.document_type
order by td.folio_number desc, td.document_type asc;

select td.id,
	td.folio_number,
	td.document_type,
	td.exempt_total,
	td.affect_total,
	td.total,
	(l.quantity * l.unit_price) as net_amount,
	l.tax_amount,
	l.extra_tax_amount,
	l.line_total
from taxation.tax_document td
	left outer join taxation.tax_document_line l on l.tax_document_id = td.id
where td.tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '61'
group by 
	td.id,
	td.exempt_total,
	td.affect_total,
	td.total,
	td.folio_number,
	td.document_type,
	l.tax_amount,
	l.extra_tax_amount,
	l.line_total,
	l.quantity,
	l.unit_price;

update taxation.tax_document set
	exempt_total = 0.0000,
	affect_total = 25400.0000,
	total = 30226.0000
where tenant_id = (select id from organization.tenant where tax_id = '96950918') and document_type = '61' and folio_number = 3;

-- Folio Ranges
SELECT TOP 1000 [id]
      ,[row_version]
      ,[document_type]
      ,[folio_number]
      ,[document_number]
      ,[emission_point]
      ,[accounting_date]
      ,[accounting_time]
      ,[expire_date]
      ,[previous_debt_balance]
      ,[comments]
      ,[document_status]
      ,[tax_rate]
      ,[document_responsible]
      ,[default_payment_type_id]
      ,[dispatch_type_id]
      ,[reduction_type_id]
      ,[generation_date]
      ,[is_retained_extra_tax]
      ,[extra_tax_code]
      ,[extra_tax_rate]
      ,[exempt_total]
      ,[affect_total]
      ,[total]
      ,[costs_center_id]
      ,[tenant_tax_id]
      ,[tenant_legal_name]
      ,[tenant_comercial_activity_name]
      ,[tenant_address]
      ,[tenant_commune]
      ,[tenant_city]
      ,[tenant_economic_activity_code]
      ,[tenant_economic_activity_name]
      ,[tenant_id]
      ,[enterprise_id]
  FROM [zentom_mssql].[taxation].[tax_document]

delete from taxation.tax_document where emission_point = 'vti_zentom';
update taxation.folio_range set current_folio = 2 where document_type = '33';
select * from taxation.folio_range;

select * from system.account_role
left join system.authorized_account on authorized_account.id = account_role.authorized_account_id
left join organization.tenant on tenant.id = authorized_account.tenant_id
left join system.account on account.id = authorized_account.account_id
left join system.role on role.id = account_role.role_id;



-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762630842'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762630842'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762630842'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735695'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735695'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735695'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '761830759'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '761830759'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '761830759'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '763766357'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '763766357'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '763766357'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735598'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735598'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735598'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '764141075'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '764141075'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '764141075'),
	(select id from system.module where name = 'document_emission_signing')
)

declare @tenant_id uniqueidentifier = (select id from organization.tenant where tax_id = '764141075');
execute system.add_setting
	'folio_range_alert',
	'90',
	@tenant_id;

set @tenant_id = (select id from organization.tenant where tax_id = '762630842');
execute system.add_setting
	'folio_range_alert',
	'90',
	@tenant_id;

set @tenant_id = (select id from organization.tenant where tax_id = '762735598');
execute system.add_setting
	'folio_range_alert',
	'90',
	@tenant_id;

set @tenant_id = (select id from organization.tenant where tax_id = '763766357');
execute system.add_setting
	'folio_range_alert',
	'90',
	@tenant_id;

set @tenant_id = (select id from organization.tenant where tax_id = '761830759');
execute system.add_setting
	'folio_range_alert',
	'90',
	@tenant_id;

set @tenant_id = (select id from organization.tenant where tax_id = '762735695');
execute system.add_setting
	'folio_range_alert',
	'90',
	@tenant_id;

select
	e.legal_name,
	e.id,
	e.tax_id,
	cc.code,
	cc.id,
	ea.line1,
	ea.line2
from organization.enterprise e
left outer join organization.costs_center cc on cc.id = e.costs_center_id
left outer join organization.enterprise_address ea on ea.enterprise_id = e.id
where e.tenant_id = '05dd3afa-270b-e611-80d0-0800273e4440';

update organization.enterprise set costs_center_id = null
where id = 'E761606F-F803-4228-ADD0-A5F600D36F06';

delete from taxation.received_tax_document;
delete from organization.enterprise where id = '970875C0-779A-4AB5-8E94-A5F6012B5B89';

update organization.enterprise set costs_center_id = null where id = 'DE5007DC-C922-4D1A-9F3F-A5F6012F2040';

select * from accounting.accounting_voucher av
left join accounting.accounting_voucher_line avl on avl.accounting_voucher_id = av.id;

select * from system.module_view;

select * from system.password_request;
delete from system.password_request;

select r.name, a.login from system.authorized_account aa
left join system.account_role ar on ar.authorized_account_id = aa.id
left join system.account a on a.id = aa.account_id
left join system.role r on r.id = ar.role_id;

select * from system.module_view;