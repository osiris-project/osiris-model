--USE zentom_mssql
--GO

------------------- Add Tenant
IF OBJECT_ID ('organization.add_tenant', 'P' ) IS NOT NULL 
    DROP PROCEDURE organization.add_tenant;
GO

CREATE PROCEDURE organization.add_tenant
(
	@tax_id VARCHAR(20),
	@legal_name NVARCHAR(100),
	@display_name NVARCHAR(100) = '',
	@email VARCHAR(1000),
	@phone VARCHAR(50),
	@bank_account_number VARCHAR(15) = '',
	@bill_resolution_date DATE = NULL,
	@bill_resolution_number INT = NULL,
	@is_enabled BIT = 1,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	SET @id = (SELECT id FROM organization.tenant WHERE tax_id = @tax_id);

	IF (@id IS NULL)
	BEGIN
		INSERT INTO organization.tenant (tax_id, legal_name, display_name, email, phone, bank_account_number, bill_resolution_date, bill_resolution_number, is_enabled) VALUES
		(@tax_id, @legal_name, @display_name, @email, @phone, @bank_account_number, @bill_resolution_date, @bill_resolution_number, @is_enabled);
		SET @id = (SELECT id FROM organization.tenant WHERE tax_id = @tax_id);
	END
END
GO

------------------- Create Tenant Group
if object_id ('organization.create_tenant_group', 'P') is not null
	drop procedure organization.create_tenant_group;
go

create procedure organization.create_tenant_group
(
	@group_name nvarchar(100),
	@headquarters_id uniqueidentifier = null,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from organization.[group] where name = @group_name);

	if @id is null
	begin
		insert into organization.[group] (name, headquarters) values (@group_name, @headquarters_id);
		set @id = (select id from organization.[group] where name = @group_name);
	end;
end;
go

------------------- Add Tenant To Group
if object_id ('organization.add_tenant_to_group', 'P') is not null
	drop procedure organization.add_tenant_to_group;
go

create procedure organization.add_tenant_to_group
(
	@group_name nvarchar(100),
	@headquarters_id uniqueidentifier = null,
	@tenant_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	declare @group_id uniqueidentifier;
	exec organization.create_tenant_group @group_name, @headquarters_id, @group_id out;
	set @id = (select id from organization.tenant_group where tenant_id = @tenant_id and group_id = @group_id);

	if @id is null
	begin
		insert into organization.tenant_group (tenant_id, group_id) values (@tenant_id, @group_id);
		set @id = (select id from organization.tenant_group where tenant_id = @tenant_id and group_id = @group_id);
	end;
end;
go

------------------- Add Enterprise
-- / Management.Enterprise
IF OBJECT_ID ('organization.add_enterprise', 'P' ) IS NOT NULL 
    DROP PROCEDURE organization.add_enterprise;
GO

CREATE PROCEDURE organization.add_enterprise
(
	@tax_id VARCHAR(20),
	@legal_name NVARCHAR(100),
	@display_name NVARCHAR(100) = '',
	@email VARCHAR(1000),
	@phone VARCHAR(50),
	@bank_account_number VARCHAR(15) = '',
	@relationship tinyint = 1,
	@tenant_id uniqueidentifier,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	SET @id = (SELECT id FROM organization.enterprise WHERE tax_id = @tax_id);

	IF (@id IS NULL)
	BEGIN
		INSERT INTO organization.enterprise (tax_id, legal_name, display_name, email, phone, bank_account_number, relationship, tenant_id) VALUES
		(@tax_id, @legal_name, @display_name, @email, @phone, @bank_account_number, @relationship, @tenant_id);
		SET @id = (SELECT id FROM organization.enterprise WHERE tax_id = @tax_id);
	END
END
GO

------------------- Add Setting
IF OBJECT_ID ('system.add_setting', 'P') IS NOT NULL
	DROP PROCEDURE [system].add_setting;
GO

CREATE PROCEDURE [system].add_setting (
	@setting_name VARCHAR(255),
	@setting_value NVARCHAR(MAX),
	@tenant_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
) AS
BEGIN
	SET @id = (SELECT id FROM [system].tenant_settings WHERE tenant_id = @tenant_id AND setting_name = @setting_name)

	IF (@id IS NULL)
	BEGIN
		INSERT INTO [system].tenant_settings (setting_name, setting_value, tenant_id) VALUES
		(@setting_name, @setting_value, @tenant_id);
		SET @id = (SELECT id FROM [system].tenant_settings WHERE tenant_id = @tenant_id AND setting_name = @setting_name)
	END
END
GO

------------------- Disable Tenant
IF OBJECT_ID ('organization.disable_tenant', 'P' ) IS NOT NULL 
    DROP PROCEDURE organization.disable_tenant;
GO

CREATE PROCEDURE organization.disable_tenant (@tax_id VARCHAR(20))
AS
BEGIN
	UPDATE organization.tenant SET is_enabled = 0 WHERE tax_id = @tax_id;
END;
GO

------------------- Enable Tenant
IF OBJECT_ID ('organization.enable_tenant', 'P' ) IS NOT NULL 
    DROP PROCEDURE organization.enable_tenant;
GO

CREATE PROCEDURE organization.enable_tenant (@tax_id VARCHAR(20))
AS
BEGIN
	UPDATE organization.tenant SET is_enabled = 1 WHERE tax_id = @tax_id;
END;
GO

------------------- Add Employee
-- / Management.Person
IF OBJECT_ID ( 'organization.add_employee', 'P' ) IS NOT NULL 
    DROP PROCEDURE organization.add_employee;
GO

create procedure organization.add_employee
(
	@person_id uniqueidentifier,
	@tenant_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
)
as
begin
	set @id = (select id from organization.employee where tenant_id = @tenant_id and person_id = @person_id);

	if (@id is null)
	begin
		insert into organization.employee (tenant_id, person_id) values (@tenant_id, @person_id);
		set @id = (select id from organization.employee where tenant_id = @tenant_id and person_id = @person_id);
	end;
end;
go

------------------- Set Enterprise Comercial Activity
if object_id ('taxation.set_enterprise_comercial_activity', 'P') is not null
	drop procedure taxation.set_enterprise_comercial_activity;
go

create procedure taxation.set_enterprise_comercial_activity
(
	@enterprise_id uniqueidentifier,
	@comercial_activity_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.enterprise_comercial_activity where enterprise_id = @enterprise_id and comercial_activity_id = @comercial_activity_id);

	if (@id is null)
	begin
		insert into taxation.enterprise_comercial_activity (enterprise_id, comercial_activity_id) values (@enterprise_id, @comercial_activity_id);
		set @id = (select id from taxation.enterprise_comercial_activity where enterprise_id = @enterprise_id and comercial_activity_id = @comercial_activity_id);
	end;
end;
go

------------------- Set Enterprise Economic Activity
if object_id ('taxation.set_enterprise_economic_activity', 'P') is not null
	drop procedure taxation.set_enterprise_economic_activity;
go

create procedure taxation.set_enterprise_economic_activity
(
	@enterprise_id uniqueidentifier,
	@economic_activity_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.enterprise_economic_activity where enterprise_id = @enterprise_id and economic_activity_id = @economic_activity_id);

	if (@id is null)
	begin
		insert into taxation.enterprise_economic_activity (enterprise_id, economic_activity_id) values (@enterprise_id, @economic_activity_id);
		set @id = (select id from taxation.enterprise_economic_activity where enterprise_id = @enterprise_id and economic_activity_id = @economic_activity_id);
	end;
end;
go

------------------- Set Tenant Comercial Activity
if object_id ('taxation.set_tenant_comercial_activity', 'P') is not null
	drop procedure taxation.set_tenant_comercial_activity;
go

create procedure taxation.set_tenant_comercial_activity
(
	@tenant_id uniqueidentifier,
	@comercial_activity_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.tenant_comercial_activity where tenant_id = @tenant_id and comercial_activity_id = @comercial_activity_id);

	if (@id is null)
	begin
		insert into taxation.tenant_comercial_activity (tenant_id, comercial_activity_id) values (@tenant_id, @comercial_activity_id);
		set @id = (select id from taxation.tenant_comercial_activity where tenant_id = @tenant_id and comercial_activity_id = @comercial_activity_id);
	end;
end;
go

------------------- Set Tenant Economic Activity
if object_id ('taxation.set_tenant_economic_activity', 'P') is not null
	drop procedure taxation.set_tenant_economic_activity;
go

create procedure taxation.set_tenant_economic_activity
(
	@tenant_id uniqueidentifier,
	@economic_activity_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.tenant_economic_activity where tenant_id = @tenant_id and economic_activity_id = @economic_activity_id);

	if (@id is null)
	begin
		insert into taxation.tenant_economic_activity (tenant_id, economic_activity_id) values (@tenant_id, @economic_activity_id);
		set @id = (select id from taxation.tenant_economic_activity where tenant_id = @tenant_id and economic_activity_id = @economic_activity_id);
	end;
end;
go

/* / System.Role */
IF OBJECT_ID ('system.add_role', 'P') IS NOT NULL
	DROP PROCEDURE [system].add_role;
GO

CREATE PROCEDURE [system].add_role
(
	@name VARCHAR(100),
	@display_name VARCHAR(100),
	@description VARCHAR(MAX) = '',
	@tenant_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	SET @id = (SELECT id FROM [system].role WHERE name = @name AND tenant_id = @tenant_id);

	IF (@id IS NULL)
	BEGIN
		INSERT INTO [system].[role] (name, display_name, [description], tenant_id) VALUES (@name, @display_name, @description, @tenant_id);
		SET @id = (SELECT id FROM [system].role WHERE name = @name AND tenant_id = @tenant_id);
	END
END;
GO

IF OBJECT_ID ('system.add_account_role', 'P') IS NOT NULL
	DROP PROCEDURE [system].add_account_role;
GO

CREATE PROCEDURE [system].add_account_role
(
	@authorized_account_id UNIQUEIDENTIFIER,
	@role_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	SET @id = (SELECT id FROM [system].account_role WHERE authorized_account_id = @authorized_account_id AND role_id = @role_id);

	IF (@id IS NULL)
	BEGIN
		INSERT INTO [system].account_role (authorized_account_id, role_id) VALUES (@authorized_account_id, @role_id);
		SET @id = (SELECT id FROM [system].account_role WHERE authorized_account_id = @authorized_account_id AND role_id = @role_id);
	END
END;
GO

-- \ System.Role

-- / System.Permission
IF OBJECT_ID ('system.set_all_permissions_to_role', 'P') IS NOT NULL
	DROP PROCEDURE [system].set_all_permissions_to_role;
GO

CREATE PROCEDURE [system].set_all_permissions_to_role(@role_id UNIQUEIDENTIFIER)
AS
BEGIN
	DECLARE permission_cursor CURSOR
		LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR
		SELECT id FROM [system].permission;

	DECLARE @insert_count INT = 0;
	DECLARE @row_count INT = @@CURSOR_ROWS;
	DECLARE @permission_id UNIQUEIDENTIFIER;
	DECLARE @id UNIQUEIDENTIFIER;

	OPEN permission_cursor;
	FETCH NEXT FROM permission_cursor INTO @permission_id;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		 SET @id = (SELECT id FROM [system].role_permission WHERE permission_id = @permission_id AND role_id = @role_id);

		 IF (@id IS NULL)
			 BEGIN
				INSERT INTO [system].role_permission (role_id, permission_id) VALUES (@role_id, @permission_id);
				SET @insert_count = @insert_count + 1;
			 END
		 ELSE

		FETCH NEXT FROM permission_cursor INTO @permission_id;
	END
	CLOSE permission_cursor;
	DEALLOCATE permission_cursor;

	PRINT 'Inserted rows: ' + (CAST(@insert_count AS VARCHAR(20)));
	PRINT 'Existing permission count: ' + (CAST(@row_count AS VARCHAR(20)));
END;
GO

-------------- Enable All Modules on Tenant
if object_id ('system.enable_all_modules', 'P') is not null
	drop procedure system.enable_all_modules;
go

create procedure system.enable_all_modules
(
	@tenant_id uniqueidentifier
)
as
begin
	declare module_cursor cursor
		local static read_only forward_only
	for
		select id from system.module;

	declare @insert_count int = 0;
	
	declare @module_id uniqueidentifier;
	declare @id uniqueidentifier;

	open module_cursor;
	declare @row_count int = @@cursor_rows;
	fetch next from module_cursor into @module_id;
	while @@fetch_status = 0
	begin
		set @id = (select id from system.tenant_module where module_id = @module_id and tenant_id = @tenant_id);

		if(@id is null)
		begin
			insert into system.tenant_module (tenant_id, module_id, settings, [version], is_enabled) values
			(@tenant_id, @module_id, '', '1.0.0', 1);
			set @insert_count = @insert_count + 1;
		end;
		fetch next from module_cursor into @module_id;
	end;
	close module_cursor;
	deallocate module_cursor;

	print N'Inserted rows: ' + (cast(@insert_count as varchar(20)));
	print N'Existing modules: ' + (cast(@row_count as varchar(20)));
end;
go

-- / Organization.AddEnterpriseAddress
IF OBJECT_ID ('organization.add_enterprise_address', 'P') IS NOT NULL
	DROP PROCEDURE organization.add_enterprise_address;
GO

CREATE PROCEDURE organization.add_enterprise_address
(
	@enterprise_id UNIQUEIDENTIFIER,
	@line1 nvarchar(70) = '',
	@line2 nvarchar(150),
	@commune_id uniqueidentifier,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	declare @op table ( id uniqueidentifier );
	set @id = (select id from organization.enterprise_address where enterprise_id = @enterprise_id and line1 = @line1 and commune_id = @commune_id);

	if @id is null
	begin
		insert into organization.enterprise_address (line1, line2, enterprise_id, commune_id) output inserted.id into @op values
		(@line1, @line2, @enterprise_id, @commune_id);
		set @id = (select id from @op);
	end;
END
GO

IF OBJECT_ID ('organization.add_tenant_address', 'P') IS NOT NULL
	DROP PROCEDURE organization.add_tenant_address;
GO

CREATE PROCEDURE organization.add_tenant_address
(
	@tenant_id UNIQUEIDENTIFIER,
	@line1 nvarchar(75),
	@line2 nvarchar(150) = '',
	@commune_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	declare @op table ( id uniqueidentifier );
	set @id = (select id from organization.tenant_address where tenant_id = @tenant_id and line1 = @line1 and commune_id = @commune_id);

	if @id is null
	begin
		insert into organization.tenant_address (line1, line2, tenant_id, commune_id) output inserted.id into @op values
		(@line1, @line2, @tenant_id, @commune_id);
		set @id = (select id from @op);
	end;
END
GO

IF OBJECT_ID ('organization.set_enterprise_address_type', 'P') is not null
	drop procedure organization.set_enterprise_address_type;
GO

create procedure organization.set_enterprise_address_type
(
	@address_role_id uniqueidentifier,
	@enterprise_address_id uniqueidentifier,
	@id uniqueidentifier = null out
)
as
begin
	set @id = (select id from organization.enterprise_address_type where enterprise_address_id = @enterprise_address_id and address_type_id = @address_role_id);

	if (@id is null)
	begin
		insert into organization.enterprise_address_type (enterprise_address_id, address_type_id) values (@enterprise_address_id, @address_role_id);
		set @id = (select id from organization.enterprise_address_type where enterprise_address_id = @enterprise_address_id and address_type_id = @address_role_id);
	end
end;
go

IF OBJECT_ID ('organization.set_tenant_address_type', 'P') is not null
	drop procedure organization.set_tenant_address_type;
GO

create procedure organization.set_tenant_address_type
(
	@address_role_id uniqueidentifier,
	@tenant_address_id uniqueidentifier,
	@id uniqueidentifier = null out
)
as
begin
	set @id = (select id from organization.tenant_address_type where tenant_address_id = @tenant_address_id and address_type_id = @address_role_id);

	if (@id is null)
	begin
		insert into organization.tenant_address_type (tenant_address_id, address_type_id) values (@tenant_address_id, @address_role_id);
		set @id = (select id from organization.tenant_address_type where tenant_address_id = @tenant_address_id and address_type_id = @address_role_id);
	end
end;
go

--if object_id ('taxation.set_tenant_folios', 'P') is not null
--	drop procedure taxation.set_tenant_folios;
--go

--create procedure taxation.set_tenant_folios
--(
--	@tenant_id uniqueidentifier,
--	@document_type nvarchar(6),
--	@start_at bigint = 1
--)
--as
--begin
--	declare @id uniqueidentifier = (select id from taxation.tax_document_folio where document_type = @document_type and tenant_id = @tenant_id);

--	if (@id is null)
--	begin
--		insert into taxation.tax_document_folio (document_type, tenant_id, folio_number, unused_folios) values
--		(@document_type, @tenant_id, @start_at, '');
--	end;
--end;
--go

if object_id ('system.add_person', 'P') is not null
	drop procedure system.add_person;
go

create procedure system.add_person
(
	@first_name NVARCHAR(100),
	@last_name NVARCHAR(100),
	@national_id NVARCHAR(20),
	@birth_date DATE = NULL,
	@gender tinyint = 0,
	@id uniqueidentifier = null out
)
as
begin
	set @id = (select id from system.person where national_id = @national_id);

	if @id is null
	begin
		if @gender is null
			set @gender = 0;

		insert into system.person (first_name, last_name, national_id, birth_date, gender) values (@first_name, @last_name, @national_id, @birth_date, @gender);
		set @id = (select id from system.person where national_id = @national_id);
	end;
end;
go

if object_id ('system.create_account', 'P') is not null
	drop procedure system.create_account;
go

create procedure system.create_account
(
	@login nvarchar(100),
	@email varchar(1000) = null,
	@password binary(128),
	@salt binary(500),
	@person_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from system.account where id = @person_id);

	if @id is null
	begin
		insert into system.account ([login], email, [password], salt, id) values (@login, @email, @password, @salt, @person_id);
		set @id = (select id from system.account where id = @person_id);
	end;
end;
go

if object_id ('system.authorize_account', 'P') is not null
	drop procedure system.authorize_account;
go

create procedure system.authorize_account
(
	@tenant_id uniqueidentifier,
	@account_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from system.authorized_account where tenant_id = @tenant_id and account_id = @account_id);

	if @id is null
	begin
		insert into system.authorized_account (tenant_id, account_id) values (@tenant_id, @account_id);
		set @id = (select id from system.authorized_account where tenant_id = @tenant_id and account_id = @account_id);
	end;
end;
go

-- Add Tenant Economic Activities
if object_id ('taxation.add_tenant_economic_activity', 'P') is not null
	drop procedure taxation.add_tenant_economic_activity;
go

create procedure taxation.add_tenant_economic_activity
(
	@code varchar(6),
	@tenant_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	declare @economic_activity_id uniqueidentifier = (select id from taxation.economic_activity where code = @code);

	if (@economic_activity_id is not null)
	begin
		set @id = (select id from taxation.tenant_economic_activity where tenant_id = @tenant_id and economic_activity_id = @economic_activity_id);

		if (@id is null)
		begin
			insert into taxation.tenant_economic_activity (tenant_id, economic_activity_id) values (@tenant_id, @economic_activity_id);
			set @id = (select id from taxation.tenant_economic_activity where tenant_id = @tenant_id and economic_activity_id = @economic_activity_id);
		end;
	end;
	else
	begin
		print N'There is not economic activity with code ' + @code;
	end;
end;
go

if object_id ('taxation.add_tenant_comercial_activity', 'P') is not null
	drop procedure taxation.add_tenant_comercial_activity;
go

create procedure taxation.add_tenant_comercial_activity
(
	@name nvarchar(200),
	@tenant_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	declare @comercial_activity uniqueidentifier = (select id from taxation.comercial_activity where name = @name);

	if (@comercial_activity is not null)
	begin
		set @id = (select id from taxation.tenant_comercial_activity where tenant_id = @tenant_id and comercial_activity_id = @comercial_activity);

		if(@id is null)
		begin
			insert into taxation.tenant_comercial_activity (tenant_id, comercial_activity_id) values (@tenant_id, @comercial_activity);
			set @id = (select id from taxation.tenant_comercial_activity where tenant_id = @tenant_id and comercial_activity_id = @comercial_activity);
		end;
	end;
	else
	begin
		print N'There is not economic activity with name ' + @name;
	end;
end;
go

-- Add Enterprise Economic Activities
if object_id ('taxation.add_enterprise_economic_activity', 'P') is not null
	drop procedure taxation.add_enterprise_economic_activity;
go

create procedure taxation.add_enterprise_economic_activity
(
	@code varchar(6),
	@enterprise_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	declare @economic_activity_id uniqueidentifier = (select id from taxation.economic_activity where code = @code);

	if (@economic_activity_id is not null)
	begin
		set @id = (select id from taxation.enterprise_economic_activity where enterprise_id = @enterprise_id and economic_activity_id = @economic_activity_id);

		if (@id is null)
		begin
			insert into taxation.enterprise_economic_activity (enterprise_id, economic_activity_id) values (@enterprise_id, @economic_activity_id);
			set @id = (select id from taxation.enterprise_economic_activity where enterprise_id = @enterprise_id and economic_activity_id = @economic_activity_id);
		end;
	end;
	else
	begin
		print N'There is not economic activity with code ' + @code;
	end;
end;
go

-- Add Enterprise Comercial Activities
--if object_id ('taxation.add_enterprise_comercial_activity', 'P') is not null
--	drop procedure taxation.add_enterprise_comercial_activity;
--go

--create procedure taxation.add_enterprise_comercial_activity
--(
--	@name nvarchar(200),
--	@enterprise_id uniqueidentifier,
--	@id uniqueidentifier = null out
--) as
--begin
--	declare @comercial_activity uniqueidentifier = (select id from taxation.comercial_activity where name = @name);

--	if (@comercial_activity is not null)
--	begin
--		set @id = (select id from taxation.enterprise_comercial_activity where enterprise_id = @enterprise_id and comercial_activity_id = @comercial_activity);

--		if (@id is null)
--		begin
--			insert into taxation.enterprise_comercial_activity (enterprise_id, comercial_activity_id) values (@enterprise_id, @comercial_activity);
--			set @id = (select id from taxation.enterprise_comercial_activity where enterprise_id = @enterprise_id and comercial_activity_id = @comercial_activity);
--		end;
--	end;
--	else
--	begin
--		print N'There is not economic activity with code ' + @name;
--	end;
--end;
--go

------------------- Add Economic Activities
if object_id ('taxation.add_economic_activity', 'P') is not null
	drop procedure taxation.add_economic_activity;
go

create procedure taxation.add_economic_activity
(
	@name nvarchar(110),
	@code varchar(6),
	@is_subject_to_tax BIT = 1,
	@tributary_category TINYINT NULL,
	@economic_activity_group_id uniqueidentifier NULL,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.economic_activity where name = @name and code = @code);

	if (@id is null)
	begin
		insert into taxation.economic_activity (name, code, is_subject_to_tax, tributary_category, economic_activity_group_id)
		values (@name, @code, @is_subject_to_tax, @tributary_category, @economic_activity_group_id);
		set @id = (select id from taxation.economic_activity where name = @name and code = @code);
	end;
end;
go

------------------- Add Comercial Activities
if object_id ('taxation.add_comercial_activity', 'P') is not null
	drop procedure taxation.add_comercial_activity;
go

create procedure taxation.add_comercial_activity
(
	@name nvarchar(200),
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.comercial_activity where name = @name);

	if (@id is null)
	begin
		insert into taxation.comercial_activity (name) values (@name);
		set @id = (select id from taxation.comercial_activity where name = @name);
	end;
end;
go

if object_id ('system.create_permission', 'P') is not null
	drop procedure [system].create_permission;
go

create procedure [system].create_permission
(
	@name varchar(100),
	@desc nvarchar(300),
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from [system].permission where name = @name);

	if (@id is null)
	begin
		insert into [system].permission (name, [description]) values (@name, @desc);
		set @id = (select id from [system].permission where name = @name);
	end;
end;
go

if object_id('taxation.add_folio_range', 'P') is not null
	drop procedure taxation.add_folio_range;
go

create procedure taxation.add_folio_range
(
	@start bigint,
	@end bigint,
	@doctype nvarchar(6),
	@is_current_range bit = 0,
	@request_date date,
	@tenant_id uniqueidentifier,
	@status tinyint = 1,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.folio_range where tenant_id = @tenant_id and document_type = @doctype and range_start = @start);

	if (@id is null) 
	begin
		insert into taxation.folio_range (tenant_id, range_start, range_end, document_type, request_date, created_date, unused_folios, [status], is_current_range, current_folio) values
		(@tenant_id, @start, @end, @doctype, @request_date, getdate(), '', @status, @is_current_range, @start);
		set @id = (select id from taxation.folio_range where tenant_id = @tenant_id and document_type = @doctype and range_start = @start);
	end;
end;
go

if object_id('system.create_module', 'P') is not null
	drop procedure system.create_module;
go

create procedure system.create_module
(
	@name varchar(100),
	@description nvarchar(250),
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from system.module where name = @name);

	if (@id is null)
	begin
		insert into system.module (name, [description]) values (@name, @description);
		set @id = (select id from system.module where name = @name);
	end;
end;
go

if object_id ('system.deprecate_module', 'P') is not null
	drop procedure system.deprecate_module;
go

create procedure system.deprecate_module
(
	@module_id uniqueidentifier = null,
	@name varchar(100) = null
) as 
begin
	if (@module_id is not null)
	begin
		update system.module set is_deprecated = 1 where id = @module_id;
	end;
	else if (@name is not null)
	begin
		begin try
			set @module_id = (select id from system.module where name = @name);
			exec system.deprecate_module @module_id, null;
		end try
		begin catch
		end catch;
	end;
end;
go

if object_id ('system.add_tenant_module', 'P') is not null
	drop procedure system.add_tenant_module;
go

create procedure system.add_tenant_module
(
	@module_id uniqueidentifier,
	@tenant_id uniqueidentifier,
	@is_enabled bit = 1,
	@settings nvarchar(max) = '',
	@version varchar(100) = '1.0.0',
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from system.tenant_module where tenant_id = @tenant_id and module_id = @module_id);

	if (@id is null)
	begin
		insert into system.tenant_module (tenant_id, module_id, is_enabled, settings, [version]) values (@tenant_id, @module_id, @is_enabled, @settings, @version);
		set @id = (select id from system.tenant_module where tenant_id = @tenant_id and module_id = @module_id);
	end;
end;
go