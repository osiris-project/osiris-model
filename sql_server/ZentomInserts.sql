﻿--USE zentom_mssql;

INSERT INTO location.country (id, name, code, phone_code) VALUES
(N'54922a04-f199-e511-80c6-080027018225', 'Chile', 'CL', 56);

------------------------------------------ Permissions
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'55922a04-f199-e511-80c6-080027018225', N'account-create', N'Permite crear cuentas de usuario')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'56922a04-f199-e511-80c6-080027018225', N'account-read', N'Permite ver la información de las cuentas de usuario')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'57922a04-f199-e511-80c6-080027018225', N'account-update', N'Permite modificar la información de las cuentas de usuario')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'58922a04-f199-e511-80c6-080027018225', N'account-delete', N'Permite eliminar las cuentas de usuario')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'59922a04-f199-e511-80c6-080027018225', N'account-list', N'Permite ver una lista de las cuentas de usuario')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'5a922a04-f199-e511-80c6-080027018225', N'bill-create', N'Permite generar facturas')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'5b922a04-f199-e511-80c6-080027018225', N'bill-read', N'Permite ver las facturas')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'5c922a04-f199-e511-80c6-080027018225', N'bill-update', N'Permite modificar las facturas')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'5d922a04-f199-e511-80c6-080027018225', N'bill-delete', N'Permite eliminar las facturas')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'5e922a04-f199-e511-80c6-080027018225', N'bill-list', N'Permite ver una lista de las facturas')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'5f922a04-f199-e511-80c6-080027018225', N'document-type-create', N'Permite crear tipos de factura')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'60922a04-f199-e511-80c6-080027018225', N'document-type-read', N'Permite ver los tipos de factura')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'61922a04-f199-e511-80c6-080027018225', N'document-type-update', N'Permite modificar los tipos de factura')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'62922a04-f199-e511-80c6-080027018225', N'document-type-delete', N'Permite eliminar los tipos de factura')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'63922a04-f199-e511-80c6-080027018225', N'document-type-list', N'Permite ver una lista de los tipos de factura')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'64922a04-f199-e511-80c6-080027018225', N'city-create', N'Permite añadir nuevas ciudades a la base de datos')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'65922a04-f199-e511-80c6-080027018225', N'city-read', N'Permite ver la información de las ciudades')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'66922a04-f199-e511-80c6-080027018225', N'city-update', N'Permite modificar una ciudad')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'67922a04-f199-e511-80c6-080027018225', N'city-delete', N'Permite eliminar una ciudad')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'68922a04-f199-e511-80c6-080027018225', N'city-list', N'Permite ver una lista de las ciudades')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'69922a04-f199-e511-80c6-080027018225', N'client-create', N'Permite añadir nuevos clientes')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'6a922a04-f199-e511-80c6-080027018225', N'client-read', N'Permite ver la información del cliente')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'6b922a04-f199-e511-80c6-080027018225', N'client-update', N'Permite modificar la información del cliente')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'6c922a04-f199-e511-80c6-080027018225', N'client-delete', N'Permite eliminar un cliente')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'6d922a04-f199-e511-80c6-080027018225', N'client-list', N'Permite ver una lista de los clientes')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'6e922a04-f199-e511-80c6-080027018225', N'country-create', N'Permite añadir nuevos países')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'6f922a04-f199-e511-80c6-080027018225', N'country-read', N'Permite ver la información del país')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'70922a04-f199-e511-80c6-080027018225', N'country-update', N'Permite modificar la información del país')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'71922a04-f199-e511-80c6-080027018225', N'country-delete', N'Permite eliminar un país')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'72922a04-f199-e511-80c6-080027018225', N'country-list', N'Permite ver una lista de los países')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'73922a04-f199-e511-80c6-080027018225', N'employee-create', N'Permite añadir un nuevo Trabajador')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'74922a04-f199-e511-80c6-080027018225', N'employee-read', N'Permite ver la información del Trabajador')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'75922a04-f199-e511-80c6-080027018225', N'employee-update', N'Permite modificar un Trabajador')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'76922a04-f199-e511-80c6-080027018225', N'employee-delete', N'Permite eliminar un Trabajador')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'77922a04-f199-e511-80c6-080027018225', N'employee-list', N'Permite ver una lista de Trabajadores')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'78922a04-f199-e511-80c6-080027018225', N'enterprise-create', N'Permite añadir una nueva Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'79922a04-f199-e511-80c6-080027018225', N'enterprise-read', N'Permite ver la información de la Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'7a922a04-f199-e511-80c6-080027018225', N'enterprise-update', N'Permite modificar una Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'7b922a04-f199-e511-80c6-080027018225', N'enterprise-delete', N'Permite eliminar una Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'7c922a04-f199-e511-80c6-080027018225', N'enterprise-list', N'Permite ver una lista de la Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'7d922a04-f199-e511-80c6-080027018225', N'enterpriseaddress-create', N'Permite añadir una nueva Dirección de Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'7e922a04-f199-e511-80c6-080027018225', N'enterpriseaddress-read', N'Permite ver la información de la Dirección de Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'7f922a04-f199-e511-80c6-080027018225', N'enterpriseaddress-update', N'Permite modificar la Dirección de Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'80922a04-f199-e511-80c6-080027018225', N'enterpriseaddress-delete', N'Permite eliminar la Dirección de Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'81922a04-f199-e511-80c6-080027018225', N'enterpriseaddress-list', N'Permite ver una lista de Direcciones de Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'82922a04-f199-e511-80c6-080027018225', N'enterprisecontactdata-create', N'Permite añadir un nuevo Dato de Contacto de una Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'83922a04-f199-e511-80c6-080027018225', N'enterprisecontactdata-read', N'Permite ver la información de Contacto de una Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'84922a04-f199-e511-80c6-080027018225', N'enterprisecontactdata-update', N'Permite modificar un Dato de Contacto de una Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'85922a04-f199-e511-80c6-080027018225', N'enterprisecontactdata-delete', N'Permite eliminar un Dato de Contacto de una Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'86922a04-f199-e511-80c6-080027018225', N'enterprisecontactdata-list', N'Permite ver una lista de Datos de Contacto de una Empresa')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'8c922a04-f199-e511-80c6-080027018225', N'option-create', N'Permite añadir una nueva Opción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'8d922a04-f199-e511-80c6-080027018225', N'option-read', N'Permite ver la información de una Opción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'8e922a04-f199-e511-80c6-080027018225', N'option-update', N'Permite modificar una Opción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'8f922a04-f199-e511-80c6-080027018225', N'option-delete', N'Permite eliminar una Opción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'90922a04-f199-e511-80c6-080027018225', N'option-list', N'Permite ver una lista de Opciones')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'91922a04-f199-e511-80c6-080027018225', N'payment-create', N'Permite añadir un nuevo Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'92922a04-f199-e511-80c6-080027018225', N'payment-read', N'Permite ver la información de Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'93922a04-f199-e511-80c6-080027018225', N'payment-update', N'Permite modificar un Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'94922a04-f199-e511-80c6-080027018225', N'payment-delete', N'Permite eliminar un Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'95922a04-f199-e511-80c6-080027018225', N'payment-list', N'Permite ver una lista de Pagos')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'96922a04-f199-e511-80c6-080027018225', N'paymenttype-create', N'Permite añadir un nuevo Tipo de Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'97922a04-f199-e511-80c6-080027018225', N'paymenttype-read', N'Permite ver la información de Tipo de Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'98922a04-f199-e511-80c6-080027018225', N'paymenttype-update', N'Permite modificar un Tipo de Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'99922a04-f199-e511-80c6-080027018225', N'paymenttype-delete', N'Permite eliminar un Tipo de Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'9a922a04-f199-e511-80c6-080027018225', N'paymenttype-list', N'Permite ver una lista de Tipos de Pago')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'9b922a04-f199-e511-80c6-080027018225', N'permission-create', N'Permite añadir un nuevo Permiso')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'9c922a04-f199-e511-80c6-080027018225', N'permission-read', N'Permite ver la información de Permiso')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'9d922a04-f199-e511-80c6-080027018225', N'permission-update', N'Permite modificar un Permiso')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'9e922a04-f199-e511-80c6-080027018225', N'permission-delete', N'Permite eliminar un Permiso')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'9f922a04-f199-e511-80c6-080027018225', N'permission-list', N'Permite ver una lista de Permisos')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a0922a04-f199-e511-80c6-080027018225', N'product-create', N'Permite añadir un nuevo Producto')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a1922a04-f199-e511-80c6-080027018225', N'product-read', N'Permite ver la información de Producto')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a2922a04-f199-e511-80c6-080027018225', N'product-update', N'Permite modificar un Producto')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a3922a04-f199-e511-80c6-080027018225', N'product-delete', N'Permite eliminar un Producto')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a4922a04-f199-e511-80c6-080027018225', N'product-list', N'Permite ver una lista de Productos')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a5922a04-f199-e511-80c6-080027018225', N'reductiontype-create', N'Permite añadir un nuevo Tipo de Reducción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a6922a04-f199-e511-80c6-080027018225', N'reductiontype-read', N'Permite ver la información de Tipo de Reducción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a7922a04-f199-e511-80c6-080027018225', N'reductiontype-update', N'Permite modificar un Tipo de Reducción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a8922a04-f199-e511-80c6-080027018225', N'reductiontype-delete', N'Permite eliminar un Tipo de Reducción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'a9922a04-f199-e511-80c6-080027018225', N'reductiontype-list', N'Permite ver una lista de Tipos de Reducción')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'aa922a04-f199-e511-80c6-080027018225', N'role-create', N'Permite añadir un nuevo Rol')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'ab922a04-f199-e511-80c6-080027018225', N'role-read', N'Permite ver la información del Rol')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'ac922a04-f199-e511-80c6-080027018225', N'role-update', N'Permite modificar un Rol')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'ad922a04-f199-e511-80c6-080027018225', N'role-delete', N'Permite eliminar un Rol')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'ae922a04-f199-e511-80c6-080027018225', N'role-list', N'Permite ver una lista de Roles')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'af922a04-f199-e511-80c6-080027018225', N'service-create', N'Permite añadir un nuevo Servicio')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'b0922a04-f199-e511-80c6-080027018225', N'service-read', N'Permite ver la información de Servicio')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'b1922a04-f199-e511-80c6-080027018225', N'service-update', N'Permite modificar un Servicio')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'b2922a04-f199-e511-80c6-080027018225', N'service-delete', N'Permite eliminar un Servicio')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'b3922a04-f199-e511-80c6-080027018225', N'service-list', N'Permite ver una lista de Servicios')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (N'b4922a04-f199-e511-80c6-080027018225', N'home-index', N'Permite acceder a la página principal')
GO

----- Tax Documents
INSERT [system].[permission] ([id], [name], [description]) VALUES (NEWID(), N'tax-document-create', N'Permite crear nuevos Documentos Tributarios')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (NEWID(), N'tax-document-read', N'Permite ver información detallada de los Documentos Tributarios')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (NEWID(), N'tax-document-update', N'Permite modificar la información del Documentos Tributarios')
GO
-- Cannot delete these entries.
--INSERT [system].[permission] ([id], [name], [description]) VALUES (NEWID(), N'tax-document-delete', N'Permite eliminar Centros de Costo')
--GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (NEWID(), N'tax-document-list', N'Permite ver la lista de Documentos Tributarioss')
GO

----- Received Tax Documents
INSERT [system].[permission] ([id], [name], [description]) VALUES (NEWID(), N'received-tax-document-create', N'Permite cargar archivos de Documentos Tributarioss recibidos')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (NEWID(), N'received-tax-document-read', N'Permite ver información detallada de los Documentos Tributarios recibidos')
GO
INSERT [system].[permission] ([id], [name], [description]) VALUES (NEWID(), N'received-tax-document-update', N'Permite modificar la información de los Documentos Tributarios recibidos')
GO

----- Folio Ranges
exec [system].create_permission 'folio-range-create', 'Permite agregar un rango de folios';
exec [system].create_permission 'folio-range-read', 'Permite al usuario ver la información detallada de un rango de folios';
exec [system].create_permission 'folio-range-update', 'Permite la modificación de un rango de folios';

----- Accounting Permissions
exec system.create_permission 'accounting-voucher-create', 'Permite crear comprobantes contables';
exec system.create_permission 'accounting-voucher-read', 'Permite ver información detallada del comprobante';
exec system.create_permission 'accounting-voucher-update-status', 'Permite cambiar el estado del comprobante';
exec system.create_permission 'accounting-voucher-export', 'Permite al usuario exportar los comprobantes a algún sistema externo';

----- Accounting Accounts
exec system.create_permission 'accounting-account-read', 'Permite ver las cuentas contables';
exec system.create_permission 'accounting-account-create', 'Permite crear cuentas contables';
exec system.create_permission 'accounting-account-update', 'Permite modificar una cuenta contable';
exec system.create_permission 'accounting-account-delete', 'Permite eliminar cuentas contables';

----- Costs Centers
exec system.create_permission 'costs-center-read', 'Permite ver los centros de costo';
exec system.create_permission 'costs-center-create', 'Permite crear centros de costo';
exec system.create_permission 'costs-center-update', 'Permite modificar un centro de costo';
exec system.create_permission 'costs-center-delete', 'Permite eliminar centros de costo';

------------------------------------------ Cities
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'da922a04-f199-e511-80c6-080027018225', N'Aisén', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'de922a04-f199-e511-80c6-080027018225', N'Antártica Chilena', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'b7922a04-f199-e511-80c6-080027018225', N'Antofagasta', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd0922a04-f199-e511-80c6-080027018225', N'Arauco', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e9922a04-f199-e511-80c6-080027018225', N'Arica', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd1922a04-f199-e511-80c6-080027018225', N'Biobío', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c8922a04-f199-e511-80c6-080027018225', N'Cachapoal', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'db922a04-f199-e511-80c6-080027018225', N'Capitán Prat', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c9922a04-f199-e511-80c6-080027018225', N'Cardenal Caro', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'cc922a04-f199-e511-80c6-080027018225', N'Cauquenes', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd3922a04-f199-e511-80c6-080027018225', N'Cautín', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e3922a04-f199-e511-80c6-080027018225', N'Chacabuco', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'bb922a04-f199-e511-80c6-080027018225', N'Chañaral', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd6922a04-f199-e511-80c6-080027018225', N'Chiloé', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'be922a04-f199-e511-80c6-080027018225', N'Choapa', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd9922a04-f199-e511-80c6-080027018225', N'Coihaique', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'ca922a04-f199-e511-80c6-080027018225', N'Colchagua', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'cf922a04-f199-e511-80c6-080027018225', N'Concepción', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'ba922a04-f199-e511-80c6-080027018225', N'Copiapó', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e2922a04-f199-e511-80c6-080027018225', N'Cordillera', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'cd922a04-f199-e511-80c6-080027018225', N'Curicó', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'b8922a04-f199-e511-80c6-080027018225', N'El Loa', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'bd922a04-f199-e511-80c6-080027018225', N'Elqui', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'dc922a04-f199-e511-80c6-080027018225', N'General Carrera', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'bc922a04-f199-e511-80c6-080027018225', N'Huasco', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'b5922a04-f199-e511-80c6-080027018225', N'Iquique', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c1922a04-f199-e511-80c6-080027018225', N'Isla de Pascua', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'bf922a04-f199-e511-80c6-080027018225', N'Limarí', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'ce922a04-f199-e511-80c6-080027018225', N'Linares', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd5922a04-f199-e511-80c6-080027018225', N'Llanquihue', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c2922a04-f199-e511-80c6-080027018225', N'Los Andes', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'dd922a04-f199-e511-80c6-080027018225', N'Magallanes', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e4922a04-f199-e511-80c6-080027018225', N'Maipo', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd4922a04-f199-e511-80c6-080027018225', N'Malleco', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c7922a04-f199-e511-80c6-080027018225', N'Marga Marga', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e5922a04-f199-e511-80c6-080027018225', N'Melipilla', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd2922a04-f199-e511-80c6-080027018225', N'Ñuble', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd7922a04-f199-e511-80c6-080027018225', N'Osorno', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'd8922a04-f199-e511-80c6-080027018225', N'Palena', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'ea922a04-f199-e511-80c6-080027018225', N'Parinacota', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c3922a04-f199-e511-80c6-080027018225', N'Petorca', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c4922a04-f199-e511-80c6-080027018225', N'Quillota', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e8922a04-f199-e511-80c6-080027018225', N'Ranco', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c5922a04-f199-e511-80c6-080027018225', N'San Antonio', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c6922a04-f199-e511-80c6-080027018225', N'San Felipe de Aconcagua', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e1922a04-f199-e511-80c6-080027018225', N'Santiago', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e6922a04-f199-e511-80c6-080027018225', N'Talagante', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'cb922a04-f199-e511-80c6-080027018225', N'Talca', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'b6922a04-f199-e511-80c6-080027018225', N'Tamarugal', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'df922a04-f199-e511-80c6-080027018225', N'Tierra del Fuego', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'b9922a04-f199-e511-80c6-080027018225', N'Tocopilla', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e0922a04-f199-e511-80c6-080027018225', N'Última Esperanza', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'e7922a04-f199-e511-80c6-080027018225', N'Valdivia', N'54922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[city] ([id], [name], [country_id]) VALUES (N'c0922a04-f199-e511-80c6-080027018225', N'Valparaíso', N'54922a04-f199-e511-80c6-080027018225')
GO
insert location.city (name, country_id) values ('Monte Águila', N'54922a04-f199-e511-80c6-080027018225');
insert location.city (name, country_id) values ('San Fernando', N'54922a04-f199-e511-80c6-080027018225');
insert location.city (name, country_id) values ('Algarrobo', N'54922a04-f199-e511-80c6-080027018225');
insert location.city (name, country_id) values ('Til Til', N'54922a04-f199-e511-80c6-080027018225');
insert location.city (name, country_id) values ('Rancagua', N'54922a04-f199-e511-80c6-080027018225');
insert location.city (name, country_id) values ('Casa Blanca', N'54922a04-f199-e511-80c6-080027018225');
insert location.city (name, country_id) values ('Villarrica', N'54922a04-f199-e511-80c6-080027018225');

------------------------------------------ Communes
insert into [location].commune (name, post_code, city_id) values ('Curauma', '', (select id from location.city where name = 'Valparaíso'));
insert into [location].commune (name, post_code, city_id) values ('Monte Águila', '', (select id from location.city where name = 'Monte Águila'));
insert into [location].commune (name, post_code, city_id) values ('Tinguiririca', '', (select id from location.city where name = 'San Fernando'));
insert into [location].commune (name, post_code, city_id) values ('Algarrobo', '', (select id from location.city where name = 'Algarrobo'));
insert into [location].commune (name, post_code, city_id) values ('Til Til', '', (select id from location.city where name = 'Til Til'));
insert location.commune (name, post_code, city_id) values ('San Fco. de Mostazal', '', (select id from location.city where name = 'Rancagua'));
insert location.commune (name, post_code, city_id) values ('Casa Blanca', '', (select id from location.city where name = 'Casa Blanca'));
insert location.commune (name, post_code, city_id) values ('Villarrica', '', (select id from location.city where name = 'Villarrica'));

INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'eb922a04-f199-e511-80c6-080027018225', N'Iquique', N'1100000', N'b5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ec922a04-f199-e511-80c6-080027018225', N'Alto Hospicio', N'1130000', N'b5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ed922a04-f199-e511-80c6-080027018225', N'Pozo Almonte', N'1180000', N'b6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ee922a04-f199-e511-80c6-080027018225', N'Camiña', N'1150000', N'b6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ef922a04-f199-e511-80c6-080027018225', N'Colchane', N'1160000', N'b6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f0922a04-f199-e511-80c6-080027018225', N'Huara', N'1140000', N'b6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f1922a04-f199-e511-80c6-080027018225', N'Pica', N'1170000', N'b6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f2922a04-f199-e511-80c6-080027018225', N'Antofagasta', N'1240000', N'b7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f3922a04-f199-e511-80c6-080027018225', N'Mejillones', N'1310000', N'b7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f4922a04-f199-e511-80c6-080027018225', N'Sierra Gorda', N'1320000', N'b7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f5922a04-f199-e511-80c6-080027018225', N'Taltal', N'1300000', N'b7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f6922a04-f199-e511-80c6-080027018225', N'Calama', N'1390000', N'b8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f7922a04-f199-e511-80c6-080027018225', N'Ollagüe', N'1420000', N'b8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f8922a04-f199-e511-80c6-080027018225', N'San Pedro de Atacama', N'1410000', N'b8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f9922a04-f199-e511-80c6-080027018225', N'Tocopilla', N'1340000', N'b9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fa922a04-f199-e511-80c6-080027018225', N'María Elena', N'1360000', N'b9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fb922a04-f199-e511-80c6-080027018225', N'Copiapó', N'1530000', N'ba922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fc922a04-f199-e511-80c6-080027018225', N'Caldera', N'1570000', N'ba922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fd922a04-f199-e511-80c6-080027018225', N'Tierra Amarilla', N'1580000', N'ba922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fe922a04-f199-e511-80c6-080027018225', N'Chañaral', N'1490000', N'bb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ff922a04-f199-e511-80c6-080027018225', N'Diego de Almagro', N'1500000', N'bb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'00932a04-f199-e511-80c6-080027018225', N'Vallenar', N'1610000', N'bc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'01932a04-f199-e511-80c6-080027018225', N'Alto del Carmen', N'1650000', N'bc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'02932a04-f199-e511-80c6-080027018225', N'Freirina', N'1630000', N'bc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'03932a04-f199-e511-80c6-080027018225', N'Huasco', N'1640000', N'bc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'04932a04-f199-e511-80c6-080027018225', N'La Serena', N'1700000', N'bd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'05932a04-f199-e511-80c6-080027018225', N'Coquimbo', N'1780000', N'bd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'06932a04-f199-e511-80c6-080027018225', N'Andacollo', N'1750000', N'bd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'07932a04-f199-e511-80c6-080027018225', N'La Higuera', N'1740000', N'bd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'08932a04-f199-e511-80c6-080027018225', N'Paihuano', N'1770000', N'bd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'09932a04-f199-e511-80c6-080027018225', N'Vicuña', N'1760000', N'bd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0a932a04-f199-e511-80c6-080027018225', N'Illapel', N'1930000', N'be922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0b932a04-f199-e511-80c6-080027018225', N'Canela', N'1960000', N'be922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0c932a04-f199-e511-80c6-080027018225', N'Los Vilos', N'1940000', N'be922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0d932a04-f199-e511-80c6-080027018225', N'Salamanca', N'1950000', N'be922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0e932a04-f199-e511-80c6-080027018225', N'Ovalle', N'1840000', N'bf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0f932a04-f199-e511-80c6-080027018225', N'Combarbalá', N'1890000', N'bf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'10932a04-f199-e511-80c6-080027018225', N'Monte Patria', N'1880000', N'bf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'11932a04-f199-e511-80c6-080027018225', N'Punitaqui', N'1900000', N'bf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'12932a04-f199-e511-80c6-080027018225', N'Río Hurtado', N'1870000', N'bf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'13932a04-f199-e511-80c6-080027018225', N'Valparaíso', N'2340000', N'c0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'14932a04-f199-e511-80c6-080027018225', N'Casablanca', N'2480000', N'c0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'15932a04-f199-e511-80c6-080027018225', N'Concón', N'2510000', N'c0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'16932a04-f199-e511-80c6-080027018225', N'Juan Fernández', N'2600000', N'c0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'17932a04-f199-e511-80c6-080027018225', N'Puchuncaví', N'2500000', N'c0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'18932a04-f199-e511-80c6-080027018225', N'Quintero', N'2490000', N'c0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'19932a04-f199-e511-80c6-080027018225', N'Viña del Mar', N'2520000', N'c0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1a932a04-f199-e511-80c6-080027018225', N'Isla de Pascua', N'2770000', N'c1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1b932a04-f199-e511-80c6-080027018225', N'Los Andes', N'2100000', N'c2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1c932a04-f199-e511-80c6-080027018225', N'Calle Larga', N'2130000', N'c2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1d932a04-f199-e511-80c6-080027018225', N'Rinconada', N'2140000', N'c2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1e932a04-f199-e511-80c6-080027018225', N'San Esteban', N'2120000', N'c2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1f932a04-f199-e511-80c6-080027018225', N'La Ligua', N'2030000', N'c3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'20932a04-f199-e511-80c6-080027018225', N'Cabildo', N'2050000', N'c3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'21932a04-f199-e511-80c6-080027018225', N'Papudo', N'2070000', N'c3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'22932a04-f199-e511-80c6-080027018225', N'Petorca', N'2040000', N'c3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'23932a04-f199-e511-80c6-080027018225', N'Zapallar', N'2060000', N'c3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'24932a04-f199-e511-80c6-080027018225', N'Quillota', N'2260000', N'c4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'25932a04-f199-e511-80c6-080027018225', N'La Calera', N'2290000', N'c4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'26932a04-f199-e511-80c6-080027018225', N'Hijuelas', N'2310000', N'c4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'27932a04-f199-e511-80c6-080027018225', N'La Cruz', N'2280000', N'c4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'28932a04-f199-e511-80c6-080027018225', N'Nogales', N'2300000', N'c4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'29932a04-f199-e511-80c6-080027018225', N'San Antonio', N'2660000', N'c5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2a932a04-f199-e511-80c6-080027018225', N'Algarrobo', N'2710000', N'c5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2b932a04-f199-e511-80c6-080027018225', N'Cartagena', N'2680000', N'c5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2c932a04-f199-e511-80c6-080027018225', N'El Quisco', N'2700000', N'c5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2d932a04-f199-e511-80c6-080027018225', N'El Tabo', N'2690000', N'c5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2e932a04-f199-e511-80c6-080027018225', N'Santo Domingo', N'2720000', N'c5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2f932a04-f199-e511-80c6-080027018225', N'San Felipe', N'2170000', N'c6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'30932a04-f199-e511-80c6-080027018225', N'Catemu', N'2230000', N'c6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'31932a04-f199-e511-80c6-080027018225', N'Llay Llay', N'2220000', N'c6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'32932a04-f199-e511-80c6-080027018225', N'Panquehue', N'2210000', N'c6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'33932a04-f199-e511-80c6-080027018225', N'Putaendo', N'2190000', N'c6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'34932a04-f199-e511-80c6-080027018225', N'Santa María', N'2200000', N'c6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'35932a04-f199-e511-80c6-080027018225', N'Quilpué', N'2430000', N'c7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'36932a04-f199-e511-80c6-080027018225', N'Limache', N'2240000', N'c7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'37932a04-f199-e511-80c6-080027018225', N'Olmué', N'2330000', N'c7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'38932a04-f199-e511-80c6-080027018225', N'Villa Alemana', N'6500000', N'c7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'39932a04-f199-e511-80c6-080027018225', N'Rancagua', N'2820000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3a932a04-f199-e511-80c6-080027018225', N'Codegua', N'2900000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3b932a04-f199-e511-80c6-080027018225', N'Coinco', N'3010000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3c932a04-f199-e511-80c6-080027018225', N'Coltauco', N'3000000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3d932a04-f199-e511-80c6-080027018225', N'Doñihue', N'3020000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3e932a04-f199-e511-80c6-080027018225', N'Graneros', N'2880000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3f932a04-f199-e511-80c6-080027018225', N'Las Cabras', N'3030000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'40932a04-f199-e511-80c6-080027018225', N'Machalí', N'2910000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'41932a04-f199-e511-80c6-080027018225', N'Malloa', N'2950000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'42932a04-f199-e511-80c6-080027018225', N'Mostazal', N'2890000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'43932a04-f199-e511-80c6-080027018225', N'Olivar', N'2920000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'44932a04-f199-e511-80c6-080027018225', N'Peumo', N'2990000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'45932a04-f199-e511-80c6-080027018225', N'Pichidegua', N'2980000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'46932a04-f199-e511-80c6-080027018225', N'Quinta de Tilcoco', N'2960000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'47932a04-f199-e511-80c6-080027018225', N'Rengo', N'2940000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'48932a04-f199-e511-80c6-080027018225', N'Requínoa', N'2930000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'49932a04-f199-e511-80c6-080027018225', N'San Vicente', N'2970000', N'c8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'4a932a04-f199-e511-80c6-080027018225', N'Pichilemu', N'3220000', N'c9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'4b932a04-f199-e511-80c6-080027018225', N'La Estrella', N'3250000', N'c9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'4c932a04-f199-e511-80c6-080027018225', N'Litueche', N'3240000', N'c9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'4d932a04-f199-e511-80c6-080027018225', N'Marchihue', N'3260000', N'c9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'4e932a04-f199-e511-80c6-080027018225', N'Navidad', N'3230000', N'c9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'4f932a04-f199-e511-80c6-080027018225', N'Paredones', N'3270000', N'c9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'50932a04-f199-e511-80c6-080027018225', N'San Fernando', N'3070000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'51932a04-f199-e511-80c6-080027018225', N'Chépica', N'3120000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'52932a04-f199-e511-80c6-080027018225', N'Chimbarongo', N'3090000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'53932a04-f199-e511-80c6-080027018225', N'Lolol', N'3140000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'54932a04-f199-e511-80c6-080027018225', N'Nancagua', N'3110000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'55932a04-f199-e511-80c6-080027018225', N'Palmilla', N'3160000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'56932a04-f199-e511-80c6-080027018225', N'Peralillo', N'3170000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'57932a04-f199-e511-80c6-080027018225', N'Placilla', N'3100000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'58932a04-f199-e511-80c6-080027018225', N'Pumanque', N'3150000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'59932a04-f199-e511-80c6-080027018225', N'Santa Cruz', N'3130000', N'ca922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'5a932a04-f199-e511-80c6-080027018225', N'Talca', N'3460000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'5b932a04-f199-e511-80c6-080027018225', N'Constitución', N'3560000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'5c932a04-f199-e511-80c6-080027018225', N'Curepto', N'3570000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'5d932a04-f199-e511-80c6-080027018225', N'Empedrado', N'3540000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'5e932a04-f199-e511-80c6-080027018225', N'Maule', N'3530000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'5f932a04-f199-e511-80c6-080027018225', N'Pelarco', N'3500000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'60932a04-f199-e511-80c6-080027018225', N'Pencahue', N'3550000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'61932a04-f199-e511-80c6-080027018225', N'Río Claro', N'3510000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'62932a04-f199-e511-80c6-080027018225', N'San Clemente', N'3520000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'63932a04-f199-e511-80c6-080027018225', N'San Rafael', N'3490000', N'cb922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'64932a04-f199-e511-80c6-080027018225', N'Cauquenes', N'3690000', N'cc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'65932a04-f199-e511-80c6-080027018225', N'Chanco', N'3720000', N'cc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'66932a04-f199-e511-80c6-080027018225', N'Pelluhue', N'3710000', N'cc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'67932a04-f199-e511-80c6-080027018225', N'Curicó', N'3340000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'68932a04-f199-e511-80c6-080027018225', N'Hualañé', N'3400000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'69932a04-f199-e511-80c6-080027018225', N'Licantén', N'3410000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'6a932a04-f199-e511-80c6-080027018225', N'Molina', N'3380000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'6b932a04-f199-e511-80c6-080027018225', N'Rauco', N'3430000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'6c932a04-f199-e511-80c6-080027018225', N'Romeral', N'3370000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'6d932a04-f199-e511-80c6-080027018225', N'Sagrada Familia', N'3390000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'6e932a04-f199-e511-80c6-080027018225', N'Teno', N'3360000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'6f932a04-f199-e511-80c6-080027018225', N'Vichuquén', N'3420000', N'cd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'70932a04-f199-e511-80c6-080027018225', N'Linares', N'3580000', N'ce922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'71932a04-f199-e511-80c6-080027018225', N'Colbún', N'3610000', N'ce922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'72932a04-f199-e511-80c6-080027018225', N'Longaví', N'3620000', N'ce922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'73932a04-f199-e511-80c6-080027018225', N'Parral', N'3630000', N'ce922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'74932a04-f199-e511-80c6-080027018225', N'Retiro', N'3640000', N'ce922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'75932a04-f199-e511-80c6-080027018225', N'San Javier', N'3660000', N'ce922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'76932a04-f199-e511-80c6-080027018225', N'Villa Alegre', N'3650000', N'ce922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'77932a04-f199-e511-80c6-080027018225', N'Yerbas Buenas', N'3600000', N'ce922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'78932a04-f199-e511-80c6-080027018225', N'Concepción', N'4030000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'79932a04-f199-e511-80c6-080027018225', N'Coronel', N'4190000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'7a932a04-f199-e511-80c6-080027018225', N'Chiguayante', N'4100000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'7b932a04-f199-e511-80c6-080027018225', N'Florida', N'4170000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'7c932a04-f199-e511-80c6-080027018225', N'Hualqui', N'4180000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'7d932a04-f199-e511-80c6-080027018225', N'Lota', N'4210000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'7e932a04-f199-e511-80c6-080027018225', N'Penco', N'4150000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'7f932a04-f199-e511-80c6-080027018225', N'San Pedro de la Paz', N'4130000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'80932a04-f199-e511-80c6-080027018225', N'Santa Juana', N'4230000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'81932a04-f199-e511-80c6-080027018225', N'Talcahuano', N'4260000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'82932a04-f199-e511-80c6-080027018225', N'Tomé', N'4160000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'83932a04-f199-e511-80c6-080027018225', N'Hualpén', N'4600000', N'cf922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'84932a04-f199-e511-80c6-080027018225', N'Lebu', N'4350000', N'd0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'85932a04-f199-e511-80c6-080027018225', N'Arauco', N'4360000', N'd0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'86932a04-f199-e511-80c6-080027018225', N'Cañete', N'4390000', N'd0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'87932a04-f199-e511-80c6-080027018225', N'Contulmo', N'4400000', N'd0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'88932a04-f199-e511-80c6-080027018225', N'Curanilahue', N'4370000', N'd0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'89932a04-f199-e511-80c6-080027018225', N'Los Álamos', N'4380000', N'd0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'8a932a04-f199-e511-80c6-080027018225', N'Tirúa', N'4410000', N'd0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'8b932a04-f199-e511-80c6-080027018225', N'Los Ángeles', N'4440000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'8c932a04-f199-e511-80c6-080027018225', N'Antuco', N'4490000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'8d932a04-f199-e511-80c6-080027018225', N'Cabrero', N'4470000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'8e932a04-f199-e511-80c6-080027018225', N'Laja', N'4560000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'8f932a04-f199-e511-80c6-080027018225', N'Mulchén', N'4530000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'90932a04-f199-e511-80c6-080027018225', N'Nacimiento', N'4550000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'91932a04-f199-e511-80c6-080027018225', N'Negrete', N'4540000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'92932a04-f199-e511-80c6-080027018225', N'Quilaco', N'4520000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'93932a04-f199-e511-80c6-080027018225', N'Quilleco', N'4500000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'94932a04-f199-e511-80c6-080027018225', N'San Rosendo', N'4570000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'95932a04-f199-e511-80c6-080027018225', N'Santa Bárbara', N'4510000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'96932a04-f199-e511-80c6-080027018225', N'Tucapel', N'4480000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'97932a04-f199-e511-80c6-080027018225', N'Yumbel', N'4580000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'98932a04-f199-e511-80c6-080027018225', N'Alto Biobío', N'4590000', N'd1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'99932a04-f199-e511-80c6-080027018225', N'Chillán', N'3780000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'9a932a04-f199-e511-80c6-080027018225', N'Bulnes', N'3930000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'9b932a04-f199-e511-80c6-080027018225', N'Cobquecura', N'3990000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'9c932a04-f199-e511-80c6-080027018225', N'Coelemu', N'3970000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'9d932a04-f199-e511-80c6-080027018225', N'Coihueco', N'3870000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'9e932a04-f199-e511-80c6-080027018225', N'Chillán Viejo', N'3820000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'9f932a04-f199-e511-80c6-080027018225', N'El Carmen', N'3900000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a0932a04-f199-e511-80c6-080027018225', N'Ninhue', N'4010000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a1932a04-f199-e511-80c6-080027018225', N'Ñiquén', N'3850000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a2932a04-f199-e511-80c6-080027018225', N'Pemuco', N'3910000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a3932a04-f199-e511-80c6-080027018225', N'Pinto', N'3880000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a4932a04-f199-e511-80c6-080027018225', N'Portezuelo', N'3960000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a5932a04-f199-e511-80c6-080027018225', N'Quillón', N'3940000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a6932a04-f199-e511-80c6-080027018225', N'Quirihue', N'4000000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a7932a04-f199-e511-80c6-080027018225', N'Ránquil', N'3950000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a8932a04-f199-e511-80c6-080027018225', N'San Carlos', N'3840000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'a9932a04-f199-e511-80c6-080027018225', N'San Fabián', N'3860000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'aa932a04-f199-e511-80c6-080027018225', N'San Ignacio', N'3890000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ab932a04-f199-e511-80c6-080027018225', N'San Nicolás', N'4020000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ac932a04-f199-e511-80c6-080027018225', N'Treguaco', N'3980000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ad932a04-f199-e511-80c6-080027018225', N'Yungay', N'3920000', N'd2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ae932a04-f199-e511-80c6-080027018225', N'Temuco', N'4780000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'af932a04-f199-e511-80c6-080027018225', N'Carahue', N'5010000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b0932a04-f199-e511-80c6-080027018225', N'Cunco', N'4890000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b1932a04-f199-e511-80c6-080027018225', N'Curarrehue', N'4910000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b2932a04-f199-e511-80c6-080027018225', N'Freire', N'4940000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b3932a04-f199-e511-80c6-080027018225', N'Galvarino', N'5030000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b4932a04-f199-e511-80c6-080027018225', N'Gorbea', N'4960000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b5932a04-f199-e511-80c6-080027018225', N'Lautaro', N'4860000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b6932a04-f199-e511-80c6-080027018225', N'Loncoche', N'4970000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b7932a04-f199-e511-80c6-080027018225', N'Melipeuco', N'4900000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b8932a04-f199-e511-80c6-080027018225', N'Nueva Imperial', N'5020000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'b9932a04-f199-e511-80c6-080027018225', N'Padre las Casas', N'4850000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ba932a04-f199-e511-80c6-080027018225', N'Perquenco', N'4870000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'bb932a04-f199-e511-80c6-080027018225', N'Pitrufquén', N'4950000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'bc932a04-f199-e511-80c6-080027018225', N'Pucón', N'4920000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'bd932a04-f199-e511-80c6-080027018225', N'Saavedra', N'5000000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'be932a04-f199-e511-80c6-080027018225', N'Teodoro Schmidt', N'4990000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'bf932a04-f199-e511-80c6-080027018225', N'Toltén', N'4980000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c0932a04-f199-e511-80c6-080027018225', N'Vilcún', N'4880000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c1932a04-f199-e511-80c6-080027018225', N'Villarrica', N'4930000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c2932a04-f199-e511-80c6-080027018225', N'Chol Chol', N'5040000', N'd3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c3932a04-f199-e511-80c6-080027018225', N'Angol', N'4650000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c4932a04-f199-e511-80c6-080027018225', N'Collipulli', N'4680000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c5932a04-f199-e511-80c6-080027018225', N'Curacautín', N'4700000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c6932a04-f199-e511-80c6-080027018225', N'Ercilla', N'4710000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c7932a04-f199-e511-80c6-080027018225', N'Lonquimay', N'4690000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c8932a04-f199-e511-80c6-080027018225', N'Los Sauces', N'4760000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'c9932a04-f199-e511-80c6-080027018225', N'Lumaco', N'4740000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ca932a04-f199-e511-80c6-080027018225', N'Purén', N'4750000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'cb932a04-f199-e511-80c6-080027018225', N'Renaico', N'4670000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'cc932a04-f199-e511-80c6-080027018225', N'Traiguén', N'4730000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'cd932a04-f199-e511-80c6-080027018225', N'Victoria', N'4720000', N'd4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ce932a04-f199-e511-80c6-080027018225', N'Puerto Montt', N'5480000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'cf932a04-f199-e511-80c6-080027018225', N'Calbuco', N'5570000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd0932a04-f199-e511-80c6-080027018225', N'Cochamó', N'5560000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd1932a04-f199-e511-80c6-080027018225', N'Fresia', N'5600000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd2932a04-f199-e511-80c6-080027018225', N'Frutillar', N'5620000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd3932a04-f199-e511-80c6-080027018225', N'Los Muermos', N'5590000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd4932a04-f199-e511-80c6-080027018225', N'Llanquihue', N'5610000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd5932a04-f199-e511-80c6-080027018225', N'Maullín', N'5580000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd6932a04-f199-e511-80c6-080027018225', N'Puerto Varas', N'5550000', N'd5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd7932a04-f199-e511-80c6-080027018225', N'Castro', N'5700000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd8932a04-f199-e511-80c6-080027018225', N'Ancud', N'5710000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'd9932a04-f199-e511-80c6-080027018225', N'Chonchi', N'5770000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'da932a04-f199-e511-80c6-080027018225', N'Curaco de Vélez', N'5740000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'db932a04-f199-e511-80c6-080027018225', N'Dalcahue', N'5730000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'dc932a04-f199-e511-80c6-080027018225', N'Puqueldón', N'5760000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'dd932a04-f199-e511-80c6-080027018225', N'Queilén', N'5780000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'de932a04-f199-e511-80c6-080027018225', N'Quellón', N'5790000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'df932a04-f199-e511-80c6-080027018225', N'Quemchi', N'5720000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e0932a04-f199-e511-80c6-080027018225', N'Quinchao', N'5750000', N'd6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e1932a04-f199-e511-80c6-080027018225', N'Osorno', N'5290000', N'd7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e2932a04-f199-e511-80c6-080027018225', N'Puerto Octay', N'5370000', N'd7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e3932a04-f199-e511-80c6-080027018225', N'Purranque', N'5380000', N'd7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e4932a04-f199-e511-80c6-080027018225', N'Puyehue', N'5360000', N'd7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e5932a04-f199-e511-80c6-080027018225', N'Río Negro', N'5390000', N'd7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e6932a04-f199-e511-80c6-080027018225', N'San Juan de la Costa', N'5400000', N'd7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e7932a04-f199-e511-80c6-080027018225', N'San Pablo', N'5350000', N'd7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e8932a04-f199-e511-80c6-080027018225', N'Chaitén', N'5850000', N'd8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'e9932a04-f199-e511-80c6-080027018225', N'Futaleufú', N'5870000', N'd8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ea932a04-f199-e511-80c6-080027018225', N'Hualaihué', N'5860000', N'd8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'eb932a04-f199-e511-80c6-080027018225', N'Palena', N'5880000', N'd8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ec932a04-f199-e511-80c6-080027018225', N'Coyhaique', N'5950000', N'd9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ed932a04-f199-e511-80c6-080027018225', N'Lago Verde', N'5960000', N'd9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ee932a04-f199-e511-80c6-080027018225', N'Aysén', N'6000000', N'da922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ef932a04-f199-e511-80c6-080027018225', N'Cisnes', N'6010000', N'da922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f0932a04-f199-e511-80c6-080027018225', N'Guaitecas', N'6020000', N'da922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f1932a04-f199-e511-80c6-080027018225', N'Cochrane', N'6100000', N'db922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f2932a04-f199-e511-80c6-080027018225', N'O''Higgins', N'6110000', N'db922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f3932a04-f199-e511-80c6-080027018225', N'Tortel', N'6120000', N'db922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f4932a04-f199-e511-80c6-080027018225', N'Chile Chico', N'6050000', N'dc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f5932a04-f199-e511-80c6-080027018225', N'Río Ibáñez', N'6060000', N'dc922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f6932a04-f199-e511-80c6-080027018225', N'Punta Arenas', N'6200000', N'dd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f7932a04-f199-e511-80c6-080027018225', N'Laguna Blanca', N'6250000', N'dd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f8932a04-f199-e511-80c6-080027018225', N'Río Verde', N'6240000', N'dd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'f9932a04-f199-e511-80c6-080027018225', N'San Gregorio', N'6260000', N'dd922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fa932a04-f199-e511-80c6-080027018225', N'Cabo de Hornos', N'6350000', N'de922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fb932a04-f199-e511-80c6-080027018225', N'Antártica', N'6360000', N'de922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fc932a04-f199-e511-80c6-080027018225', N'Porvenir', N'6300000', N'df922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fd932a04-f199-e511-80c6-080027018225', N'Primavera', N'6310000', N'df922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'fe932a04-f199-e511-80c6-080027018225', N'Timaukel', N'6320000', N'df922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'ff932a04-f199-e511-80c6-080027018225', N'Natales', N'6160000', N'e0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'00942a04-f199-e511-80c6-080027018225', N'Torres del Paine', N'6170000', N'e0922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'01942a04-f199-e511-80c6-080027018225', N'Santiago', N'8320000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'02942a04-f199-e511-80c6-080027018225', N'Cerrillos', N'9200000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'03942a04-f199-e511-80c6-080027018225', N'Cerro Navia', N'9080000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'04942a04-f199-e511-80c6-080027018225', N'Conchalí', N'8540000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'05942a04-f199-e511-80c6-080027018225', N'El Bosque', N'8010000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'06942a04-f199-e511-80c6-080027018225', N'Estación Central', N'9160000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'07942a04-f199-e511-80c6-080027018225', N'Huechuraba', N'8580000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'08942a04-f199-e511-80c6-080027018225', N'Independencia', N'8380000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'09942a04-f199-e511-80c6-080027018225', N'La Cisterna', N'7970000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0a942a04-f199-e511-80c6-080027018225', N'La Florida', N'8240000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0b942a04-f199-e511-80c6-080027018225', N'La Granja', N'8780000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0c942a04-f199-e511-80c6-080027018225', N'La Pintana', N'8820000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0d942a04-f199-e511-80c6-080027018225', N'La Reina', N'7850000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0e942a04-f199-e511-80c6-080027018225', N'Las Condes', N'7550000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'0f942a04-f199-e511-80c6-080027018225', N'Lo Barnechea', N'7690000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'10942a04-f199-e511-80c6-080027018225', N'Lo Espejo', N'9120000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'11942a04-f199-e511-80c6-080027018225', N'Lo Prado', N'8980000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'12942a04-f199-e511-80c6-080027018225', N'Macul', N'7810000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'13942a04-f199-e511-80c6-080027018225', N'Maipú', N'9250000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'14942a04-f199-e511-80c6-080027018225', N'Ñuñoa', N'7750000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'15942a04-f199-e511-80c6-080027018225', N'Pedro Aguirre Cerda', N'8460000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'16942a04-f199-e511-80c6-080027018225', N'Peñalolén', N'7910000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'17942a04-f199-e511-80c6-080027018225', N'Providencia', N'7500000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'18942a04-f199-e511-80c6-080027018225', N'Pudahuel', N'9020000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'19942a04-f199-e511-80c6-080027018225', N'Quilicura', N'8700000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1a942a04-f199-e511-80c6-080027018225', N'Quinta Normal', N'8500000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1b942a04-f199-e511-80c6-080027018225', N'Recoleta', N'8420000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1c942a04-f199-e511-80c6-080027018225', N'Renca', N'8640000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1d942a04-f199-e511-80c6-080027018225', N'San Joaquín', N'8940000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1e942a04-f199-e511-80c6-080027018225', N'San Miguel', N'8900000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'1f942a04-f199-e511-80c6-080027018225', N'San Ramón', N'8860000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'20942a04-f199-e511-80c6-080027018225', N'Vitacura', N'7630000', N'e1922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'21942a04-f199-e511-80c6-080027018225', N'Puente Alto', N'8150000', N'e2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'22942a04-f199-e511-80c6-080027018225', N'Pirque', N'9480000', N'e2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'23942a04-f199-e511-80c6-080027018225', N'San José de Maipo', N'9460000', N'e2922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'24942a04-f199-e511-80c6-080027018225', N'Colina', N'9340000', N'e3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'25942a04-f199-e511-80c6-080027018225', N'Lampa', N'9380000', N'e3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'26942a04-f199-e511-80c6-080027018225', N'Tiltil', N'9420000', N'e3922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'27942a04-f199-e511-80c6-080027018225', N'San Bernardo', N'8050000', N'e4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'28942a04-f199-e511-80c6-080027018225', N'Buin', N'9500000', N'e4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'29942a04-f199-e511-80c6-080027018225', N'Calera de Tango', N'9560000', N'e4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2a942a04-f199-e511-80c6-080027018225', N'Paine', N'9540000', N'e4922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2b942a04-f199-e511-80c6-080027018225', N'Melipilla', N'9580000', N'e5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2c942a04-f199-e511-80c6-080027018225', N'Alhué', N'9650000', N'e5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2d942a04-f199-e511-80c6-080027018225', N'Curacaví', N'9630000', N'e5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2e942a04-f199-e511-80c6-080027018225', N'María Pinto', N'9620000', N'e5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'2f942a04-f199-e511-80c6-080027018225', N'San Pedro', N'9660000', N'e5922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'30942a04-f199-e511-80c6-080027018225', N'Talagante', N'9670000', N'e6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'31942a04-f199-e511-80c6-080027018225', N'El Monte', N'9810000', N'e6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'32942a04-f199-e511-80c6-080027018225', N'Isla de Maipo', N'9790000', N'e6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'33942a04-f199-e511-80c6-080027018225', N'Padre Hurtado', N'9710000', N'e6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'34942a04-f199-e511-80c6-080027018225', N'Peñaflor', N'9750000', N'e6922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'35942a04-f199-e511-80c6-080027018225', N'Valdivia', N'5090000', N'e7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'36942a04-f199-e511-80c6-080027018225', N'Corral', N'5190000', N'e7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'37942a04-f199-e511-80c6-080027018225', N'Lanco', N'5160000', N'e7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'38942a04-f199-e511-80c6-080027018225', N'Los Lagos', N'5170000', N'e7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'39942a04-f199-e511-80c6-080027018225', N'Máfil', N'5200000', N'e7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3a942a04-f199-e511-80c6-080027018225', N'Mariquina', N'5150000', N'e7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3b942a04-f199-e511-80c6-080027018225', N'Paillaco', N'5230000', N'e7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3c942a04-f199-e511-80c6-080027018225', N'Panguipulli', N'5210000', N'e7922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3d942a04-f199-e511-80c6-080027018225', N'La Unión', N'5220000', N'e8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3e942a04-f199-e511-80c6-080027018225', N'Futrono', N'5180000', N'e8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'3f942a04-f199-e511-80c6-080027018225', N'Lago Ranco', N'5250000', N'e8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'40942a04-f199-e511-80c6-080027018225', N'Río Bueno', N'5240000', N'e8922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'41942a04-f199-e511-80c6-080027018225', N'Arica', N'1000000', N'e9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'42942a04-f199-e511-80c6-080027018225', N'Camarones', N'1040000', N'e9922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'43942a04-f199-e511-80c6-080027018225', N'Putre', N'1070000', N'ea922a04-f199-e511-80c6-080027018225')
GO
INSERT [location].[commune] ([id], [name], [post_code], [city_id]) VALUES (N'44942a04-f199-e511-80c6-080027018225', N'General Lagos', N'1080000', N'ea922a04-f199-e511-80c6-080027018225')
GO

------------------------------------------ Tax Document Types ------------------------------------------
INSERT INTO taxation.document_type ([code], [name], tax_rate) VALUES
(30, 'Factura', 0.19),
(32, 'Factura Exenta', 0.00),
(33, 'Factura Electrónica', 0.19),
(34, 'Factura Exenta Electrónica ', 0.0),
(39, 'Boleta Electrónica', 0.1),
(40, 'Liquidación de Factura', 0.0),
(41, 'Boleta Exenta Electrónica', 0.0),
(43, 'Liquidación Factura Electrónica', 0.0),
(45, 'Factura de Compra', 0.0),
(46, 'Factura de Compra Electrónica', 0.0),
(52, 'Guía de Despacho Electrónica', 0.0),
(55, 'Nota de Débito', 0.0),
(56, 'Nota de Débito Electrónica', 0.0),
(60, 'Nota de Crédito', 0.0),
(61, 'Nota de Crédito Electrónica', 0.0),
(101, 'Factura de Exportación', 0.0),
(108, 'Solicitud de Registro de Factura', 0.0),
(110, 'Factura de Exportación Electrónica', 0.0),
(111, 'Nota de Débito de Exportación Electrónica', 0.0),
(112, 'Nota de Crédito de Exportación Electrónica', 0.0),
(901, 'Factura a empresas del territorio preferencial', 0.0),
(914, 'Declaración de Ingreso (DIN)', 0.0),
(918, 'Declaración de Ingreso a Zona Franca Primaria', 0.0),
(801, 'Orden de compra', 0.0),
(802, 'Nota de pedido', 0.0),
(803, 'Contrato', 0.0),
(804, 'Resolución', 0.0),
(805, 'Proceso ChileCompra', 0.0),
(806, 'Ficha ChileCompra', 0.0),
(807, 'DUS', 0.0),
(808, 'B/L (Conocimiento de embarque)', 0.0),
(809, 'AWB (Air Will Bill)', 0.0),
(810, 'MIC/DTA', 0.0),
(811, 'Carta de Porte', 0.0),
(812, 'Resolución del SNA donde califica Servicios de Exportación', 0.0),
(813, 'Pasaporte', 0.0),
(814, 'Certificado de Depósito Bolsa Prod. Chile', 0.0),
(815, 'Vale de Prenda Bolsa Prod. Chile', 0.0);

INSERT INTO taxation.payment_type (code, [description]) VALUES 
('PAC', 'Pago Automático con cargo a Cuenta Corriente'),
('PAT', 'Pago Automático con cargo a Tarjeta Crédito'),
('1', 'Pago al contado'),
('2', 'Crédito'),
('3', 'Sin costo (entrega gratuita)');

INSERT INTO taxation.service_type (code, [description]) VALUES 
(1, 'Factura de servicios periódicos domiciliarios'),
(2, 'Factura de otros servicios periódicos'),
(3, 'Factura de servicios'),
(4, 'Servicios de hotelería'),
(5, 'Servicios de transporte terrestre internacional');

INSERT INTO taxation.dispatch_type (code, [description]) VALUES
('CRET', 'Cliente retira'),
('DCCE', 'Despacho Central - Certificado'),
('DCNO', 'Despacho Central - Normal'),
('DCOT', 'Despacho Central - Otros'),
('OCCA', 'Oficina Central, Con Carta'),
('PCER', 'Postal Certificado'),
('PNOR', 'Postal Normal'),
('POTR', 'Postal Otros'),
('RMAN', 'Reparto por mano');


-------------------------------------------- Economic Activities
DECLARE @parent_group_id UNIQUEIDENTIFIER;
DECLARE @group_id UNIQUEIDENTIFIER;
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES
(newid(), 'Agricultura, Ganadería, Caza y Silvicultura', '0', 0, NULL);
SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Agricultura, Ganadería, Caza y Silvicultura');
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Cultivos En General; Cultivo De Productos De Mercado; Horticultura', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Cultivos En General; Cultivo De Productos De Mercado; Horticultura' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Trigo', '11111', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Maiz', '11112', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Avena', '11113', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Arroz', '11114', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Cebada', '11115', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Otros Cereales', '11119', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo Forrajeros En Praderas Naturales', '11121', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo Forrajeros En Praderas Mejoradas O Sembradas', '11122', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Porotos O Frijol', '11131', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo, Producción De Lupino', '11132', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Otras Legumbres', '11139', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Papas', '11141', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Camotes O Batatas', '11142', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Otros Tubérculos N.c.p', '11149', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Raps', '11151', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Maravilla', '11152', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Otras Oleaginosas N.c.p.', '11159', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Producción De Semillas De Cereales, Legumbres, Oleaginosas', '11160', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Remolacha', '11191', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Tabaco', '11192', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Fibras Vegetales Industriales', '11193', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Plantas Aromáticas O Medicinales', '11194', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Cultivos N.c.p.', '11199', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo Tradicional De Hortalizas Frescas', '11211', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Hortalizas En Invernaderos Y Cultivos Hidroponicos', '11212', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo Orgánico De Hortalizas', '11213', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Plantas Vivas Y Productos De La Floricultura', '11220', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Producción De Semillas De Flores, Prados, Frutas Y Hortalizas', '11230', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Producción En Viveros; Excepto Especies Forestales', '11240', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo Y Recolección De Hongos, Trufas Y Savia; Producción De Jarabe De Arce De Azúcar Y Azúcar', '11250', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Uva Destinada A Producción De Pisco Y Aguardiente', '11311', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Uva Destinada A Producción De Vino', '11312', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Uva De Mesa', '11313', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Frutales En Árboles O Arbustos Con Ciclo De Vida Mayor A Una Temporada', '11321', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Frutales Menores En Plantas Con Ciclo De Vida De Una Temporada', '11322', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Plantas Cuyas Hojas O Frutas Se Utilizan Para Preparar Bebidas', '11330', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Especias', '11340', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Ganado Bovino Para La Producción Lechera', '12111', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Ganado Para Producción De Carne, O Como Ganado Reproductor', '12112', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Ganado Ovino Y/o Explotación Lanera', '12120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Equinos (Caballares, Mulares)', '12130', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Porcinos', '12210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Aves De Corral Para La Producción De Carne', '12221', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Aves De Corral Para La Producción De Huevos', '12222', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Aves Finas O No Tradicionales', '12223', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cría De Animales Domésticos; Perros Y Gatos', '12230', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Apicultura', '12240', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Ranicultura, Helicicultura U Otra Actividad Con Animales Menores O Insectos', '12250', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Explotaciones De Animales No Clasificados En Otra Parte, Incluido Sus Subproductos', '12290', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Cultivo Prod. Agrícolas En Combinación Con Cría De Animales', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Cultivo Prod. Agrícolas En Combinación Con Cría De Animales' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Explotación Mixta', '13000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Servicios Agrícolas Y Ganaderos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Servicios Agrícolas Y Ganaderos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicio De Corte Y Enfardado De Forraje', '14011', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicio De Recolección, Empacado, Trilla, Descascaramiento Y Desgrane; Y Similares', '14012', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicio De Roturación Siembra Y Similares', '14013', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Destrucción De Plagas; Pulverizaciones, Fumigaciones U Otras', '14014', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cosecha, Poda, Amarre Y Labores De Adecuación De La Planta U Otras', '14015', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Servicios Agrícolas N.c.p.', '14019', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Adiestramiento, Guardería Y Cuidados De Mascotas; Excepto Actividades Veterinarias', '14021', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Ganaderos, Excepto Actividades Veterinarias', '14022', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Caza Ordinaria Y Mediante Trampas, Repoblación, Act. Servicio Conexas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Caza Ordinaria Y Mediante Trampas, Repoblación, Act. Servicio Conexas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Caza De Mamíferos Marinos; Excepto Ballenas', '15010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Caza Ordinaria Y Mediante Trampas, Y Actividades De Servicios Conexas', '15090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Silvicultura, Extracción De Madera Y Actividades De Servicios Conexas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Silvicultura, Extracción De Madera Y Actividades De Servicios Conexas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Explotación De Bosques', '20010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Recolección De Productos Forestales Silvestres', '20020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Explotación De Viveros De Especies Forestales', '20030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Forestación', '20041', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Corta De Madera', '20042', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Control De Incendios Forestales', '20043', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Servicios Conexas A La Silvicultura N.c.p.', '20049', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Pesca', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Pesca' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Explt. De Criaderos De Peces Y Prod. Del Mar; Servicios Relacionados', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Explt. De Criaderos De Peces Y Prod. Del Mar; Servicios Relacionados' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo De Especies Acuáticas En Cuerpo De Agua Dulce', '51010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reproducción Y Crianzas De Peces Marinos', '51020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cultivo, Reproducción Y Crecimientos De Vegetales Acuáticos', '51030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reproducción Y Cría De Moluscos Y Crustaceos.', '51040', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Relacionados Con La Acuicultura, No Incluye Servicios Profesionales Y De Extracción', '51090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Pesca Extractiva: Y Servicios Relacionados', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Pesca Extractiva: Y Servicios Relacionados' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Pesca Industrial', '52010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividad Pesquera De Barcos Factorías', '52020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Pesca Artesanal. Extracción De Recursos Acuáticos En General; Incluye Ballenas', '52030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Recolección De Productos Marinos, Como Perlas Naturales, Esponjas, Corales Y Algas.', '52040', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Relacionados Con La Pesca, No Incluye Servicios Profesionales', '52050', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Explotación De Minas Y Canteras', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Explotación De Minas Y Canteras' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Extracción, Aglomeración De Carbón De Piedra, Lignito Y Turba', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Extracción, Aglomeración De Carbón De Piedra, Lignito Y Turba' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción, Aglomeración De Carbón De Piedra, Lignito Y Turba', '100000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Extracción De Petróleo Crudo Y Gas Natural; Actividades Relacionadas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Extracción De Petróleo Crudo Y Gas Natural; Actividades Relacionadas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Petróleo Crudo Y Gas Natural', '111000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Servicios Relacionadas Con La Extracción De Petróleo Y Gas', '112000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Extracción De Minerales Metalíferos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Extracción De Minerales Metalíferos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Minerales De Uranio Y Torio', '120000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Minerales De Hierro', '131000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Oro Y Plata', '132010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Zinc Y Plomo', '132020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Manganeso', '132030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Otros Minerales Metalíferos N.c.p.', '132090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Cobre', '133000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Explotación De Minas Y Canteras', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Explotación De Minas Y Canteras' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Piedra, Arena Y Arcilla', '141000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Nitratos Y Yodo', '142100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Sal', '142200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Extracción De Litio Y Cloruros, Excepto Sal', '142300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Explotación De Otras Minas Y Canteras N.c.p.', '142900', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Industrias Manufactureras No Metálicas', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Industrias Manufactureras No Metálicas' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Producción, Procesamiento Y Conservación De Alimentos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Producción, Procesamiento Y Conservación De Alimentos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Producción, Procesamiento De Carnes Rojas Y Productos Cárnicos', '151110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Conservación De Carnes Rojas (Frigoríficos)', '151120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Producción, Procesamiento Y Conservación De Carnes De Ave Y Otras Carnes Distintas A Las Rojas', '151130', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Cecinas, Embutidos Y Carnes En Conserva.', '151140', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Producción De Harina De Pescado', '151210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos Enlatados De Pescado Y Mariscos', '151221', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Congelados De Pescados Y Mariscos', '151222', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Productos Ahumados, Salados, Deshidratados Y Otros Procesos Similares', '151223', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Productos En Base A Vegetales Acuáticos', '151230', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración Y Conservación De Frutas, Legumbres Y Hortalizas', '151300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Aceites Y Grasas De Origen Vegetal', '151410', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Aceites Y Grasas De Origen Animal, Excepto Las Mantequillas', '151420', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Aceites Y Grasas De Origen Marino', '151430', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Elaboración De Productos Lácteos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Elaboración De Productos Lácteos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Leche, Mantequilla, Productos Lácteos Y Derivados', '152010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Quesos', '152020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Postres A Base De Leche (Helados, Sorbetes Y Otros Similares)', '152030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Elab. De Prod. De Molinería, Almidones Y Prod. Derivados Del Almidón', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Elab. De Prod. De Molinería, Almidones Y Prod. Derivados Del Almidón' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Harinas De Trigo', '153110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Molienda De Arroz', '153120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Otras Molineras Y Alimentos A Base De Cereales', '153190', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Almidones Y Productos Derivados Del Almidón', '153210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Glucosa Y Otros Azúcares Diferentes De La Remolacha', '153220', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Alimentos Preparados Para Animales', '153300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Elaboración De Otros Productos Alimenticios', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Elaboración De Otros Productos Alimenticios' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Pan, Productos De Panadería Y Pastelería', '154110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Galletas', '154120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Azúcar De Remolacha O Cana', '154200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Cacao Y Chocolates', '154310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Confitería', '154320', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Macarrones, Fideos, Alcuzcuz Y Productos Farinaceos Similares', '154400', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Te, Café, Infusiones', '154910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Levaduras Naturales O Artificiales', '154920', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Vinagres, Mostazas, Mayonesas Y Condimentos En General', '154930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Otros Productos Alimenticios No Clasificados En Otra Parte', '154990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Elaboración De Bebidas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Elaboración De Bebidas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Piscos (Industrias Pisqueras)', '155110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Bebidas Alcohólicas Y De Alcohol Etílico A Partir De Sustancias Fermentadas Y Otros', '155120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Vinos', '155200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Bebidas Malteadas, Cervezas Y Maltas', '155300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Bebidas No Alcohólicas', '155410', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Envasado De Agua Mineral Natural, De Manantial Y Potable Preparada', '155420', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Hielo', '155430', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Elaboración De Productos Del Tabaco', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Elaboración De Productos Del Tabaco' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Cigarros Y Cigarrillos', '160010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos Del Tabaco', '160090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Hilandería, Tejedura Y Acabado De Productos Textiles', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Hilandería, Tejedura Y Acabado De Productos Textiles' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Preparación De Hilatura De Fibras Textiles; Tejedura Prod. Textiles', '171100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Acabado De Productos Textil', '171200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos Textiles', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Otros Productos Textiles' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Artículos Confeccionados De Materias Textiles, Excepto Prendas De Vestir', '172100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Tapices Y Alfombra', '172200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Cuerdas, Cordeles, Bramantes Y Redes', '172300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Tejidos De Uso Industrial Como Tejidos Impregnados, Moltoprene, Batista, Etc.', '172910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos Textiles N.c.p.', '172990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Tejidos Y Artículos De Punto Y Ganchillo', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Tejidos Y Artículos De Punto Y Ganchillo' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Tejidos De Punto', '173000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Prendas De Vestir; Excepto Prendas De Piel', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Prendas De Vestir; Excepto Prendas De Piel' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Prendas De Vestir Textiles Y Similares', '181010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Prendas De Vestir De Cuero Natural, Artificial, Plástico', '181020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Accesorios De Vestir', '181030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Ropa De Trabajo', '181040', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Procesamiento Y Fabricación De Artículos De Piel Y Cuero', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Procesamiento Y Fabricación De Artículos De Piel Y Cuero' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Adobo Y Tenidos De Pieles; Fabricación De Artículos De Piel', '182000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Curtido Y Adobo De Cueros', '191100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maletas, Bolsos De Mano Y Similares; Artículos De Talabartería Y Guarnicionería', '191200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Calzado', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Calzado' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Calzado', '192000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Aserrado Y Acepilladura De Maderas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Aserrado Y Acepilladura De Maderas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Aserrado Y Acepilladura De Maderas', '201000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. De Productos De Madera Y Corcho,  Paja Y De Materiales Trenzables', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. De Productos De Madera Y Corcho,  Paja Y De Materiales Trenzables' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Tableros, Paneles Y Hojas De Madera Para Enchapado', '202100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Partes Y Piezas De Carpintería Para Edificios Y Construcciones', '202200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Recipientes De Madera', '202300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos De Madera; Artículos De Corcho, Paja Y Materiales Trenzables', '202900', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Papel Y Productos Del Papel', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Papel Y Productos Del Papel' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Celulosa Y Otras Pastas De Madera', '210110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Papel De Periódico', '210121', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Papel Y Cartón N.c.p.', '210129', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Papel Y Cartón Ondulado Y De Envases De Papel Y Cartón', '210200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Artículos De Papel Y Cartón', '210900', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Edición', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Edición' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Edición Principalmente De Libros', '221101', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Edición De Folletos, Partituras Y Otras Publicaciones', '221109', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Edición De Periódicos, Revistas Y Publicaciones Periódicas', '221200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Edición De Grabaciones', '221300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Edición', '221900', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Impresión Y De Servicios Conexos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Impresión Y De Servicios Conexos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Impresión Principalmente De Libros', '222101', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Impresión N.c.p.', '222109', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Servicio Relacionada Con La Impresión', '222200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reproducción De Grabaciones', '223000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Hornos Coque Y De Refinación De Petróleo', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Productos De Hornos Coque Y De Refinación De Petróleo' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Hornos Coque', '231000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Refinación De Petróleo', '232000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Elaboración De Combustible Nuclear', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Elaboración De Combustible Nuclear' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Combustible Nuclear', '233000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Sustancias Químicas Básicas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Sustancias Químicas Básicas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Carbón Vegetal, Y Briquetas De Carbón Vegetal', '241110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Sustancias Químicas Básicas, Excepto Abonos Y Compuestos De Nitrógeno', '241190', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Abonos Y Compuestos De Nitrógeno', '241200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Plásticos En Formas Primarias Y De Caucho Sintético', '241300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos Químicos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Otros Productos Químicos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Plaguicidas Y Otros Productos Químicos De Uso Agropecuario', '242100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Pinturas, Barnices Y Productos De Revestimiento Similares', '242200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos Farmaceuticos, Sustancias Químicas Medicinales Y Productos Botánicos', '242300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricaciones De Jabones Y Detergentes, Preparados Para Limpiar, Perfumes Y Preparados De Tocador', '242400', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Explosivos Y Productos De Pirotecnia', '242910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos Químicos N.c.p.', '242990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Fibras Manufacturadas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Fibras Manufacturadas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Fibras Manufacturadas', '243000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Caucho', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Productos De Caucho' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Cubiertas Y Cámaras De Caucho', '251110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Recauchado Y Renovación De Cubiertas De Caucho', '251120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos De Caucho', '251900', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Plástico', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Productos De Plástico' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Planchas, Láminas, Cintas, Tiras De Plástico', '252010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Tubos, Mangueras Para La Construcción', '252020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Artículos De Plástico', '252090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Vidrios Y Productos De Vidrio', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Vidrios Y Productos De Vidrio' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación, Manipulado Y Transformación De Vidrio Plano', '261010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Vidrio Hueco', '261020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Fibras De Vidrio', '261030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Artículos De Vidrio N.c.p.', '261090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos Minerales No Metálicos N.c.p.', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Productos Minerales No Metálicos N.c.p.' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Cerámica No Refractaria Para Uso No Estructural Con Fines Ornamentales', '269101', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Cerámica No Refractaria Para Uso No Estructural N.c.p.', '269109', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Cerámicas Refractaria', '269200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Arcilla Y Cerámicas No Refractarias Para Uso Estructural', '269300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Cemento, Cal Y Yeso', '269400', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Hormigón, Artículos De Hormigón Y Mortero (Mezcla Para Construcción)', '269510', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos De Fibrocemento Y Asbestocemento', '269520', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Paneles De Yeso Para La Construcción', '269530', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Artículos De Cemento Y Yeso N.c.p.', '269590', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Corte, Tallado Y Acabado De La Piedra', '269600', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Mezclas Bituminosas A Base De Asfalto, De Betunes Naturales, Y Productos Similares', '269910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos Minerales No Metálicos N.c.p', '269990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Industrias Manufactureras Metálicas', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Industrias Manufactureras Metálicas' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Industrias Básicas De Hierro Y Acero', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Industrias Básicas De Hierro Y Acero' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Industrias Basicas De Hierro Y Acero', '271000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. De Productos Primarios De Metales Preciosos Y Metales No Ferrosos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. De Productos Primarios De Metales Preciosos Y Metales No Ferrosos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Productos De Cobre En Formas Primarias.', '272010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Elaboración De Productos De Aluminio En Formas Primarias', '272020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos Primarios De Metales Preciosos Y De Otros Metales No Ferrosos N.c.p.', '272090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fundición De Metales', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fundición De Metales' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fundición De Hierro Y Acero', '273100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fundición De Metales No Ferrosos', '273200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. De Prod. Metálicos Para Uso Estructural', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. De Prod. Metálicos Para Uso Estructural' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Productos Metálicos De Uso Estructural', '281100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Recipientes De Gas Comprimido O Licuado', '281211', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Tanques, Depósitos Y Recipientes De Metal N.c.p.', '281219', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Tanques, Depósitos Y Recipientes De Metal', '281280', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Generadores De Vapor, Excepto Calderas De Agua Caliente Para Calefacción', '281310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Generadores De Vapor, Excepto Calderas De Agua Caliente Para Calefacción Central', '281380', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. De Otros Prod. Elaborados De Metal; Act. De Trabajo De Metales', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. De Otros Prod. Elaborados De Metal; Act. De Trabajo De Metales' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Forja, Prensado, Estampado Y Laminado De Metal; Incluye Pulvimetalurgia', '289100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Tratamientos Y Revestimientos De Metales; Obras De Ingeniería Mecánica En General', '289200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Artículos De Cuchillería', '289310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Herramientas De Mano Y Artículos De Ferretería', '289320', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Cables, Alambres Y Productos De Alambre', '289910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Productos Elaborados De Metal N.c.p.', '289990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria De Uso General', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Maquinaria De Uso General' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Motores Y Turbinas, Excepto Para Aeronaves, Vehículos Automotores Y Motocicletas', '291110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Motores Y Turbinas, Excepto Para Aeronaves, Vehículos Automotores Y Motocicletas', '291180', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Bombas, Grifos, Válvulas, Compresores, Sistemas Hidráulicos', '291210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Bombas, Compresores, Sistemas Hidráulicos, Válvulas Y Artículos De Grifería', '291280', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Cojinetes, Engranajes, Trenes De Engranajes Y Piezas De Transmisión', '291310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Cojinetes, Engranajes, Trenes De Engranajes Y Piezas De Transmisión', '291380', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Hornos, Hogares Y Quemadores', '291410', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Hornos, Hogares Y Quemadores', '291480', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Equipo De Elevación Y Manipulación', '291510', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Equipo De Elevación Y Manipulación', '291580', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otro Tipo De Maquinarias De Uso General', '291910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación Otros Tipos De Maquinaria Y Equipos De Uso General', '291980', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria De Uso Especial', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Maquinaria De Uso Especial' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria Agropecuaria Y Forestal', '292110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Maquinaria Agropecuaria Y Forestal', '292180', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Máquinas Herramientas', '292210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Máquinas Herramientas', '292280', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria Metalúrgica', '292310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Maquinaria Para La Industria Metalúrgica', '292380', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria Para Minas Y Canteras Y Para Obras De Construcción', '292411', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Partes Para Máquinas De Sondeo O Perforación', '292412', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Maquinaria Para La Explotación De Petróleo, Minas, Canteras, Y Obras De Construcción', '292480', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria Para La Elaboración De Alimentos, Bebidas Y Tabacos', '292510', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Maquinaria Para La Elaboración De Alimentos, Bebidas Y Tabacos', '292580', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria Para La Elaboración De Prendas Textiles, Prendas De Vestir Y Cueros', '292610', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Maquinaria Para La Industria Textil, De La Confección, Del Cuero Y Del Calzado', '292680', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Armas Y Municiones', '292710', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Armas', '292780', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Tipos De Maquinarias De Uso Especial', '292910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Otros Tipos De Maquinaria De Uso Especial', '292980', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Aparatos De Uso Doméstico N.c.p.', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Aparatos De Uso Doméstico N.c.p.' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Aparatos De Uso Doméstico N.c.p.', '293000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria De Oficina, Contabilidad E Informática', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Maquinaria De Oficina, Contabilidad E Informática' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación Y Armado De Computadores Y Hardware En General', '300010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Maquinaria De Oficina, Contabilidad, N.c.p.', '300020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. Y Reparación De Motores, Generadores Y Transformadores Eléctricos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. Y Reparación De Motores, Generadores Y Transformadores Eléctricos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Motores, Generadores Y Transformadores Eléctricos', '311010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Motores, Generadores Y Transformadores Eléctricos', '311080', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Aparatos De Distribución Y Control; Sus Reparaciones', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Aparatos De Distribución Y Control; Sus Reparaciones' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Aparatos De Distribución Y Control', '312010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Aparatos De Distribución Y Control', '312080', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Hilos Y Cables Aislados', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Hilos Y Cables Aislados' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Hilos Y Cables Aislados', '313000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Acumuladores De Pilas Y Baterías Primarias', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Acumuladores De Pilas Y Baterías Primarias' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Acumuladores De Pilas Y Baterías Primarias', '314000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación Y Reparación De Lámparas Y Equipo De Iluminación', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación Y Reparación De Lámparas Y Equipo De Iluminación' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Lámparas Y Equipo De Iluminación', '315010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Equipo De Iluminación', '315080', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación Y Reparación De Otros Tipos De Equipo Eléctrico N.c.p.', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación Y Reparación De Otros Tipos De Equipo Eléctrico N.c.p.' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Tipos De Equipo Eléctrico N.c.p.', '319010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Otros Tipos De Equipo Eléctrico N.c.p.', '319080', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Componentes Electrónicos; Sus Reparaciones', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Componentes Electrónicos; Sus Reparaciones' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Componentes Electrónicos', '321010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Componentes Electrónicos', '321080', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. Y Reparación De Transmisores De Radio, Televisión, Telefonía', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. Y Reparación De Transmisores De Radio, Televisión, Telefonía' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Transmisores De Radio Y Televisión, Aparatos Para Telefonía Y Telegrafía Con Hilos', '322010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Transmisores De Radio Y Televisión, Aparatos Para Telefonía Y Telegrafía Con Hilos', '322080', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. De Receptores De Radio, Televisión, Aparatos De Audio/vídeo', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. De Receptores De Radio, Televisión, Aparatos De Audio/vídeo' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Receptores (Radio Y Tv); Aparatos De Grabación Y Reproducción (Audio Y Video)', '323000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. De Aparatos E Instrumentos Médicos Y Para Realizar Mediciones', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. De Aparatos E Instrumentos Médicos Y Para Realizar Mediciones' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Equipo Médico Y Quirúrgico, Y De Aparatos Ortopédicos', '331110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Laboratorios Dentales', '331120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Equipo Médico Y Quirúrgico, Y De Aparatos Ortopédicos', '331180', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Instrumentos Y Aparatos Para Medir, Verificar, Ensayar, Navegar Y Otros Fines', '331210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Instrumentos Y Aparatos Para Medir, Verificar, Ensayar, Navegar Y Otros Fines', '331280', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Equipos De Control De Procesos Industriales', '331310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Equipos De Control De Procesos Industriales', '331380', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. Y Reparación De Instrumentos De Óptica Y Equipo Fotográfico', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. Y Reparación De Instrumentos De Óptica Y Equipo Fotográfico' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación Y/o Reparación De Lentes Y Artículos Oftalmológicos', '332010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Instrumentos De Optica N.c.p. Y Equipos Fotográficos', '332020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Instrumentos De Optica N.c.p Y Equipo Fotográficos', '332080', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Relojes', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Relojes' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Relojes', '333000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Vehículos Automotores', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Vehículos Automotores' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Vehículos Automotores', '341000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Carrocerías Para Vehículos Automotores; Fabricación De Remolques Y Semi Remolques', '342000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Partes Y Accesorios Para Vehículos Automotores Y Sus Motores', '343000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Construcción Y Reparación De Buques Y Otras Embarcaciones', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Construcción Y Reparación De Buques Y Otras Embarcaciones' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Construcción Y Reparación De Buques; Astilleros', '351110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Construcción De Embarcaciones Menores', '351120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Embarcaciones Menores', '351180', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Construcción De Embarcaciones De Recreo Y Deporte', '351210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Embarcaciones De Recreo Y Deportes', '351280', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fab. De Locomotoras Y Material Rodante Para Ferrocarriles Y Tranvías', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fab. De Locomotoras Y Material Rodante Para Ferrocarriles Y Tranvías' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Locomotoras Y De Material Rodante Para Ferrocarriles Y Tranvías', '352000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Aeronaves Y Naves Espaciales; Sus Reparaciones', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Aeronaves Y Naves Espaciales; Sus Reparaciones' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Aeronaves Y Naves Espaciales', '353010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Aeronaves Y Naves Espaciales', '353080', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Tipos De Equipo De Transporte N.c.p.', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Otros Tipos De Equipo De Transporte N.c.p.' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Motocicletas', '359100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Bicicletas Y De Sillones De Ruedas Para Invalidos', '359200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Equipos De Transporte N.c.p.', '359900', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Muebles', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Muebles' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Muebles Principalmente De Madera', '361010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Otros Muebles N.c.p., Incluso Colchones', '361020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Industrias Manufactureras N.c.p.', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Industrias Manufactureras N.c.p.' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Joyas Y Productos Conexos', '369100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Instrumentos De Música', '369200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Artículos De Deporte', '369300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Juegos Y Juguetes', '369400', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Plumas Y Lápices De Toda Clase Y Artículos De Escritorio En General', '369910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Brochas, Escobas Y Cepillos', '369920', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Fósforos', '369930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Artículos De Otras Industrias N.c.p.', '369990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Reciclamiento De Desperdicios Y Desechos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Reciclamiento De Desperdicios Y Desechos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reciclamiento De Desperdicios Y Desechos Metálicos', '371000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reciclamiento De Papel', '372010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reciclamiento De Vidrio', '372020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reciclamiento De Otros Desperdicios Y Desechos N.c.p.', '372090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Suministro De Electricidad, Gas Y Agua', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Suministro De Electricidad, Gas Y Agua' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Generación, Captación Y Distribución De Energía Eléctrica', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Generación, Captación Y Distribución De Energía Eléctrica' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Generación Hidroeléctrica', '401011', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Generación En Centrales Termoeléctrica De Ciclos Combinados', '401012', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Generación En Otras Centrales Termoeléctricas', '401013', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Generación En Otras Centrales N.c.p.', '401019', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transmisión De Energía Eléctrica', '401020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Distribución De Energia Eléctrica', '401030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Fabricación De Gas; Distribución De Combustibles Gaseosos Por Tuberías', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Fabricación De Gas; Distribución De Combustibles Gaseosos Por Tuberías' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Fabricación De Gas; Distribución De Combustibles Gaseosos Por Tuberías', '402000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Suministro De Vapor Y Agua Caliente', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Suministro De Vapor Y Agua Caliente' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Suministro De Vapor Y Agua Caliente', '403000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Captación, Depuración Y Distribución De Agua', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Captación, Depuración Y Distribución De Agua' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Captación, Depuración Y Distribución De Agua', '410000', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Construcción', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Construcción' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Construcción', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Construcción' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Preparación Del Terreno, Excavaciones Y Movimientos De Tierras', '451010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Demolición Y El Derribo De Edificios Y Otras Estructuras', '451020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Construcción De Edificios Completos O De Partes De Edificios', '452010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Obras De Ingeniería', '452020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Acondicionamiento De Edificios', '453000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Obras Menores En Construcción (Contratistas, Albaniles, Carpinteros)', '454000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Equipo De Construcción O Demolición Dotado De Operarios', '455000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Mayor Y Menor; Rep. Veh.automotores/enseres Domésticos', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Comercio Al Por Mayor Y Menor; Rep. Veh.automotores/enseres Domésticos' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta De Vehículos Automotores', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta De Vehículos Automotores' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Vehículos Automotores (Importación, Distribución) Excepto Motocicletas', '501010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta O Compraventa Al Por Menor De Vehículos Automotores Nuevos O Usados; Excepto Motocicletas', '501020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Mantenimiento Y Reparación De Vehículos Automotores', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Mantenimiento Y Reparación De Vehículos Automotores' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicio De Lavado De Vehículos Automotores', '502010', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Remolque De Vehículos (Gruas)', '502020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Mantenimiento Y Reparación De Vehículos Automotores', '502080', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta De Partes, Piezas Y Accesorios De Vehículos Automotores', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta De Partes, Piezas Y Accesorios De Vehículos Automotores' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta De Partes, Piezas Y Accesorios De Vehículos Automotores', '503000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta, Mantenimiento Y Reparación De Motocicletas Y Sus Partes', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta, Mantenimiento Y Reparación De Motocicletas Y Sus Partes' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta De Motocicletas', '504010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta De Piezas Y Accesorios De Motocicletas', '504020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Motocicletas', '504080', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Combustible Para Automotores', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Al Por Menor De Combustible Para Automotores' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Combustible Para Automotores', '505000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor A Cambio De Una Retribución O Por Contrata', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Al Por Mayor A Cambio De Una Retribución O Por Contrata' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Corretaje De Productos Agrícolas', '511010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Corretaje De Ganado (Ferias De Ganado)', '511020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Tipos De Corretajes O Remates N.c.p. (No Incluye Servicios De Martillero)', '511030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Materias Primas Agropecuarias', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Al Por Mayor De Materias Primas Agropecuarias' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Animales Vivos', '512110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Productos Pecuarios (Lanas, Pieles, Cueros Sin Procesar); Excepto Alimentos', '512120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Materias Primas Agrícolas', '512130', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Mayorista De Frutas Y Verduras', '512210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Mayoristas De Carnes', '512220', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Mayoristas De Productos Del Mar (Pescado, Mariscos, Algas)', '512230', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Mayoristas De Vinos Y Bebidas Alcohólicas Y De Fantasía', '512240', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Confites', '512250', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Tabaco Y Productos Derivados', '512260', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Huevos, Leche, Abarrotes, Y Otros Alimentos N.c.p.', '512290', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Enseres Domésticos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Al Por Mayor De Enseres Domésticos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Productos Textiles, Prendas De Vestir Y Calzado', '513100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Muebles', '513910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Artículos Eléctricos Y Electrónicos Para El Hogar', '513920', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Artículos De Perfumería, Cosméticos, Jabones Y Productos De Limpieza', '513930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Papel Y Cartón', '513940', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Libros', '513951', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Revistas Y Periódicos', '513952', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Productos Farmaceuticos', '513960', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Instrumentos Científicos Y Quirúrgicos', '513970', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Otros Enseres Domésticos N.c.p.', '513990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Productos Intermedios, Desechos No Agropecuarios', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Al Por Mayor De Productos Intermedios, Desechos No Agropecuarios' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Combustibles Líquidos', '514110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Combustibles Sólidos', '514120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Combustibles Gaseosos', '514130', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Productos Conexos A Los Combustibles', '514140', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Metales Y Minerales Metalíferos', '514200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Madera No Trabajada Y Productos Resultantes De Su Elaboración Primaria', '514310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Materiales De Construcción, Artículos De Ferretería Y Relacionados', '514320', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Productos Químicos', '514910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Desechos Metálicos (Chatarra)', '514920', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Insumos Veterinarios', '514930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Otros Productos Intermedios, Desperdicios Y Desechos N.c.p.', '514990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria, Equipo Y Materiales Conexos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Al Por Mayor De Maquinaria, Equipo Y Materiales Conexos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria Agrícola Y Forestal', '515001', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria Metalúrgica', '515002', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria Para La Minería', '515003', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria Para La Construcción', '515004', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria Para La Elaboración De Alimentos, Bebidas Y Tabaco', '515005', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria Para Textiles Y Cueros', '515006', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Máquinas Y Equipos De Oficina; Incluye Materiales Conexos', '515007', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria Y Equipo De Transporte Excepto Vehículos Automotores', '515008', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Maquinaria, Herramientas, Equipo Y Materiales N.c.p.', '515009', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Otros Productos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Al Por Mayor De Otros Productos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Mayor De Otros Productos N.c.p.', '519000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor No Especializado En Almacenes', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Comercio Al Por Menor No Especializado En Almacenes' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Grandes Establecimientos (Venta De Alimentos); Hipermercados', '521111', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Almacenes Medianos (Venta De Alimentos); Supermercados, Minimarkets', '521112', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Almacenes Pequenos (Venta De Alimentos)', '521120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Grandes Tiendas - Productos De Ferretería Y Para El Hogar', '521200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Grandes Tiendas - Vestuario Y Productos Para El Hogar', '521300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Otros Productos En Pequenos Almacenes No Especializados', '521900', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Por Menor De Alimentos, Bebidas, Tabacos En Almc. Especializados', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Por Menor De Alimentos, Bebidas, Tabacos En Almc. Especializados' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Bebidas Y Licores (Botillerías)', '522010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Carnes (Rojas, Blancas, Otras) Productos Cárnicos Y Similares', '522020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Verduras Y Frutas (Verdulería)', '522030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Pescados, Mariscos Y Productos Conexos', '522040', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Productos De Panadería Y Pastelería', '522050', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Alimentos Para Mascotas Y Animales En General', '522060', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Aves Y Huevos', '522070', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Productos De Confiterías, Cigarrillos, Y Otros', '522090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Otros Prod. Nuevos En Almc. Especializados', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Comercio Al Por Menor De Otros Prod. Nuevos En Almc. Especializados' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Farmacias - Pertenecientes A Cadena De Establecimientos', '523111', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Farmacias Independientes', '523112', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Productos Medicinales', '523120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Artículos Ortopédicos', '523130', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Artículos De Tocador Y Cosméticos', '523140', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Calzado', '523210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Prendas De Vestir En General, Incluye Accesorios', '523220', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Lanas, Hilos Y Similares', '523230', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Maleterías, Talabarterías Y Artículos De Cuero', '523240', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Ropa Interior Y Prendas De Uso Personal', '523250', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Textiles Para El Hogar Y Otros Productos Textiles N.c.p.', '523290', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Artículos Electrodomésticos Y Electrónicos Para El Hogar', '523310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Cristales, Lozas, Porcelana, Menaje (Cristalerías)', '523320', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Muebles; Incluye Colchones', '523330', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Instrumentos Musicales (Casa De Música)', '523340', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Discos, Cassettes, Dvd Y Videos', '523350', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Lámparas, Apliqués Y Similares', '523360', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Aparatos, Artículos, Equipo De Uso Doméstico N.c.p.', '523390', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Artículos De Ferretería Y Materiales De Construcción', '523410', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Pinturas, Barnices Y Lacas', '523420', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Productos De Vidrio', '523430', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Artículos Fotográficos', '523911', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Artículos Ópticos', '523912', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Por Menor De Juguetes', '523921', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Libros', '523922', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Revistas Y Diarios', '523923', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio De Artículos De Suministros De Oficinas Y Artículos De Escritorio En General', '523924', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Computadoras, Softwares Y Suministros', '523930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Armerías, Artículos De Caza Y Pesca', '523941', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Bicicletas Y Sus Repuestos', '523942', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Artículos Deportivos', '523943', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Artículos De Joyería, Fantasías Y Relojerías', '523950', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Gas Licuado En Bombonas', '523961', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Carbón, Lena Y Otros Combustibles De Uso Doméstico', '523969', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Artículos Típicos (Artesanías)', '523991', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Flores, Plantas, Árboles, Semillas, Abonos', '523992', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor De Mascotas Y Accesorios', '523993', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Ventas Al Por Menor De Otros Productos En Almacenes Especializados N.c.p.', '523999', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor En Almacenes De Artículos Usados', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Venta Al Por Menor En Almacenes De Artículos Usados' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Antiguedades', '524010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Ropa Usada', '524020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor De Artículos Y Artefactos Usados N.c.p.', '524090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Comercio Al Por Menor No Realizado En Almacenes', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Comercio Al Por Menor No Realizado En Almacenes' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor En Empresas De Venta A Distancia Por Correo', '525110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor En Empresas De Venta A Distancia Vía Telefónica', '525120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor En Empresas De Venta A Distancia Vía Internet; Comercio Electrónico', '525130', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor En Puestos De Venta Y Mercados', '525200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor Realizada Por Independientes En Transporte Público (Ley 20.388)', '525911', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor No Realizada En Almacenes De Productos Propios N.c.p.', '525919', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Máquinas Expendedoras', '525920', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Venta Al Por Menor A Cambio De Una Retribución O Por Contrata', '525930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Tipos De Venta Al Por Menor No Realizada En Almacenes N.c.p.', '525990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Reparación De Efectos Personales Y Enseres Domésticos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Reparación De Efectos Personales Y Enseres Domésticos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Calzado Y Otros Artículos De Cuero', '526010', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparaciones Eléctricas Y Electrónicas', '526020', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Reparación De Relojes Y Joyas', '526030', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Reparaciones De Efectos Personales Y Enseres Domésticos N.c.p.', '526090', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Hoteles Y Restaurantes', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Hoteles Y Restaurantes' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Hoteles; Campamentos Y Otros Tipos De Hospedaje Temporal', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Hoteles; Campamentos Y Otros Tipos De Hospedaje Temporal' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Hoteles', '551010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Moteles', '551020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Residenciales', '551030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Tipos De Hospedaje Temporal Como Camping, Albergues, Posadas, Refugios Y Similares', '551090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Restaurantes, Bares Y Cantinas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Restaurantes, Bares Y Cantinas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Restaurantes', '552010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Establecimientos De Comida Rápida (Bares, Fuentes De Soda, Gelaterías, Pizzerías Y Similares)', '552020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Casinos Y Clubes Sociales', '552030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Comida Preparada En Forma Industrial', '552040', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Banquetes, Bodas Y Otras Celebraciones', '552050', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Otros Establecimientos Que Expenden Comidas Y Bebidas', '552090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Transporte, Almacenamiento Y Comunicaciones', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Transporte, Almacenamiento Y Comunicaciones' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Transporte Por Ferrocarriles', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Transporte Por Ferrocarriles' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Interurbano De Pasajeros Por Ferrocarriles', '601001', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte De Carga Por Ferrocarriles', '601002', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Otros Tipos De Transporte Por Vía Terrestre', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Otros Tipos De Transporte Por Vía Terrestre' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Urbano De Pasajeros Vía Ferrocarril (Incluye Metro)', '602110', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Urbano De Pasajeros Vía Autobus (Locomoción Colectiva)', '602120', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Interurbano De Pasajeros Vía Autobus', '602130', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Urbano De Pasajeros Vía Taxi Colectivo', '602140', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Transporte Escolar', '602150', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Transporte De Trabajadores', '602160', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Tipos De Transporte Regular De Pasajeros Por Vía Terrestre N.c.p.', '602190', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transportes Por Taxis Libres Y Radiotaxis', '602210', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Transporte A Turistas', '602220', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte De Pasajeros En Vehículos De Tracción Humana Y Animal', '602230', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Tipos De Transporte No Regular De Pasajeros N.c.p.', '602290', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte De Carga Por Carretera', '602300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Transporte Por Tuberías', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Transporte Por Tuberías' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Por Tuberías', '603000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Transporte Marítimo Y De Cabotaje', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Transporte Marítimo Y De Cabotaje' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Marítimo Y De Cabotaje De Pasajeros', '611001', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Marítimo Y De Cabotaje De Carga', '611002', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Transporte Por Vías De Navegación Interiores', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Transporte Por Vías De Navegación Interiores' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte De Pasajeros Por Vías De Navegación Interiores', '612001', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte De Carga Por Vías De Navegación Interiores', '612002', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Transporte Por Vía Aérea', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Transporte Por Vía Aérea' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Regular Por Vía Aérea De Pasajeros', '621010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte Regular Por Vía Aérea De Carga', '621020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte No Regular Por Vía Aérea De Pasajeros', '622001', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte No Regular Por Vía Aérea De Carga', '622002', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Act. De Transporte Complementarias Y Auxiliares; Agencias De Viaje', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Act. De Transporte Complementarias Y Auxiliares; Agencias De Viaje' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Manipulación De La Carga', '630100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Almacenamiento Y Depósito', '630200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Terminales Terrestres De Pasajeros', '630310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Estacionamiento De Vehículos Y Parquímetros', '630320', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Puertos Y Aeropuertos', '630330', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Prestados Por Concesionarios De Carreteras', '630340', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades Conexas Al Transporte N.c.p.', '630390', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Agencias Y Organizadores De Viajes; Actividades De Asistencia A Turistas N.c.p.', '630400', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Agencias De Aduanas', '630910', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Agencias De Transporte', '630920', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades Postales Y De Correo', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades Postales Y De Correo' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades Postales Nacionales', '641100', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Correo Distintas De Las Actividades Postales Nacionales', '641200', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Telecomunicaciones', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Telecomunicaciones' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Telefonía Fija', '642010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Telefonía Móvil', '642020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Portadores Telefónicos (Larga Distancia Nacional E Internacional)', '642030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Televisión No Abierta', '642040', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Proveedores De Internet', '642050', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Centros De Llamados; Incluye Envío De Fax', '642061', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Centros De Acceso A Internet', '642062', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Servicios De Telecomunicaciones N.c.p.', '642090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Intermediación Financiera', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Intermediación Financiera' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Intermediación Monetaria', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Intermediación Monetaria' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Banca Central', '651100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Bancos', '651910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Financieras', '651920', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Tipos De Intermediación Monetaria N.c.p.', '651990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Otros Tipos De Intermediación Financiera', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Otros Tipos De Intermediación Financiera' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Leasing Financiero', '659110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Leasing Habitacional', '659120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Financiamiento Del Fomento De La Producción', '659210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Crédito Prendario', '659220', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Factoring', '659231', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Securitizadoras', '659232', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Instituciones Financieras N.c.p.', '659290', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Administradoras De Fondos De Inversión', '659911', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Administradoras De Fondos Mutuos', '659912', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Administradoras De Fices (Fondos De Inversión De Capital Extranjero)', '659913', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Administradoras De Fondos Para La Vivienda', '659914', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Administradoras De Fondos Para Otros Fines Y/o Generales', '659915', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Sociedades De Inversión Y Rentistas De Capitales Mobiliarios En General', '659920', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Financiación Planes De Seg. Y De Pensiones, Excepto Afiliación Oblig.', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Financiación Planes De Seg. Y De Pensiones, Excepto Afiliación Oblig.' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Planes De Seguro De Vida', '660101', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Planes De Reaseguros De Vida', '660102', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Administradoras De Fondos De Pensiones (Afp)', '660200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Planes De Seguros Generales', '660301', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Planes De Reaseguros Generales', '660302', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Isapres', '660400', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Act. Aux. De La Intermediación Financiera, Excepto Planes De Seguros', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Act. Aux. De La Intermediación Financiera, Excepto Planes De Seguros' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Administración De Mercados Financieros', '671100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Corredores De Bolsa', '671210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Agentes De Valores', '671220', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Servicios De Corretaje', '671290', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cámara De Compensación', '671910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Administradora De Tarjetas De Crédito', '671921', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Empresas De Asesoría, Consultoría Financiera Y De Apoyo Al Giro', '671929', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Clasificadores De Riesgos', '671930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Casas De Cambio Y Operadores De Divisa', '671940', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades Auxiliares De La Intermediación Financiera N.c.p.', '671990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Act. Auxiliares De Financiación De Planes De Seguros Y De Pensiones', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Act. Auxiliares De Financiación De Planes De Seguros Y De Pensiones' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Corredores De Seguros', '672010', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Agentes Y Liquidadores De Seguros', '672020', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades Auxiliares De La Financiación De Planes De Seguros Y De Pensiones N.c.p.', '672090', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades Inmobiliarias, Empresariales Y De Alquiler', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades Inmobiliarias, Empresariales Y De Alquiler' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades Inmobiliarias Realizadas Con Bienes Propios O Arrendados', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades Inmobiliarias Realizadas Con Bienes Propios O Arrendados' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Arriendo De Inmuebles Amoblados O Con Equipos Y Maquinarias', '701001', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Compra, Venta Y Alquiler (Excepto Amoblados) De Inmuebles Propios O Arrendados', '701009', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Act. Inmobiliarias Realizadas A Cambio De Retribución O Por Contrata', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Act. Inmobiliarias Realizadas A Cambio De Retribución O Por Contrata' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Corredores De Propiedades', '702000', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Alquiler Equipo De Transporte', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Alquiler Equipo De Transporte' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Autos Y Camionetas Sin Chofer', '711101', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Otros Equipos De Transporte Por Vía Terrestre Sin Operarios', '711102', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Transporte Por Vía Acuática Sin Tripulación', '711200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Equipo De Transporte Por Vía Aérea Sin Tripulantes', '711300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Alquiler De Otros Tipos De Maquinaria Y Equipo', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Alquiler De Otros Tipos De Maquinaria Y Equipo' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Maquinaria Y Equipo Agropecuario', '712100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Maquinaria Y Equipo De Construcción E Ingeniería Civil', '712200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Maquinaria Y Equipo De Oficina (Sin Operarios Ni Servicio Administrativo)', '712300', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Otros Tipos De Maquinarias Y Equipos N.c.p.', '712900', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Alquiler De Efectos Personales Y Enseres Domésticos N.c.p.', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Alquiler De Efectos Personales Y Enseres Domésticos N.c.p.' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Bicicletas Y Artículos Para Deportes', '713010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Arriendo De Videos, Juegos De Video, Y Equipos Reproductores De Video, Música Y Similares', '713020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Mobiliario Para Eventos (Sillas, Mesas, Mesones, Vajillas, Toldos Y Relacionados)', '713030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Alquiler De Otros Efectos Personales Y Enseres Domésticos N.c.p.', '713090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Servicios Informáticos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Servicios Informáticos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Asesores Y Consultores En Informática (Software)', '722000', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Procesamiento De Datos Y Actividades Relacionadas Con Bases De Datos', '724000', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Empresas De Servicios Integrales De Informática', '726000', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Mantenimiento Y Reparación De Maquinaria De Oficina', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Mantenimiento Y Reparación De Maquinaria De Oficina' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Mantenimiento Y Reparación De Maquinaria De Oficina, Contabilidad E Informática', '725000', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Investigaciones Y Desarrollo Experimental', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Investigaciones Y Desarrollo Experimental' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Investigaciones Y Desarrollo Experimental En El Campo De Las Ciencias Naturales Y La Ingeniería', '731000', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Investigaciones Y Desarrollo Experimental En El Campo De Las Ciencias Sociales Y Las Humanidades', '732000', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades Jurídicas Y De Asesoramiento Empresarial En General', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades Jurídicas Y De Asesoramiento Empresarial En General' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Jurídicos', '741110', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicio Notarial', '741120', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Conservador De Bienes Raices', '741130', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Receptores Judiciales', '741140', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Arbitrajes, Síndicos, Peritos Y Otros', '741190', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Contabilidad, Teneduría De Libros Y Auditoría; Asesoramientos Tributarios', '741200', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Investigación De Mercados Y Realización De Encuestas De Opinión Pública', '741300', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Asesoramiento Empresarial Y En Materia De Gestión', '741400', 0, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Arquitectura E Ingeniería Y Otras Actividades Técnicas', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Arquitectura E Ingeniería Y Otras Actividades Técnicas' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Arquitectura Y Técnico Relacionado', '742110', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Empresas De Servicios Geológicos Y De Prospección', '742121', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Profesionales En Geología Y Prospección', '742122', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Empresas De Servicios De Topografía Y Agrimensura', '742131', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Profesionales De Topografía Y Agrimensura', '742132', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Ingeniería Prestados Por Empresas N.c.p.', '742141', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Ingeniería Prestados Por Profesionales N.c.p.', '742142', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Servicios Desarrollados Por Profesionales', '742190', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicio De Revisión Técnica De Vehículos Automotores', '742210', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Servicios De Ensayos Y Analisis Técnicos', '742290', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Publicidad', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Publicidad' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Empresas De Publicidad', '743001', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Personales En Publicidad', '743002', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Act. Empresariales Y De Profesionales Prestadas A Empresas N.c.p.', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Act. Empresariales Y De Profesionales Prestadas A Empresas N.c.p.' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Suministro De Personal; Empresas Servicios Transitorios', '749110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Reclutamiento De Personal', '749190', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Investigación', '749210', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Integrales De Seguridad', '749221', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Transporte De Valores', '749222', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Personales Relacionados Con Seguridad', '749229', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Empresas De Limpieza De Edificios Residenciales Y No Residenciales', '749310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Desratización Y Fumigación No Agrícola', '749320', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Revelado, Impresión, Ampliación De Fotografías', '749401', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Fotografía Publicitaria', '749402', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Personales De Fotografía', '749409', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Envasado Y Empaque', '749500', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Cobranza De Cuentas', '749911', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Evaluación Y Calificación Del Grado De Solvencia', '749912', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Asesorías En La Gestión De La Compra O Venta De Pequenas Y Medianas Empresas', '749913', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Disenadores De Vestuario', '749921', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Disenadores De Interiores', '749922', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Disenadores N.c.p.', '749929', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Empresas De Taquigrafía, Reproducción, Despacho De Correspondencia, Y Otras Labores De Oficina', '749931', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Personales De Traducción, Interpretación Y Labores De Oficina', '749932', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Empresas De Traducción E Interpretación', '749933', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Fotocopias', '749934', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Agencias De Contratación De Actores', '749940', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Subasta (Martilleros)', '749950', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Galerías De Arte', '749961', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Ferias De Exposiciones Con Fines Empresariales', '749962', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Contestación De Llamadas (Call Center)', '749970', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades Empresariales N.c.p.', '749990', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Adm. Publica Y Defensa; Planes De Seg. Social Afiliación Obligatoria', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Adm. Publica Y Defensa; Planes De Seg. Social Afiliación Obligatoria' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Gobierno Central Y Administración Pública', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Gobierno Central Y Administración Pública' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Gobierno Central', '751110', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Municipalidades', '751120', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades Del Poder Judicial', '751200', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades Del Poder Legislativo', '751300', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Relaciones Exteriores', '752100', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Defensa', '752200', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Mantenimiento Del Orden Público Y De Seguridad', '752300', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Planes De Seguridad Social De Afiliación Obligatoria', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Planes De Seguridad Social De Afiliación Obligatoria' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Planes De Seguridad Social De Afiliación Obligatoria Relacionados Con Salud', '753010', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Cajas De Compensación', '753020', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Planes De Seguridad Social De Afiliación Obligatoria', '753090', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Enseñanza', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Enseñanza' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Enseñanza Preescolar, Primaria, Secundaria Y Superior ; Profesores', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Enseñanza Preescolar, Primaria, Secundaria Y Superior ; Profesores' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Establecimientos De Enseñanza Preescolar', '801010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Establecimientos De Enseñanza Primaria', '801020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Establecimientos De Enseñanza Secundaria De Formación General', '802100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Establecimientos De Enseñanza Secundaria De Formación Técnica Y Profesional', '802200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Universidades', '803010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Institutos Profesionales', '803020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Centros De Formación Técnica', '803030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Establecimientos De Enseñanza Primaria Y Secundaria Para Adultos', '809010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Establecimientos De Enseñanza Preuniversitaria', '809020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Educación Extraescolar (Escuela De Conducción, Música, Modelaje, Etc.)', '809030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Educación A Distancia (Internet, Correspondencia, Otras)', '809041', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Personales De Educación', '809049', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Servicios Sociales Y De Salud', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Servicios Sociales Y De Salud' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades Relacionadas Con La Salud Humana', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades Relacionadas Con La Salud Humana' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Hospitales Y Clínicas', '851110', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Clínicas Psiquiatricas, Centros De Rehabilitación, Asilos Y Clínicas De Reposo', '851120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Médicos En Forma Independiente', '851211', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Establecimientos Médicos De Atención Ambulatoria (Centros Médicos)', '851212', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Odontólogos En Forma Independiente', '851221', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Centros De Atención Odontológica', '851222', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Laboratorios Clínicos; Incluye Bancos De Sangre', '851910', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Profesionales De La Salud', '851920', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades Empresariales Relacionadas Con La Salud Humana', '851990', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades Veterinarias', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades Veterinarias' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Clínicas Veterinarias', '852010', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Médicos Veterinarios En Forma Independiente', '852021', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Otros Profesionales Independientes En El Área Veterinaria', '852029', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Servicios Sociales', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Servicios Sociales' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Sociales Con Alojamiento', '853100', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Sociales Sin Alojamiento', '853200', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Servicios Comunitarias, Sociales Y Personales', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Otras Actividades De Servicios Comunitarias, Sociales Y Personales' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Eliminación De Desperdicios Y Aguas Residuales, Saneamiento', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Eliminación De Desperdicios Y Aguas Residuales, Saneamiento' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Vertederos', '900010', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Barrido De Exteriores', '900020', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Recogida Y Eliminación De Desechos', '900030', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Evacuación De Riles Y Aguas Servidas', '900040', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Tratamiento De Riles Y Aguas Servidas', '900050', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Manejo De Desperdicios', '900090', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Act. De Organizaciones Empresariales, Profesionales Y De Empleadores', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Act. De Organizaciones Empresariales, Profesionales Y De Empleadores' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Organizaciones Empresariales Y De Empleadores', '911100', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Colegios Profesionales', '911210', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Otras Organizaciones Profesionales', '911290', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Sindicatos Y De Otras Organizaciones', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Sindicatos Y De Otras Organizaciones' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Sindicatos', '912000', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Organizaciones Religiosas', '919100', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Organizaciones Políticas', '919200', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Centros De Madres Y Unidades Vecinales Y Comunales', '919910', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Clubes Sociales', '919920', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Institutos De Estudios, Fundaciones, Corporaciones De Desarrollo (Educación, Salud)', '919930', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Otras Asociaciones N.c.p.', '919990', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Act. De Cinematografía, Radio Y Tv Y Otras Act. De Entretenimiento', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Act. De Cinematografía, Radio Y Tv Y Otras Act. De Entretenimiento' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Producción De Películas Cinematográficas', '921110', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Distribuidora Cinematográficas', '921120', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Exhibición De Filmes Y Videocintas', '921200', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Televisión', '921310', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Radio', '921320', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Producción De Recitales Y Otros Eventos Musicales Masivos', '921411', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Producción Teatral Y Otros N.c.p.', '921419', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades Empresariales De Artistas', '921420', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades Artísticas; Funciones De Artistas, Actores, Músicos, Conferencistas, Otros', '921430', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Agencias De Venta De Billetes De Teatro, Salas De Concierto Y De Teatro', '921490', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Instructores De Danza', '921911', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Discotecas, Cabaret, Salas De Baile Y Similares', '921912', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Parques De Atracciones Y Centros Similares', '921920', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Espectáculos Circenses, De Títeres U Otros Similares', '921930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Entretenimiento N.c.p.', '921990', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Agencias De Noticias Y Servicios Periodísticos', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Agencias De Noticias Y Servicios Periodísticos' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Agencias De Noticias', '922001', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Periodísticos Prestado Por Profesionales', '922002', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades De Bibliotecas, Archivos Y Museos Y Otras Act. Culturales', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades De Bibliotecas, Archivos Y Museos Y Otras Act. Culturales' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Bibliotecas Y Archivos', '923100', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Museos Y Preservación De Lugares Y Edificios Históricos', '923200', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Jardines Botánicos Y Zoológicos Y De Parques Nacionales', '923300', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Actividades Deportivas Y Otras Actividades De Esparcimiento', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Actividades Deportivas Y Otras Actividades De Esparcimiento' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Explotación De Instalaciones Especializadas Para Las Practicas Deportivas', '924110', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Clubes De Deportes Y Estadios', '924120', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Futbol Profesional', '924131', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Futbol Amateur', '924132', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Hipódromos', '924140', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Promoción Y Organización De Espectáculos Deportivos', '924150', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Escuelas Para Deportes', '924160', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades Relacionadas Al Deporte N.c.p.', '924190', NULL, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Sistemas De Juegos De Azar Masivos.', '924910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Casino De Juegos', '924920', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Salas De Billar, Bowling, Pool Y Juegos Electrónicos', '924930', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Contratación De Actores Para Cine, Tv, Y Teatro', '924940', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otros Servicios De Diversión Y Esparcimientos N.c.p.', '924990', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Servicios', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Otras Actividades De Servicios' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Lavado Y Limpieza De Prendas De Tela Y De Piel, Incluso Las Limpiezas En Seco', '930100', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Peluquerías Y Salones De Belleza', '930200', NULL, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios Funerarios', '930310', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios En Cementerios', '930320', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Servicios De Carrozas Fúnebres (Transporte De Cadáveres)', '930330', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Servicios Funerarios Y Otras Actividades Conexas', '930390', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Actividades De Mantenimiento Físico Corporal (Baños, Turcos, Saunas)', '930910', 1, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Otras Actividades De Servicios Personales N.c.p.', '930990', 0, 2, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Consejo De Administración De Edificios Y Condominios', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Consejo De Administración De Edificios Y Condominios' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Consejo De Administración De Edificios Y Condominios', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Consejo De Administración De Edificios Y Condominios' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Hogares Privados Individuales Con Servicio Doméstico', '950001', 0, NULL, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Consejo De Administración De Edificios Y Condominios', '950002', 0, 1, @group_id);
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Organizaciones Y Órganos Extraterritoriales', '0', 0, NULL);

SET @parent_group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Organizaciones Y Órganos Extraterritoriales' AND code = '0' AND (economic_activity_group_id IS NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, economic_activity_group_id) VALUES (newid(), 'Organizaciones Y Órganos Extraterritoriales', '0', 0, @parent_group_id);

SET @group_id = (SELECT id FROM taxation.economic_activity WHERE name = 'Organizaciones Y Órganos Extraterritoriales' AND code = '0' AND (economic_activity_group_id IS NOT NULL));
INSERT INTO taxation.economic_activity (id, name, code, is_subject_to_tax, tributary_category, economic_activity_group_id) VALUES (newid(), 'Organizaciones Y Órganos Extraterritoriales', '990000', NULL, 1, @group_id);

exec taxation.add_comercial_activity 'Producción de Energías Renovables';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación y Producción de Energía Eléct';
exec taxation.add_comercial_activity 'Transmisión';
exec taxation.add_comercial_activity 'Proyectos de desarrollo limpio, generaci';
exec taxation.add_comercial_activity 'Generacion y Comercializacion de Energia';
exec taxation.add_comercial_activity 'Gen., Dist. y Comerc. de Energía Solar';
exec taxation.add_comercial_activity 'Gen. y Trans. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Eléctrico';
exec taxation.add_comercial_activity 'DISTRIBUCION DE ENERGÍA';
exec taxation.add_comercial_activity 'Transmisión de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación, Transmisión y Distribución d';
exec taxation.add_comercial_activity 'Gen., Trans., Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generadora Eléctrica';
exec taxation.add_comercial_activity 'Comercialización de energía eléctrica y ';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Transmisión de Energía Eléctrica';
exec taxation.add_comercial_activity 'GENERACION, COMERCIALIZACION Y DIST. ENE';
exec taxation.add_comercial_activity 'Transmisión, suministro y comercializaci';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'Gen., Trans., Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'Gen., Capacit. y Dist. Energía Eléctrica';
exec taxation.add_comercial_activity 'Generacion Hidroelectrica';
exec taxation.add_comercial_activity 'Transmisión, suministro y comercializaci';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Prestaciones de Servicios Eléctricos - V';
exec taxation.add_comercial_activity 'Generación de Electricidad';
exec taxation.add_comercial_activity 'Generaciónj Hidroeléctrica';
exec taxation.add_comercial_activity 'Minería';
exec taxation.add_comercial_activity 'REFINACION, INDUSTRIALIZACION Y COMERCIA';
exec taxation.add_comercial_activity 'Prod. Energía y Proces. de Combustible';
exec taxation.add_comercial_activity 'Distribución de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generadora y Comercializadora de Energia';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica y su Dis';
exec taxation.add_comercial_activity 'Venta y Distriobución de Energía';
exec taxation.add_comercial_activity 'Distribución de Energía Eléctrica';
exec taxation.add_comercial_activity 'Distribución de energía eléctrica';
exec taxation.add_comercial_activity 'Distribución y Venta de Energía Eléctric';
exec taxation.add_comercial_activity 'Distribución de Energía Eléctrica';
exec taxation.add_comercial_activity 'GENERACION Y DISTRIBUCION DE ENERGIA ELE';
exec taxation.add_comercial_activity 'DISTRIBUIDORA DE ENERGÍA';
exec taxation.add_comercial_activity 'DISTRIBUCION DE ENERGIA';
exec taxation.add_comercial_activity 'Generación y Distribución de Energía Elé';
exec taxation.add_comercial_activity 'Instalación Calefacción y Energía Solar';
exec taxation.add_comercial_activity 'Agrícola';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Venta y Distribución de Energía Eléctric';
exec taxation.add_comercial_activity 'GANADERA FORESTAL y ELECTRICA';
exec taxation.add_comercial_activity 'Gen. y Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Gen. y Trans. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Distribuidora de Energía Eléctrica';
exec taxation.add_comercial_activity 'Distribución de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación energía Eléctrica';
exec taxation.add_comercial_activity 'Gen. Energía Renovable no convencional';
exec taxation.add_comercial_activity 'Central de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Gen. y Comerc. de Energía';
exec taxation.add_comercial_activity 'Generación Energía Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'SOCIEDADES DE INVERSION Y RENTISTAS DE C';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'TRANSPORTE DE PASAJEROS';
exec taxation.add_comercial_activity 'Minería';
exec taxation.add_comercial_activity 'OTROS SERVICIOS AGRICOLAS';
exec taxation.add_comercial_activity 'Distribucion de Energia Electrica';
exec taxation.add_comercial_activity 'GENERAC, TRANSM, DIST. ELECTRICIDAD';
exec taxation.add_comercial_activity 'Generación, transmisión y distribución d';
exec taxation.add_comercial_activity 'Generación Eléctrica';
exec taxation.add_comercial_activity 'Gen. en otras centrales termoelectricas';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación de energía';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación y Venta de Energía';
exec taxation.add_comercial_activity 'Gen. hidroeléctrica, Vta de Ener. y Pot.';
exec taxation.add_comercial_activity 'Gen., Trans., Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación Eléctrica';
exec taxation.add_comercial_activity 'Generación de Energía hidroeléctrica';
exec taxation.add_comercial_activity 'Generacion de Enrgia Electrica';
exec taxation.add_comercial_activity 'Generacion Hidroelectrica';
exec taxation.add_comercial_activity 'GENERACION DE ENERGIA ELECTRICA';
exec taxation.add_comercial_activity 'Generación de Electricidad';
exec taxation.add_comercial_activity 'Gen., Trans., Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Comercialización de Energía Eléctrica ';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación Energía Eléctrica';
exec taxation.add_comercial_activity 'Gen, adquirir, dist y sum energía eléct.';
exec taxation.add_comercial_activity 'Generación Eléctrica Eólica';
exec taxation.add_comercial_activity 'Distribución de Energía Eléctrica';
exec taxation.add_comercial_activity 'Distribución de Energía Eléctrica';
exec taxation.add_comercial_activity 'Producción de Energía';
exec taxation.add_comercial_activity 'Obras Menores en Construcción';
exec taxation.add_comercial_activity 'Gen. y Dist. de Electricidad';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica y distribución';
exec taxation.add_comercial_activity 'Generacion de Energia Electrica';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'GENERACION EN CENTRALES TERMOELECTRICAS';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'ENERGIAS RENOVABLES';
exec taxation.add_comercial_activity 'Producción Energia Electrica';
exec taxation.add_comercial_activity 'Generación en otras Centrales N.C.P.';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación Energía Eléctrica';
exec taxation.add_comercial_activity 'Gen., trasm y Dist. de Enería Electrica';
exec taxation.add_comercial_activity 'Generación y venta de energía';
exec taxation.add_comercial_activity 'Gen., Compra y Vta Ener. y Pot. Eléctric';
exec taxation.add_comercial_activity 'Generación Eléctrica';
exec taxation.add_comercial_activity 'Gen. y Comer. de Energía Electrica';
exec taxation.add_comercial_activity 'Generación Energía Eléctrica';
exec taxation.add_comercial_activity 'Gen., Compra y Vta Ener. y Pot. Eléctric';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generacion Electrica';
exec taxation.add_comercial_activity 'Generación Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'Transmisión de Energía Eléctrica';
exec taxation.add_comercial_activity 'Gen., Trans., Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Gen. y Venta Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación en otras Centrales N.C.P.';
exec taxation.add_comercial_activity 'Generacion de Enrgia Electrica';
exec taxation.add_comercial_activity 'Central Hidroeléctrica';
exec taxation.add_comercial_activity 'Fabricación de Embases de Vidrio';
exec taxation.add_comercial_activity 'PRODUCCION DE PULPA DE MADERA';
exec taxation.add_comercial_activity 'INDUSTRIA QUIMICA';
exec taxation.add_comercial_activity 'Gen., Trans. y Venta Energía Eléctrica';
exec taxation.add_comercial_activity 'Extracción de Minerales de Hierro';
exec taxation.add_comercial_activity 'Generadora Energía Eléctrica';
exec taxation.add_comercial_activity 'Producción Energia Electrica';
exec taxation.add_comercial_activity 'Gen., Dist. y sum. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Exp., Prod., Dist., y Sum. Energía Elec.';
exec taxation.add_comercial_activity 'PRODUCCION DE PANELES DE MADERA';
exec taxation.add_comercial_activity 'Generación Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación, Distribución, Transmisión y ';
exec taxation.add_comercial_activity 'Distribución de Energía Eléctrica';
exec taxation.add_comercial_activity 'Distribuidora de Electricidad';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'Dist. y Sumin. Energía Eléctrica';
exec taxation.add_comercial_activity 'GENERACION DE ENERGIA ELECTRICA';
exec taxation.add_comercial_activity 'Eléctrico';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Gen. y Comerc. de Electricidad';
exec taxation.add_comercial_activity 'GENERACION DE ENERGIA ELECTRICA';
exec taxation.add_comercial_activity 'Generación Energia Eléctrica';
exec taxation.add_comercial_activity 'Elaboración de Alimentos, vtas materias ';
exec taxation.add_comercial_activity 'GENERACIÓN, TRANSMISIÓN Y VENTA DE ENERG';
exec taxation.add_comercial_activity 'Transmisión y Transformación de Energía';
exec taxation.add_comercial_activity 'Distribuidora de Electricidad';
exec taxation.add_comercial_activity 'Distribución y Venta de Energía Eléctric';
exec taxation.add_comercial_activity 'Gen. y Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Eléctrico';
exec taxation.add_comercial_activity 'Explotación transporte y compraventa de ';
exec taxation.add_comercial_activity 'Generación, Transmisión y Distribución d';
exec taxation.add_comercial_activity 'Venta y distribución de energía eléctric';
exec taxation.add_comercial_activity 'DISTRIBUCION Y VENTA DE ENERGIA ELECTRIC';
exec taxation.add_comercial_activity 'Distribución y Venta de Energía Eléctric';
exec taxation.add_comercial_activity 'GENERACIÓN, TRANSMISIÓN Y VENTA DE ENERG';
exec taxation.add_comercial_activity 'Generación de Electricidad';
exec taxation.add_comercial_activity 'Gen. Dist. y Comer. todo tipo Energía';
exec taxation.add_comercial_activity 'Distribuidora de Electricidad';
exec taxation.add_comercial_activity 'Producción y Distribución de Gas';
exec taxation.add_comercial_activity 'CENTRAL HIDROELECTRICA ';
exec taxation.add_comercial_activity 'Distribuidora de Energía Electrica';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Distribuidora de Energía Electrica';
exec taxation.add_comercial_activity 'Consultoría e Inversiones';
exec taxation.add_comercial_activity 'Gen., Trans., Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Gen., Trans., Dist. de Energía Eléctrica';
exec taxation.add_comercial_activity 'DISTRIBUCION Y VTA DE ENERGIA ELECTRICA';
exec taxation.add_comercial_activity 'Transmisión';
exec taxation.add_comercial_activity 'GENERACION DE ENERGIA ELECTRICA';
exec taxation.add_comercial_activity 'Gen. y Trans. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Transmitir, Generar, Transportar, Compra';
exec taxation.add_comercial_activity 'Transmisión de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación, Transmisión y Suministro de';
exec taxation.add_comercial_activity 'Asesorias informatica';
exec taxation.add_comercial_activity 'Gen.,Trans. y Dist. de Suministro Eléctr';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación en otras Centrales Termoeléct';
exec taxation.add_comercial_activity 'Gen., Trans. y Dist. Suministro Elec.';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación en otras centrales';
exec taxation.add_comercial_activity 'Comercialización de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generacion en otras centrales N.C.P.';
exec taxation.add_comercial_activity 'Proyectos de Gener. Energia Electrica';
exec taxation.add_comercial_activity 'Gen. y Trans. de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Gen., Transp. y Dist. Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación ELéctrica';
exec taxation.add_comercial_activity 'Generación de Electricidad';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'REPARACION DE OTROS TIPOS DE EQUIPO ELEC';
exec taxation.add_comercial_activity 'Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'Agrícola, ganadera, piscicultura';
exec taxation.add_comercial_activity 'Transmisión y distribución eléctrica';
exec taxation.add_comercial_activity 'Generación de energía eléctrica';
exec taxation.add_comercial_activity 'Generación de energía eléctrica';
exec taxation.add_comercial_activity 'GENERACION EN OTRAS CENTRALES';
exec taxation.add_comercial_activity 'DISTRIBUCION DE ENERGIA ELECTRICA';
exec taxation.add_comercial_activity 'Ingeniería eléctrica';
exec taxation.add_comercial_activity 'Ingeniería y Construcción';
exec taxation.add_comercial_activity 'Ingeniería, Instalciones y Montajes eléc';
exec taxation.add_comercial_activity 'Construcción Electromecánica';
exec taxation.add_comercial_activity 'Compra, venta, generación y distribución';
exec taxation.add_comercial_activity 'Generación en otras centrales N.C.P.';
exec taxation.add_comercial_activity 'Generación en otras centrales N.C.P.';
exec taxation.add_comercial_activity 'Generación en otras centrales N.C.P';
exec taxation.add_comercial_activity 'Desarrollo e inversión de proyectos de G';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'Transmisión de Energía Eléctrica';
exec taxation.add_comercial_activity 'Minería';
exec taxation.add_comercial_activity 'Producción de Energía';
exec taxation.add_comercial_activity 'Montajes Industriales';
exec taxation.add_comercial_activity 'Ingeniería y Construcción';
exec taxation.add_comercial_activity 'Ingeniería y Construcción';
exec taxation.add_comercial_activity 'Ingeniería y Constrcción';
exec taxation.add_comercial_activity 'Contratista Construcción Obras Menores';
exec taxation.add_comercial_activity 'Const. y Mont. de Líneas y SE Electricas';
exec taxation.add_comercial_activity 'Generación Eleéctrica';
exec taxation.add_comercial_activity 'Servicios de Ingeniería y otras activida';
exec taxation.add_comercial_activity 'Generación de Energía Hidroeléctrica';
exec taxation.add_comercial_activity 'Gen., Dist. y Trans. Energía Eléctrica';
exec taxation.add_comercial_activity 'Desarrollo e inversión de proyectos de G';
exec taxation.add_comercial_activity 'Fábrica de Electrodo y Gases';
exec taxation.add_comercial_activity 'Proyectos de Energía Renovable';
exec taxation.add_comercial_activity 'Transmisión Energía Eléctrica';
exec taxation.add_comercial_activity 'Distribucion de Energia Electrica';
exec taxation.add_comercial_activity 'Proyectos de Energía Renovable y Comerci';
exec taxation.add_comercial_activity 'Generación Hidroeléctrica';
exec taxation.add_comercial_activity 'Generación y Comercialización de Energía';
exec taxation.add_comercial_activity 'Gen. Dist. Trans. Com. Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'GENERAC TRANSM DISTR ENERG ELECTRICA, IN';
exec taxation.add_comercial_activity 'GENERACION DE ENERGIA ELECTRICA';
exec taxation.add_comercial_activity 'Comercialización y administración de obr';
exec taxation.add_comercial_activity 'OBRAS DE INGENIERIA Y GENERACION HIDROEL';
exec taxation.add_comercial_activity 'GENERACION HIDROELECTRICA';
exec taxation.add_comercial_activity 'Generación de Energía Eléctrica';
exec taxation.add_comercial_activity 'Generación de Energía';
exec taxation.add_comercial_activity 'GENERACION ELECTRICIDAD';
exec taxation.add_comercial_activity 'GENERACION EN OTRAS CENTRALES N.C.P.';
exec taxation.add_comercial_activity 'Servicios de Vertederos';
exec taxation.add_comercial_activity 'GENERACION EN OTRAS CENTRALES N.C.P.';
exec taxation.add_comercial_activity 'TRANSMISION DE ENERGIA ELECTRICA';

-------------------------------------------- END Economic Activities

INSERT INTO organization.address_type (name, [description]) values
('legal', 'Dirección legal de la empresa'),
('service_reception', 'Lugar donde se recibirán los productos/servicios'),
('supply', 'Dirección de suministros');

DECLARE @enterprise_id UNIQUEIDENTIFIER;
DECLARE @employee_id UNIQUEIDENTIFIER;
DECLARE @role_id UNIQUEIDENTIFIER;
DECLARE @account_role_id UNIQUEIDENTIFIER;
DECLARE @account_id UNIQUEIDENTIFIER;
DECLARE @sector_id UNIQUEIDENTIFIER;
DECLARE @person_id UNIQUEIDENTIFIER;
declare @authorized_account_id uniqueidentifier;
declare @comercial_activity uniqueidentifier;
declare @address_id uniqueidentifier;
declare @commune_id uniqueidentifier = (select id from location.commune where name = 'Santiago');

EXECUTE organization.add_tenant
	@tax_id = '764037022',
	@legal_name = 'Sociedad Tecnologa Valorti Limitada',
	@display_name = 'Valor TI',
	@email = 'contacto@valorti.com',
	@phone = '',
	@id = @enterprise_id OUT;

EXECUTE system.add_person
	@first_name = 'Nicolas',
	@last_name = 'Mancilla',
	@national_id = '158448165',
	@birth_date = null,
	@gender = null,
	@id = @person_id OUT;

execute organization.add_employee
	@person_id,
	@enterprise_id,
	@employee_id out;

execute system.create_account
	@login = 'nmancilla@valorti.com',
	@email = 'nmancilla@valorti.com',
	@password = 0x7F074495BA09DABC6D4EEE19220C84BB7AAC86B20B8D94A7BB1AF2B09BBE1D3FF434AD8498255805EF28B7AD4FE75B842F490A915E0E7D8E1E973B2493A31EF8A4C597CCA8F1AEDCF2153A152464F6EB9D5D7A551AF976D5287F8272C2BFAE470A657578B8BD660ACF43D906C36ECE5F69168A83C9980472FD552B008D332B5A,
	@salt = 0x0001000000FFFFFFFF01000000000000000C0200000054446F62656C696B2E5574696C732E50617373776F72642C2056657273696F6E3D312E302E353732352E32313930392C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D6E756C6C05010000001F446F62656C696B2E5574696C732E50617373776F72642E48617368496E666F0500000002686C02736C026963027373027370000000010008080808020000004000000040000000410000000603000000017C040000000B000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000,
	@person_id = @person_id,
	@id = @account_id out;

execute system.authorize_account
	@enterprise_id,
	@account_id,
	@authorized_account_id out;

EXECUTE [system].add_role 
	'administrator',
	'Administrador',
	'Tiene todos los permisos de la aplicación',
	@enterprise_id,
	@id = @role_id OUT;

EXECUTE [system].add_account_role
	@authorized_account_id,
	@role_id;

-- #RealData
insert into taxation.comercial_activity (name) values
('Obras de Ingeniería'),
('Amanecer Solar SPA'),
('San Andrés SPA'),
('Gen. y Dist. de Energía Elect.');

exec [system].create_module 'document_reception', 'Allows the tenant to receive documents';
exec [system].create_module 'document_emission', 'Allows the tenant to create and manage documents';
exec [system].create_module 'document_reception_trace', 'Allows the tenant to trace the received documents with the ones that should have been received';
exec [system].create_module 'document_emission_signing', 'Allows the integration of a signing entity for the emitted documents';
exec [system].create_module 'accounting_voucher_export', 'Allows exportation of the accounting vouchers with some format';
exec [system].create_module 'accounting_voucher', 'Allows creation of accounting vouchers';

-- TODO: correct bill_resolution_[date|number].

SET @sector_id = (SELECT id FROM taxation.economic_activity WHERE code = '401019');

----------------- Sunedison -----------------------
--- User
INSERT [system].[person] ([id], [first_name], [last_name], [national_id], [birth_date], [gender]) VALUES (N'f28e9550-8b5b-4ee2-ba7e-a5ef00d546d3', N'Carolina', N'Fabrega', N'22757608-1', NULL, 0)
INSERT [system].[account] ([id], [login], [email], [password], [salt]) VALUES (N'f28e9550-8b5b-4ee2-ba7e-a5ef00d546d3', N'cfabrega@sunedison.com', N'cfabrega@sunedison.com', 0x24AD3F3064809D5023E953CF1902A89B79810213BB075DE116C5289219517DD792791FDE35B260F3F5BDE7C8696EB8AB9E7F1B629F5C2D6A34AE84560CAE76B2FA555067F9A05E789DF80A3AF6922C78BF5E7E679ACD9124D0EAB032FBFA36D4AE68DA800A60D54CB5AE7D6FF9C4F176DD91EED7887A2B8E8F2D76D31E68DD9A, 0x0001000000FFFFFFFF01000000000000000C0200000053446F62656C696B2E5574696C732E50617373776F72642C2056657273696F6E3D312E302E353935352E313631302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D6E756C6C05010000001F446F62656C696B2E5574696C732E50617373776F72642E48617368496E666F0500000002686C02736C026963027373027370000000010008080808020000004000000040000000360000000603000000017C020000000B00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000)

----------------- Sunedison
exec organization.add_tenant
	@tax_id = '762630842',
	@legal_name = 'Sunedison Chile Construction Limitada',
	@display_name = 'Sunedison',
	@email = '',
	@phone = '',
	@bank_account_number = '',
	@bill_resolution_date = '',
	@bill_resolution_number = 0,
	@is_enabled = 1,
	@id = @enterprise_id OUT;

execute organization.add_tenant_address
	@enterprise_id,
	'Magdalena 140, Piso 10, Of. 1001',
	'',
	@commune_id;

execute system.add_setting
	'repository_folder_name',
	'SunedisonChileConstructionLimitada',
	@enterprise_id;

execute system.add_setting
	'signing_provider',
	'provider=artikos_cl;folder_name=SunedisonChileConstructionLimitada',
	@enterprise_id;

exec taxation.add_comercial_activity
	'Obras de Ingeniería',
	@comercial_activity out;

exec taxation.set_tenant_comercial_activity
	@enterprise_id,
	@comercial_activity;

EXECUTE taxation.set_tenant_economic_activity
	@enterprise_id,
	@sector_id;

EXECUTE [system].add_role
	'administrator',
	'Administrador',
	'Tiene todos los permisos de la aplicación',
	@enterprise_id,
	@id = @role_id OUT;

----------------- San Andres SPA
EXECUTE organization.add_tenant
	@tax_id = '762735695',
	@legal_name = 'San Andrés SPA',
	@display_name = 'San Andrés',
	@email = '',
	@phone = '',
	@bank_account_number = '',
	@bill_resolution_date = '',
	@bill_resolution_number = 0,
	@is_enabled = 1,
	@id = @enterprise_id OUT;

execute organization.add_tenant_address
	@enterprise_id,
	'Magdalena 140, Piso 10, Of. 1001',
	'',
	@commune_id;

execute system.add_setting
	'repository_folder_name',
	'SanAndresSPA',
	@enterprise_id;

execute system.add_setting
	'signing_provider',
	'provider=artikos_cl;folder_name=SanAndresSPA',
	@enterprise_id;

exec taxation.add_comercial_activity
	'San Andrés SPA',
	@comercial_activity out;

exec taxation.set_tenant_comercial_activity
	@enterprise_id,
	@comercial_activity;

EXECUTE taxation.set_tenant_economic_activity
	@enterprise_id,
	@sector_id;

EXECUTE [system].add_role
	'administrator',
	'Administrador',
	'Tiene todos los permisos de la aplicación',
	@enterprise_id,
	@id = @role_id OUT;

----------------- Generación Solar SPA
EXECUTE organization.add_tenant
	@tax_id = '761830759',
	@legal_name = 'Generación Solar SPA',
	@display_name = 'Generacion Solar',
	@email = '',
	@phone = '',
	@bank_account_number = '',
	@bill_resolution_date = '',
	@bill_resolution_number = 0,
	@is_enabled = 1,
	@id = @enterprise_id OUT;

execute organization.add_tenant_address
	@enterprise_id,
	'Magdalena 140, Piso 10, Of. 1001',
	'',
	@commune_id;

execute system.add_setting
	'repository_folder_name',
	'GeneracionSolarSPA',
	@enterprise_id;

execute system.add_setting
	'signing_provider',
	'provider=artikos_cl;folder_name=GeneracionSolarSPA',
	@enterprise_id;

exec taxation.add_comercial_activity
	'Generación en Otras Centrales',
	@comercial_activity out;

exec taxation.set_tenant_comercial_activity
	@enterprise_id,
	@comercial_activity;

EXECUTE taxation.set_tenant_economic_activity
	@enterprise_id,
	@sector_id;

EXECUTE [system].add_role
	'administrator',
	'Administrador',
	'Tiene todos los permisos de la aplicación',
	@enterprise_id,
	@id = @role_id OUT;

----------------- Javiera SPA
EXECUTE organization.add_tenant
	@tax_id = '763766357',
	@legal_name = 'Javiera SPA',
	@display_name = 'Javiera',
	@email = '',
	@phone = '', 
	@bank_account_number = '',
	@bill_resolution_date = '',
	@bill_resolution_number = 0,
	@is_enabled = 1,
	@id = @enterprise_id OUT;

execute organization.add_tenant_address
	@enterprise_id,
	'Magdalena 140, Piso 10, Of. 1001',
	'',
	@commune_id;

execute system.add_setting
	'repository_folder_name',
	'JavieraSPA',
	@enterprise_id;

execute system.add_setting
	'signing_provider',
	'provider=artikos_cl;folder_name=JavieraSPA',
	@enterprise_id;

exec taxation.add_comercial_activity
	'Generación en Otras Centrales',
	@comercial_activity out;

exec taxation.set_tenant_comercial_activity
	@enterprise_id,
	@comercial_activity;

EXECUTE taxation.set_tenant_economic_activity
	@enterprise_id,
	@sector_id;

EXECUTE [system].add_role
	'administrator',
	'Administrador',
	'Tiene todos los permisos de la aplicación',
	@enterprise_id,
	@id = @role_id OUT;

----------------- Amanecer Solar SPA
EXECUTE organization.add_tenant
	@tax_id = '762735598',
	@legal_name = 'Amanecer Solar SPA',
	@display_name = 'Amanecer Solar',
	@email = '',
	@phone = '',
	@bank_account_number = '',
	@bill_resolution_date = '',
	@bill_resolution_number = 0,
	@is_enabled = 1,
	@id = @enterprise_id OUT;

execute organization.add_tenant_address
	@enterprise_id,
	'Magdalena 140, Piso 10, Of. 1001',
	'',
	@commune_id;

execute system.add_setting
	'repository_folder_name',
	'AmanecerSolarSPA',
	@enterprise_id;

execute system.add_setting
	'signing_provider',
	'provider=artikos_cl;folder_name=AmanecerSolarSPA',
	@enterprise_id;

exec taxation.add_comercial_activity
	'Amanecer Solar SPA',
	@comercial_activity out;

exec taxation.set_tenant_comercial_activity
	@enterprise_id,
	@comercial_activity;

EXECUTE taxation.set_tenant_economic_activity
	@enterprise_id,
	@sector_id;

EXECUTE [system].add_role
	'administrator',
	'Administrador',
	'Tiene todos los permisos de la aplicación',
	@enterprise_id,
	@id = @role_id OUT;

----------------- Chungungo SPA
EXECUTE organization.add_tenant
	@tax_id = '764141075',
	@legal_name = 'Chungungo S.A.',
	@display_name = 'Chungungo',
	@email = '',
	@phone = '',
	@bank_account_number = '',
	@bill_resolution_date = '',
	@bill_resolution_number = 0,
	@is_enabled = 1,
	@id = @enterprise_id OUT;

execute organization.add_tenant_address
	@enterprise_id,
	'Magdalena 140, Piso 10, Of. 1001',
	'',
	@commune_id;

execute system.add_setting
	'repository_folder_name',
	'ChungungoSA',
	@enterprise_id;

execute system.add_setting
	'signing_provider',
	'provider=artikos_cl;folder_name=ChungungoSA',
	@enterprise_id;

exec taxation.add_comercial_activity
	'Gen. y Dist. de Energía Elect.',
	@comercial_activity out;

exec taxation.set_tenant_comercial_activity
	@enterprise_id,
	@comercial_activity;

set @sector_id = (select id from taxation.economic_activity where code = '401030');
EXECUTE taxation.set_tenant_economic_activity
	@enterprise_id,
	@sector_id;

EXECUTE [system].add_role
	'administrator',
	'Administrador',
	'Tiene todos los permisos de la aplicación',
	@enterprise_id,
	@id = @role_id OUT;

insert into system.authorized_account (tenant_id, account_id) values
((select id from organization.tenant where tax_id = '762630842'), 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3'),
((select id from organization.tenant where tax_id = '762735695'), 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3'),
((select id from organization.tenant where tax_id = '761830759'), 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3'),
((select id from organization.tenant where tax_id = '763766357'), 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3'),
((select id from organization.tenant where tax_id = '762735598'), 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3'),
((select id from organization.tenant where tax_id = '764141075'), 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3');


set @enterprise_id = (select id from organization.tenant where tax_id = '762630842');
insert into system.account_role (role_id, authorized_account_id) values
(
	(select id from [system].[role] where name = 'administrator' and tenant_id = @enterprise_id),
	(select id from system.authorized_account where account_id = 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3' and tenant_id = @enterprise_id)
);

set @enterprise_id = (select id from organization.tenant where tax_id = '762735695');
insert into system.account_role (role_id, authorized_account_id) values
(
	(select id from [system].[role] where name = 'administrator' and tenant_id = @enterprise_id),
	(select id from system.authorized_account where account_id = 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3' and tenant_id = @enterprise_id)
);

set @enterprise_id = (select id from organization.tenant where tax_id = '761830759');
insert into system.account_role (role_id, authorized_account_id) values
(
	(select id from [system].[role] where name = 'administrator' and tenant_id = @enterprise_id),
	(select id from system.authorized_account where account_id = 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3' and tenant_id = @enterprise_id)
);

set @enterprise_id = (select id from organization.tenant where tax_id = '763766357');
insert into system.account_role (role_id, authorized_account_id) values
(
	(select id from [system].[role] where name = 'administrator' and tenant_id = @enterprise_id),
	(select id from system.authorized_account where account_id = 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3' and tenant_id = @enterprise_id)
);

set @enterprise_id = (select id from organization.tenant where tax_id = '762735598');
insert into system.account_role (role_id, authorized_account_id) values
(
	(select id from [system].[role] where name = 'administrator' and tenant_id = @enterprise_id),
	(select id from system.authorized_account where account_id = 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3' and tenant_id = @enterprise_id)
);

set @enterprise_id = (select id from organization.tenant where tax_id = '764141075');
insert into system.account_role (role_id, authorized_account_id) values
(
	(select id from [system].[role] where name = 'administrator' and tenant_id = @enterprise_id),
	(select id from system.authorized_account where account_id = 'F28E9550-8B5B-4EE2-BA7E-A5EF00D546D3' and tenant_id = @enterprise_id)
);

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762630842'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762630842'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762630842'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735695'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735695'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735695'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '761830759'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '761830759'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '761830759'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '763766357'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '763766357'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '763766357'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735598'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735598'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '762735598'),
	(select id from system.module where name = 'document_emission_signing')
)

-------------------------
insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '764141075'),
	(select id from system.module where name = 'document_reception')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '764141075'),
	(select id from system.module where name = 'document_emission')
)

insert into system.tenant_module (tenant_id, module_id) values
(
	(select id from organization.tenant where tax_id = '764141075'),
	(select id from system.module where name = 'document_emission_signing')
)

-- #RealData --