--USE zentom_mssql;

IF OBJECT_ID('taxation.tax_document_reference', 'U') IS NOT NULL
	DROP TABLE [taxation].[tax_document_reference];

IF OBJECT_ID('taxation.tax_document_service', 'U') IS NOT NULL
	DROP TABLE [taxation].[tax_document_service];

IF OBJECT_ID('taxation.tax_document_folio', 'U') IS NOT NULL
	DROP TABLE [taxation].[tax_document_folio];

if object_id('taxation.folio_range', 'U') is not null
	drop table taxation.folio_range;

if object_id('accounting.accounting_account', 'U') is not null
	drop table accounting.accounting_account;

if object_id('accounting.accounting_voucher_line', 'U') is not null
	drop table accounting.accounting_voucher_line;

if object_id('accounting.accounting_voucher', 'U') is not null
	drop table accounting.accounting_voucher;

if object_id('taxation.received_taxdoc_weak_reference', 'U') is not null
	drop table taxation.received_taxdoc_weak_reference;

IF OBJECT_ID('taxation.tax_document_line', 'U') IS NOT NULL
	DROP TABLE taxation.tax_document_line;

if object_id ('taxation.tax_document_weak_reference', 'U') is not null
	drop table taxation.tax_document_weak_reference;

IF OBJECT_ID('taxation.tax_document_totals', 'U') IS NOT NULL
	DROP TABLE taxation.tax_document_totals;

IF OBJECT_ID('system.option_permission', 'U') IS NOT NULL
	DROP TABLE [system].[option_permission];

IF OBJECT_ID('organization.enterprise_employee', 'U') IS NOT NULL
	DROP TABLE organization.enterprise_employee;

IF OBJECT_ID('organization.enterprise_address_type', 'U') is not null
	drop table organization.enterprise_address_type;

IF OBJECT_ID('organization.tenant_address_type', 'U') is not null
	drop table organization.tenant_address_type;

if object_id ('organization.tenant_group', 'U') is not null
	drop table organization.tenant_group;

IF OBJECT_ID('system.option', 'U') IS NOT NULL
	DROP TABLE [system].[option];

IF OBJECT_ID('system.role_permission', 'U') IS NOT NULL
	DROP TABLE [system].[role_permission];

IF OBJECT_ID('system.permission', 'U') IS NOT NULL
	DROP TABLE [system].[permission];

IF OBJECT_ID('system.account_role', 'U') IS NOT NULL
	DROP TABLE [system].[account_role];

if object_id ('taxation.enterprise_sector', 'U') is not null
	drop table taxation.enterprise_sector;

if object_id ('taxation.tenant_sector', 'U') is not null
	drop table taxation.tenant_sector;

if object_id ('taxation.sector', 'U') is not null
	drop table taxation.sector;

if object_id ('system.authorized_account', 'U') is not null
	drop table system.authorized_account;

IF OBJECT_ID('system.role', 'U') IS NOT NULL
	DROP TABLE [system].[role];

IF OBJECT_ID('organization.client', 'U') IS NOT NULL
	DROP TABLE organization.[client];

IF OBJECT_ID('organization.enterprise_tenant', 'U') IS NOT NULL
	DROP TABLE organization.enterprise_tenant;

IF OBJECT_ID('taxation.enterprise_economic_activity', 'U') IS NOT NULL
	DROP TABLE [taxation].[enterprise_economic_activity];

IF OBJECT_ID('taxation.tenant_economic_activity', 'U') IS NOT NULL
	DROP TABLE [taxation].[tenant_economic_activity];

IF OBJECT_ID('taxation.enterprise_comercial_activity', 'U') IS NOT NULL
	DROP TABLE [taxation].[enterprise_comercial_activity];

IF OBJECT_ID('taxation.tenant_comercial_activity', 'U') IS NOT NULL
	DROP TABLE [taxation].[tenant_comercial_activity];

IF OBJECT_ID('organization.enterprise_contact_info', 'U') IS NOT NULL
	DROP TABLE organization.[enterprise_contact_info];

IF OBJECT_ID('organization.product', 'U') IS NOT NULL
	DROP TABLE organization.[product];

IF OBJECT_ID('organization.service', 'U') IS NOT NULL
	DROP TABLE organization.[service];

IF OBJECT_ID('taxation.payment', 'U') IS NOT NULL
	DROP TABLE [taxation].[payment];

if object_id('taxation.received_taxdoc_reference', 'U') is not null
	drop table taxation.received_taxdoc_reference;

if object_id('taxation.received_tax_document_receptor', 'U') is not null
	drop table taxation.received_tax_document_receptor;

if object_id('taxation.received_tax_document_line', 'U') is not null
	drop table taxation.received_tax_document_line;

if object_id('taxation.received_tax_document', 'U') is not null
	drop table taxation.received_tax_document;

if object_id ('taxation.taxdoc_client_address_type', 'U') is not null
	drop table taxation.taxdoc_client_address_type;

if object_id('taxation.taxdoc_client_address', 'U') is not null
	drop table taxation.taxdoc_client_address;

if object_id('taxation.taxdoc_client', 'U') is not null
	drop table taxation.taxdoc_client;

IF OBJECT_ID('taxation.tax_document', 'U') IS NOT NULL
	DROP TABLE [taxation].[tax_document];

IF OBJECT_ID('taxation.economic_activity', 'U') IS NOT NULL
	DROP TABLE [taxation].[economic_activity];

IF OBJECT_ID('taxation.comercial_activity', 'U') IS NOT NULL
	DROP TABLE [taxation].[comercial_activity];

IF OBJECT_ID('organization.employee_contact_info', 'U') IS NOT NULL
	DROP TABLE organization.employee_contact_info;

IF OBJECT_ID('organization.person_contact_info', 'U') IS NOT NULL
	DROP TABLE organization.person_contact_info;

IF OBJECT_ID('organization.contact_info', 'U') IS NOT NULL
	DROP TABLE organization.contact_info;

if object_id ('system.tax_document_template', 'U') is not null
	drop table [system].tax_document_template;

if object_id ('system.password_request', 'U') is not null
	drop table [system].password_request;

IF OBJECT_ID('system.account', 'U') IS NOT NULL
	DROP TABLE [system].[account];

IF OBJECT_ID('organization.employee', 'U') IS NOT NULL
	DROP TABLE organization.[employee];

IF OBJECT_ID('organization.person', 'U') IS NOT NULL
	DROP TABLE organization.[person];

IF OBJECT_ID('organization.contact_type', 'U') IS NOT NULL
	DROP TABLE organization.[contact_type];

IF OBJECT_ID('organization.enterprise_address', 'U') IS NOT NULL
	DROP TABLE organization.[enterprise_address];

IF OBJECT_ID('organization.tenant_address', 'U') IS NOT NULL
	DROP TABLE organization.[tenant_address];
	
if object_id ('system.tenant_module', 'U') is not null
	drop table system.tenant_module;

IF OBJECT_ID ('system.enterprise_settings', 'U') IS NOT NULL
	DROP TABLE [system].enterprise_settings;

IF OBJECT_ID ('system.tenant_settings', 'U') IS NOT NULL
	DROP TABLE [system].tenant_settings;

if object_id('organization.costs_center', 'U') is not null
	drop table [organization].costs_center;

IF OBJECT_ID('organization.enterprise', 'U') IS NOT NULL
	DROP TABLE organization.[enterprise];

IF OBJECT_ID('taxation.reduction_type', 'U') IS NOT NULL
	DROP TABLE [taxation].[reduction_type];

IF OBJECT_ID('taxation.specific_tax', 'U') IS NOT NULL
	DROP TABLE [taxation].[specific_tax];

IF OBJECT_ID('taxation.dispatch_type', 'U') IS NOT NULL
	DROP TABLE [taxation].[dispatch_type];

IF OBJECT_ID('taxation.payment_type', 'U') IS NOT NULL
	DROP TABLE [taxation].[payment_type];

IF OBJECT_ID('taxation.document_type', 'U') IS NOT NULL
	DROP TABLE [taxation].[document_type];

IF OBJECT_ID('taxation.service_type', 'U') IS NOT NULL
	DROP TABLE [taxation].[service_type];

IF OBJECT_ID('i18n.country_locale', 'U') IS NOT NULL
	DROP TABLE [i18n].[country_locale];

IF OBJECT_ID('location.address', 'U') IS NOT NULL
	DROP TABLE [location].[address];

IF OBJECT_ID('location.commune', 'U') IS NOT NULL
	DROP TABLE [location].[commune];

IF OBJECT_ID('location.city', 'U') IS NOT NULL
	DROP TABLE [location].[city];

IF OBJECT_ID('location.country', 'U') IS NOT NULL
	DROP TABLE [location].[country];

IF OBJECT_ID('i18n.locale', 'U') IS NOT NULL
	DROP TABLE [i18n].[locale];

if object_id ('organization.address_type', 'U') is not null
	drop table organization.address_type;

if object_id ('customization.property', 'U') is not null
	drop table customization.property;

IF OBJECT_ID ('organization.tenant', 'U') IS NOT NULL
	DROP TABLE organization.tenant;

if object_id ('location.address', 'U') is not null
	drop table [location].[address];

if object_id ('system.person', 'U') is not null
	drop table system.person;

if object_id ('organization.group', 'U') is not null
	drop table organization.[group];

if object_id ('system.module', 'U') is not null
	drop table system.module;

-- Schemas
IF NOT EXISTS (SELECT schema_name FROM INFORMATION_SCHEMA.SCHEMATA WHERE schema_name = 'i18n')
BEGIN
	EXEC sp_executesql N'CREATE SCHEMA i18n'
END

IF NOT EXISTS (SELECT schema_name FROM INFORMATION_SCHEMA.SCHEMATA WHERE schema_name = 'system')
BEGIN
	EXEC sp_executesql N'CREATE SCHEMA system'
END

IF NOT EXISTS (SELECT schema_name FROM INFORMATION_SCHEMA.SCHEMATA WHERE schema_name = 'organization')
BEGIN
	EXEC sp_executesql N'CREATE SCHEMA organization'
END

IF NOT EXISTS (SELECT schema_name FROM INFORMATION_SCHEMA.SCHEMATA WHERE schema_name = 'location')
BEGIN
	EXEC sp_executesql N'CREATE SCHEMA location'
END

IF NOT EXISTS (SELECT schema_name FROM INFORMATION_SCHEMA.SCHEMATA WHERE schema_name = 'taxation')
BEGIN
	EXEC sp_executesql N'CREATE SCHEMA taxation'
END

IF NOT EXISTS (SELECT schema_name FROM INFORMATION_SCHEMA.SCHEMATA WHERE schema_name = 'accounting')
BEGIN
	EXEC sp_executesql N'CREATE SCHEMA accounting'
END

if not exists (select SCHEMA_NAME from INFORMATION_SCHEMA.SCHEMATA where SCHEMA_NAME = 'customization')
begin
	exec sp_executesql N'create schema customization';
end

----------------------------------------- System

-- TODO: System.Modules

-- TODO: Audit changes.
--create table [system].registry (
--	id uniqueidentifier rowguidcol not null default newsequentialid(),
--	row_version timestamp not null,
--	table_name varchar(255) not null,
--	change_date datetime not null,
--	account_id uniqueidentifier not null,
--	constraint registry_pk primary key (id)
--);

-- System.Permission
CREATE TABLE [system].[permission] (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(100) NOT NULL,
	[description] nvarchar(300) NOT NULL default '',
	constraint permission_pk primary key (id),
	constraint permission_name_un unique (name)
);

-- System.Role
CREATE TABLE [system].[role] (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(100) NOT NULL,
	display_name nvarchar(100) NOT NULL,
	[description] nvarchar(300) NOT NULL default '',
	parent_role_id uniqueidentifier null default null,
	tenant_id uniqueidentifier NOT NULL,
	constraint role_pk primary key (id),
	constraint role_un unique (tenant_id, name)
);

-- System.RolePermission
CREATE TABLE [system].role_permission (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	role_id uniqueidentifier NOT NULL,
	permission_id uniqueidentifier NOT NULL,
	row_version TIMESTAMP NOT NULL,
	constraint role_permission_pk primary key (id),
	constraint role_permission_un unique (role_id, permission_id)
);

-- System.Account, SYSTEM WIDE UNIQUE - one person has one account.
CREATE TABLE [system].[account] (
	id uniqueidentifier ROWGUIDCOL NOT NULL, -- FK to person
	row_version TIMESTAMP NOT NULL,
	login nvarchar(100) NOT NULL,
	email varchar(1000) NOT NULL,
	[password] BINARY(128) NOT NULL,
	salt BINARY(500) NOT NULL,
	constraint account_pk primary key (id),
	constraint account_un unique ([login])
);

-- System.Person, SYSTEM WIDE - people is unique.
create table [system].person (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	first_name nvarchar(100) NOT NULL,
	last_name nvarchar(100) NOT NULL,
	national_id varchar(20) NOT NULL,
	birth_date date NULL default NULL,
	gender tinyint not null default 0,
	constraint person_pk primary key (id),
	constraint person_un unique (national_id)
);

-- System.PasswordRequest
create table [system].password_request (
	id uniqueidentifier rowguidcol not null,
	row_version timestamp not null,
	reset_expire_date datetime null,
	request_date datetime not null,
	ipv4 varchar(12) not null,
	ipv6 varchar(39) null,
	account_id uniqueidentifier not null,
	constraint password_reset_pk primary key (id)
);

-- System.Template
create table [system].tax_document_template (
	id uniqueidentifier ROWGUIDCOL NOT NULL,
	row_version TIMESTAMP NOT NULL,
	name nvarchar(255) not null,
	document_type nvarchar(20) not null,
	file_path varchar(max) not null,
	created_at datetime not null,
	account_id uniqueidentifier not null,
	tenant_id uniqueidentifier not null,
	constraint template_pk primary key (id),
	constraint template_un unique (name, account_id, tenant_id)
);

-- System.AccountRole
CREATE TABLE [system].account_role (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	authorized_account_id uniqueidentifier NOT NULL,
	role_id uniqueidentifier NOT NULL,
	row_version TIMESTAMP NOT NULL,
	constraint account_role_pk primary key (id),
	constraint account_role_un unique (authorized_account_id, role_id)
);

-- System.AuthorizedAccount
create table system.authorized_account (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	tenant_id uniqueidentifier not null,
	account_id uniqueidentifier not null,
	constraint authorized_account_pk primary key (id),
	constraint authorized_account_un unique (tenant_id, account_id)
);
 
-- System.Settings
CREATE TABLE [system].tenant_settings (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP not null,
	setting_name varchar(255) NOT NULL,
	setting_value nvarchar(MAX) NOT NULL,
	tenant_id uniqueidentifier NOT NULL,
	constraint tenant_settings_pk primary key (id),
	constraint tenant_settings_un unique (tenant_id, setting_name)
);

create table [system].module (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	name varchar(100) not null,
	[description] nvarchar(250) not null default '',
	is_deprecated bit not null default 0,
	default_settings nvarchar(max) not null default '',
	constraint module_pk primary key (id),
	constraint module_un unique (name)
);

create table [system].tenant_module (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp NOT NULL,
	module_id uniqueidentifier not null,
	tenant_id uniqueidentifier not null,
	is_enabled bit not null default 0,
	settings nvarchar(max) not null default '',
	[version] varchar(100) not null default '1.0.0',
	constraint tenant_module_pk primary key (id),
	constraint tenant_module_un unique (module_id, tenant_id)
);



-- System.AddressType
create table organization.address_type (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(100) not null,
	[description] nvarchar(1000) not null default '',
	constraint address_type_pk primary key (id),
	constraint address_type_un unique (name)
);

----------------------------------------- Organization
-- Organization.Employee
CREATE TABLE organization.employee (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	person_id uniqueidentifier not null,
	tenant_id uniqueidentifier not null,
	constraint employee_pk primary key (id),
	constraint employee_un unique (tenant_id, person_id)
);

-- Organization.Group
create table organization.[group] (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	name nvarchar(200) not null,
	headquarters uniqueidentifier null,
	constraint group_pk primary key (id),
	constraint group_un unique (name)
);

-- Organization.Tenant
create table organization.tenant (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	tax_id varchar(20) NOT NULL,
	legal_name nvarchar(100) NOT NULL,
	display_name nvarchar(100) NOT NULL default '',
	email varchar(1000) NOT NULL,
	phone varchar(50) NOT NULL,
	bill_resolution_date DATE NULL,
	bill_resolution_number INT NULL,
	bank_account_number varchar(15) NOT NULL default '',
	is_enabled BIT NOT NULL default 1,
	constraint tenant_pk primary key (id),
	constraint tenant_un unique (legal_name, tax_id)
);

-- Organization.TenantGroup
create table organization.tenant_group (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	group_id uniqueidentifier not null,
	tenant_id uniqueidentifier not null,
	constraint tenant_group_pk primary key (id),
	constraint tenant_group_un unique (tenant_id, group_id)
);

-- Organization.Enterprise
CREATE TABLE organization.enterprise (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	tax_id varchar(20) NOT NULL,
	legal_name nvarchar(100) NOT NULL,
	display_name nvarchar(100) NOT NULL default '',
	email varchar(1000) NOT NULL,
	phone varchar(50) NOT NULL,
	bank_account_number varchar(15) NOT NULL default '',
	relationship tinyint not null default 1,
	costs_center_id uniqueidentifier null default null,
	tenant_id uniqueidentifier not null,
	constraint enterprise_pk primary key (id),
	constraint enterprise_legal_name_un unique (tenant_id, legal_name),
	constraint enterprise_tax_id_un unique (tenant_id, tax_id)
);

-- Organization.EnterpriseAddress
CREATE TABLE organization.enterprise_address (
	id uniqueidentifier ROWGUIDCOL NOT NULL default newsequentialid(),
	row_version timestamp not null,
	line1 nvarchar(70) not null,
	line2 nvarchar(150) not null default '',
	commune_id uniqueidentifier not null,
	enterprise_id uniqueidentifier NOT NULL,
	constraint enterprise_address_pk primary key (id),
	constraint enterprise_address_un unique (line1, commune_id, enterprise_id)
);

-- Organization.TenantAddress
CREATE TABLE organization.tenant_address (
	id uniqueidentifier ROWGUIDCOL NOT NULL default newsequentialid(),
	row_version TIMESTAMP NOT NULL,
	line1 nvarchar(70) not null,
	line2 nvarchar(150) not null default '',
	commune_id uniqueidentifier not null,
	tenant_id uniqueidentifier NOT NULL,
	constraint tenant_address_pk primary key (id),
	constraint tenant_address_un unique (line1, commune_id, tenant_id)
);

-- Organization.TenantAddressType
create table organization.tenant_address_type (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	tenant_address_id uniqueidentifier not null,
	address_type_id uniqueidentifier not null,
	constraint tenant_address_type_pk primary key (id),
	constraint tenant_address_type_un unique (tenant_address_id, address_type_id)
);

-- Organization.EnterpriseAddressType
create table organization.enterprise_address_type (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	enterprise_address_id uniqueidentifier not null,
	address_type_id uniqueidentifier not null,
	constraint enterprise_address_type_pk primary key (id),
	constraint enterprise_address_type_un unique (enterprise_address_id, address_type_id)
);

-- Organization.Product
CREATE TABLE organization.product (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(100) NOT NULL,
	description nvarchar(140) NOT NULL default '',
	unit_price DECIMAL(13, 4) NOT NULL,
	is_discontinued BIT NOT NULL default 0,
	tenant_id uniqueidentifier NOT NULL, -- Owner of the product.
	constraint product_pk primary key (id),
	constraint product_un unique (name, tenant_id) -- Unique per enterprise
);

-- Organization.Service
CREATE TABLE organization.[service] (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(100) NOT NULL,
	description nvarchar(140) NOT NULL default '',
	unit_price DECIMAL(13, 4) NOT NULL,
	is_discontinued BIT NOT NULL default 0,
	tenant_id uniqueidentifier NOT NULL, -- Owner of the service
	constraint service_pk primary key (id),
	constraint service_un unique (name, tenant_id)
);

-- Organization.CostsCenter
create table organization.costs_center (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	code varchar(24) not null,
	name nvarchar(100) NOT NULL,
	[description] nvarchar(max) not null default '',
	tenant_id uniqueidentifier not null,
	constraint costs_center_pk primary key (id),
	constraint costs_center_un unique(code, name, tenant_id)
);

-- Pending update
--alter table organization.costs_center drop constraint costs_center_un;
--alter table organization.costs_center add constraint costs_center_un unique (code, name, tenant_id);

----------------------------------------- Taxation
-- Taxation.EconomicActivity
-- Type: global
CREATE TABLE taxation.economic_activity (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(110) NOT NULL,
	code varchar(6) NOT NULL,
	is_subject_to_tax BIT NULL default NULL,
	tributary_category TINYINT NULL,
	economic_activity_group_id uniqueidentifier NULL,
	constraint economic_activity_pk primary key (id),
	constraint economic_activity_un unique (name, code, economic_activity_group_id)
);

-- Taxation.ComercialActivity (Giro)
create table taxation.comercial_activity (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(200) not null,
	constraint comercial_activity_pk primary key (id),
	constraint comercial_activity_un unique (name)
);

-- Taxation.SpecificTax
-- Type: global
CREATE TABLE taxation.specific_tax (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(100) NOT NULL,
	code INT NOT NULL,
	is_retained BIT NOT NULL default 0,
	tax_rate DECIMAL(4, 3) NOT NULL,
	constraint specific_tax_pk primary key (id),
	constraint specific_tax_un unique (name, code)
);

-- Taxation.EnterpriseEconomicActivity
CREATE TABLE taxation.enterprise_economic_activity (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	enterprise_id uniqueidentifier NOT NULL,
	economic_activity_id uniqueidentifier NOT NULL,
	row_version TIMESTAMP NOT NULL,
	constraint enterprise_economic_activity_pk primary key (id),
	constraint enterprise_economic_activity_un unique (enterprise_id, economic_activity_id)
);

-- Taxation.TenantEconomicActivity
create table taxation.tenant_economic_activity (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	tenant_id uniqueidentifier NOT NULL,
	economic_activity_id uniqueidentifier NOT NULL,
	row_version TIMESTAMP NOT NULL,
	constraint tenant_economic_activity_pk primary key (id),
	constraint tenant_economic_activity_un unique (tenant_id, economic_activity_id)
);

-- Taxation.TenantComercialActivity
create table taxation.tenant_comercial_activity (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version timestamp not null,
	comercial_activity_id uniqueidentifier not null,
	tenant_id uniqueidentifier not null,
	constraint tenant_comercial_activity_pk primary key (id),
	constraint tenant_comercial_activity_un unique (comercial_activity_id, tenant_id)
);

-- Taxation.EnterpriseComercialActivity
create table taxation.enterprise_comercial_activity (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version timestamp not null,
	comercial_activity_id uniqueidentifier not null,
	enterprise_id uniqueidentifier not null,
	constraint enterprise_comercial_activity_pk primary key (id),
	constraint enterprise_comercial_activity_un unique (comercial_activity_id, enterprise_id)
);

-- Taxation.ReceivedTaxDocument
create table taxation.received_tax_document (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	document_type varchar(3) NOT NULL,
	folio_number BIGINT NOT NULL,
	accounting_date DATE NOT NULL,
	expiry_date date null default null,
	comments nvarchar(200) NOT NULL default '',
	file_path nvarchar(MAX) NOT NULL,
	tax_rate DECIMAL(4, 3) NOT NULL,
	reception_date datetime null,
	extra_tax_code SMALLINT NULL default NULL,
	extra_tax_rate DECIMAL(4, 3) NOT NULL default 0.000,
	reception_status tinyint not null default 0,
	net_amount decimal(13, 4) not null default 0.0000,
	exempt_amount decimal(13, 4) not null default 0.0000,
	retained_tax_amount decimal(13, 4) not null default 0.0000,
	tax_amount decimal(13, 4) not null default 0.0000,
	total decimal(13, 4) not null default 0.0000,
	prvdr_tax_id varchar(20) NOT NULL,
	prvdr_legal_name nvarchar(100) NOT NULL,
	prvdr_phone varchar(50) NOT NULL,
	prvdr_bank_account_number varchar(15) not null default '',
	prvdr_comercial_activity nvarchar(60) not null,
	prvdr_economic_activity varchar(6) null,
	prvdr_address_line1 nvarchar(70) not null,
	prvdr_address_line2 nvarchar(150) not null default '',
	prvdr_address_commune nvarchar(20) not null,
	prvdr_address_city nvarchar(20) not null,
	rcptr_tax_id varchar(20) not null,
	rcptr_legal_name varchar(100) not null,
	rcptr_comercial_activity_name varchar(200) not null,
	rcptr_address varchar(400) not null,
	rcptr_commune varchar(50) not null,
	tenant_id uniqueidentifier NOT NULL,
	enterprise_id uniqueidentifier null,
	accounting_voucher_id uniqueidentifier null default null,
	constraint received_tax_document_pk primary key (id),
	constraint received_tax_document_un unique (document_type, folio_number, prvdr_tax_id, tenant_id)
);

create table taxation.received_tax_document_line (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	line_number SMALLINT NOT NULL,
	[text] nvarchar(80) NOT NULL,
	unit_price DECIMAL(13, 4) NOT NULL default 0.0000,
	quantity DECIMAL(13, 4) NOT NULL default 1.0000,
	tax_amount DECIMAL(13, 4) NOT NULL default 0.0000,
	extra_tax_amount DECIMAL(13, 4) NOT NULL default 0.0000,
	line_total DECIMAL(13, 4) NOT NULL default 1.0000,
	additional_gloss nvarchar(1000) NOT NULL default '',
	measure_unit nvarchar(30) NOT NULL default '',
	tax_type tinyint not null default 0,
	received_tax_document_id uniqueidentifier NOT NULL,
	constraint received_tax_document_line_pk primary key (id),
	constraint received_tax_document_line_un unique (line_number, received_tax_document_id)
);

create table taxation.received_taxdoc_reference (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	parent_id uniqueidentifier NOT NULL,
	reference_id uniqueidentifier NOT NULL,
	reference_reason varchar(15) NOT NULL,
	reference_comment nvarchar(90) NOT NULL,
	reference_date date null,
	constraint received_taxdoc_reference_pk primary key (id),
	constraint received_taxdoc_reference_un unique (parent_id, reference_id)
);

-- Taxation.TaxDocument
CREATE TABLE taxation.tax_document (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	document_type varchar(3) NOT NULL,
	folio_number BIGINT NOT NULL,
	document_number BIGINT NOT NULL IDENTITY(1, 1),
	emission_point varchar(10) NOT NULL,
	accounting_date DATE NOT NULL,
	accounting_time TIME NULL default NULL,
	expire_date DATE NULL default NULL,
	previous_debt_balance DECIMAL(13, 4) NOT NULL default 0.0000,
	comments nvarchar(200) NOT NULL default '',
	document_status varchar(30) NOT NULL default 'Saved',
	tax_rate DECIMAL(4, 3) NOT NULL,
	document_responsible varchar(100) NULL,
	default_payment_type_id uniqueidentifier NULL,
	dispatch_type_id uniqueidentifier NULL,
	reduction_type_id uniqueidentifier NULL,
	generation_date datetime null,
	is_retained_extra_tax BIT NOT NULL default 0,
	extra_tax_code SMALLINT NULL default NULL,
	extra_tax_rate DECIMAL(4, 3) NOT NULL default 0.000,
	exempt_total decimal (13, 4) not null default 0.000,
	affect_total decimal (13, 4) not null default 0.000,
	total decimal (13, 4) not null default 0.0000,
	costs_center_id uniqueidentifier null,
	tenant_tax_id varchar(20) not null,
	tenant_legal_name nvarchar(100) not null,
	tenant_comercial_activity_name nvarchar(60) not null,
	tenant_address nvarchar(60) not null,
	tenant_commune nvarchar(20) not null,
	tenant_city nvarchar(20),
	tenant_economic_activity_code varchar(6),
	tenant_economic_activity_name nvarchar(110),
	tenant_id uniqueidentifier NOT NULL,
	enterprise_id uniqueidentifier null default null,
	constraint tax_document_pk primary key (id),
	constraint tax_document_un unique (document_type, folio_number, tenant_id)
);

create table taxation.tax_document_weak_reference (
	id uniqueidentifier rowguidcol not null,
	row_version timestamp not null,
	document_type varchar(6) not null,
	folio bigint not null,
	[date] date not null,
	reason nvarchar(90) not null default '',
	tax_document_id uniqueidentifier not null,
	constraint tax_document_weak_reference_pk primary key (id),
	constraint tax_document_weak_reference_un unique (document_type, folio, tax_document_id)
);

create table taxation.taxdoc_client (
	id uniqueidentifier rowguidcol not null,
	row_version timestamp not null,
	tax_id varchar(20) NOT NULL,
	legal_name nvarchar(100) NOT NULL,
	phone varchar(50) NOT NULL,
	bank_account_number varchar(15) not null default '',
	comercial_activity nvarchar(40) not null,
	constraint taxdoc_client_pk primary key (id)
);

create table taxation.taxdoc_client_address (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	line1 nvarchar(70) not null,
	line2 nvarchar(150) not null default '',
	commune nvarchar(20) not null,
	city nvarchar(15) not null,
	taxdoc_client_id uniqueidentifier not null,
	constraint taxdoc_client_address_pk primary key (id),
	constraint taxdoc_client_address_un unique (line1, commune, taxdoc_client_id)
);

create table taxation.taxdoc_client_address_type (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	address_type_id uniqueidentifier not null,
	taxdoc_client_address_id uniqueidentifier not null,
	constraint taxdoc_client_address_type_pk primary key (id),
	constraint taxdoc_client_address_type_un unique (address_type_id, taxdoc_client_address_id)
);

-- Taxation.TaxDocumentDetail
CREATE TABLE taxation.tax_document_line (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	tax_document_id uniqueidentifier NOT NULL,
	row_version TIMESTAMP NOT NULL,
	line_number SMALLINT NOT NULL,
	[text] nvarchar(80) NOT NULL,
	unit_price DECIMAL(13, 4) NOT NULL default 0.0000,
	quantity DECIMAL(13, 4) NOT NULL default 1.0000,
	tax_amount DECIMAL(13, 4) NOT NULL default 0.0000,
	extra_tax_amount DECIMAL(13, 4) NOT NULL default 0.0000,
	line_total DECIMAL(13, 4) NOT NULL default 1.0000,
	additional_gloss nvarchar(1000) NOT NULL default '',
	measure_unit nvarchar(30) NOT NULL default '',
	tax_type tinyint not null default 0,
	constraint tax_document_line_pk primary key (id),
	constraint tax_document_line_un unique (line_number, tax_document_id)
);

-- Taxation.TaxDocumentReference
CREATE TABLE taxation.tax_document_reference (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	parent_id uniqueidentifier NOT NULL,
	reference_id uniqueidentifier NOT NULL,
	row_version TIMESTAMP NOT NULL,
	reference_reason varchar(15) NOT NULL,
	reference_comment nvarchar(90) NOT NULL,
	[date] date not null default getdate(),
	constraint tax_document_reference_pk primary key (id),
	constraint tax_document_reference_un unique (parent_id, reference_id)
);

-- Taxation.ReductionType
CREATE TABLE taxation.reduction_type (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	code TINYINT NOT NULL,
	[description] nvarchar(70) NOT NULL,
	constraint reduction_type_pk primary key (id),
	constraint reduction_type_code_un unique (code)
);

-- Taxation.DocumentType
CREATE TABLE taxation.document_type (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	code varchar(6) NOT NULL,
	name nvarchar(70) NOT NULL,
	tax_rate DECIMAL(4,3) NOT NULL,
	constraint document_type_pk primary key (id),
	constraint document_type_code_un unique (code, name)
);

-- Taxation.DispatchType
CREATE TABLE taxation.dispatch_type (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	code nvarchar(10) NOT NULL,
	[description] nvarchar(70) NOT NULL,
	constraint dispatch_type_pk primary key (id),
	constraint dispatch_type_un unique (code)
);

-- Taxation.PaymentType
CREATE TABLE taxation.payment_type (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	code nvarchar(6) NOT NULL,
	[description] nvarchar(70) NOT NULL,
	constraint payment_type_pk primary key (id),
	constraint payment_type_code_un unique (code)
);

-- Taxation.Payment
CREATE TABLE taxation.payment (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	amount DECIMAL(13, 4) NOT NULL,
	expire_date DATETIME NOT NULL,
	payment_type_id uniqueidentifier NOT NULL,
	tax_document_id uniqueidentifier NOT NULL,
	constraint payment_pk primary key (id)
);

-- Taxation.ServiceType
CREATE TABLE taxation.service_type (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	code INT NOT NULL,
	[description] nvarchar(70) NOT NULL,
	constraint service_type_pk primary key (id),
	constraint service_type_un unique (code)
);

-- Taxation.FolioRange
CREATE TABLE taxation.folio_range (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version TIMESTAMP not null,
	document_type varchar(3) not null,
	range_start bigint not null,
	range_end bigint not null,
	is_current_range bit not null default 0,
	current_folio bigint not null,
	unused_folios varchar(MAX) not null default '',
	created_date datetime not null,
	request_date date not null,
	[status] tinyint not null default 0,
	tenant_id uniqueidentifier not null,
	constraint folio_range_pk primary key (id),
	constraint folio_range_un unique (range_start, document_type, tenant_id)
);

----------------------------------------- Accounting
create table accounting.accounting_voucher (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	number int not null identity(1, 1),
	[date] date not null,
	[description] nvarchar(60) null default null,
	[status] tinyint not null default 0,
	tenant_id uniqueidentifier not null,
	constraint account_voucher_pk primary key (id),
	constraint account_voucher_un unique ([date], number, tenant_id)
);

create table accounting.accounting_voucher_line (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	number int not null,
	provider_tax_id nvarchar(20) not null default '',
	document_type varchar(6) not null,
	folio_number bigint not null,
	[description] nvarchar(60) not null default '',
	emission_date date not null,
	[expiry_date] date not null,
	referenced_document_type varchar(6) null default null,
	referenced_document_folio bigint null default null,
	debit decimal (13, 4) not null default 0.0000,
	asset decimal (13, 4) not null default 0.0000,
	net_amount decimal (13, 4) not null default 0.0000,
	net_fixed_asset decimal (13, 4) not null default 0.0000,
	total_exempt_amount decimal (13, 4) not null default 0.0000,
	total_tax_amount decimal(13, 4) not null default 0.0000,
	total_unrecoverable_amount decimal(13, 4) not null default 0.0000,
	total decimal (13, 4) not null default 0.0000,
	custom_field_1 nvarchar(500) not null default '',
	custom_field_2 nvarchar(500) not null default '',
	custom_field_3 nvarchar(500) not null default '',
	costs_center_id uniqueidentifier not null,
	accounting_account_id uniqueidentifier not null,
	accounting_voucher_id uniqueidentifier not null,
	enterprise_id uniqueidentifier null default null,
	constraint accounting_voucher_line_pk primary key (id),
	constraint accounting_voucher_line_un unique (accounting_voucher_id, number)
);

create table accounting.accounting_account (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	code varchar(18) not null,
	[description] nvarchar(100) not null default '',
	tenant_id uniqueidentifier not null,
	constraint accounting_account_pk primary key (id),
	constraint accounting_account_un unique (code, tenant_id)
);

--create table accounting.accounting_voucher_line_property (
--	id uniqueidentifier rowguidcol not null default newsequentialid(),
--	row_version timestamp not null,
--	accounting_voucher_line_id uniqueidentifier not null,
--	property_id uniqueidentifier not null,
--	content nvarchar(max) not null,
--	constraint accounting_voucher_line_property_pk primary key (id),
--	constraint accounting_voucher_line_property_un unique (accounting_voucher_line_id, property_id)
--);

----------------------------------------- LOCATION

-- Location.Address
create table [location].[address] (
	id uniqueidentifier rowguidcol not null default newsequentialid(),
	row_version timestamp not null,
	line1 nvarchar(70) not null,
	line2 nvarchar(150) not null default '',
	commune_id uniqueidentifier not null,
	constraint address_pk primary key (id),
	constraint address_un unique (line1, commune_id)
);

-- Location.Commune
-- Global
CREATE TABLE location.commune (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(20) NOT NULL,
	post_code varchar(7) NOT NULL default '',
	city_id uniqueidentifier NOT NULL,
	constraint commune_pk primary key (id),
	constraint commune_un unique (name, city_id)
);

-- Location.City
-- Global
CREATE TABLE location.city (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(100) NOT NULL,
	country_id uniqueidentifier NOT NULL,
	constraint city_pk primary key (id),
	constraint city_un unique (name, country_id)
);

-- Location.Country
-- Global
CREATE TABLE location.country (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	name nvarchar(100) NOT NULL,
	code varchar(2) NOT NULL default '',
	phone_code varchar(5) NOT NULL default '',
	constraint country_pk primary key (id),
	constraint country_un unique (name)
);

-- I18n.Locale
-- Global
CREATE TABLE i18n.locale (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	row_version TIMESTAMP NOT NULL,
	code varchar(6) NOT NULL,
	full_name varchar(50) NOT NULL default '',
	constraint locale_pk primary key (id),
	constraint locale_un unique (code)
);

-- I18n.CountryLocale
-- Global
CREATE TABLE i18n.country_locale (
	id uniqueidentifier ROWGUIDCOL NOT NULL default NEWSEQUENTIALID(),
	country_id uniqueidentifier NOT NULL,
	locale_id uniqueidentifier NOT NULL,
	row_version TIMESTAMP NOT NULL,
	constraint country_locale_pk primary key (id),
	constraint country_locale_un unique (country_id, locale_id)
);

----------------------------------- Customization
--create table customization.property (
--	id uniqueidentifier rowguidcol not null default newsequentialid(),
--	name varchar(100) not null,
--	[type] varchar(50) not null,
--	[description] varchar(500) not null default '',
--	tenant_id uniqueidentifier not null,
--	constraint property_pk primary key (id),
--	constraint property_un unique(name, tenant_id)
--);