--use zentom_mssql;

-- indexes
create nonclustered index role_permission_role_idx on [system].role_permission (role_id);
create nonclustered index role_permission_permission_idx on [system].role_permission (permission_id);

create nonclustered index account_role_account_idx on [system].account_role (authorized_account_id);
create nonclustered index account_role_role_idx on [system].account_role (role_id);

create nonclustered index taxdoc_reference_parent_idx on taxation.tax_document_reference (parent_id);
create nonclustered index taxdoc_reference_reference_idx on taxation.tax_document_reference (reference_id);

create nonclustered index taxdoc_line_taxdoc_idx on taxation.tax_document_line (tax_document_id);

create nonclustered index country_locale_country_idx on i18n.country_locale (country_id);
create nonclustered index country_locale_locale_idx on i18n.country_locale (locale_id);

create nonclustered index tenant_idx on organization.tenant (tax_id);

-- foreign keys
----------------------------------------- Organization
------------------- Enterprise
alter table organization.enterprise
add constraint enterprise_tenant_fk foreign key (tenant_id) references organization.tenant (id),
	constraint enterprise_costs_center_fk foreign key (costs_center_id) references organization.costs_center (id) on delete set null;

------------------- Employee
alter table organization.employee
add constraint employee_employer_fk foreign key (tenant_id) references organization.tenant (id),
	constraint employee_person_fk foreign key (person_id) references [system].person (id);

------------------- Product
alter table organization.product
add constraint product_tenant_fk foreign key (tenant_id) references organization.tenant (id);

------------------- Service
alter table organization.[service]
add constraint service_tenant_fk foreign key (tenant_id) references organization.tenant (id);

------------------- Enterprise Address
alter table organization.enterprise_address
add constraint enterprise_address_enterprise_fk foreign key (enterprise_id) references organization.enterprise (id) on delete cascade;

------------------- Tenant Address
alter table organization.tenant_address
add constraint tenant_address_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade;

------------------- Enterprise Address Type
alter table organization.enterprise_address_type
add constraint ent_addr_type_ent_addr_fk foreign key (enterprise_address_id) references organization.enterprise_address (id) on delete cascade on update cascade,
	constraint ent_addr_type_addr_fk foreign key (address_type_id) references organization.address_type (id) on delete cascade on update cascade;

------------------- Tenant Address Type
alter table organization.tenant_address_type
add constraint tnt_addr_type_tnt_addr_fk foreign key (tenant_address_id) references organization.tenant_address (id) on delete cascade on update cascade,
	constraint tnt_addr_type_addr_fk foreign key (address_type_id) references organization.address_type (id) on delete cascade on update cascade;

------------------- Folio Range
alter table taxation.folio_range
add constraint taxdoc_folio_range_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade;

------------------- Costs Center
alter table organization.costs_center
add constraint costs_center_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete no action;

----------------------------------------- Taxation
------------------- Received Tax Document
alter table taxation.received_tax_document
add constraint received_tax_document_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade,
	constraint received_tax_document_enterprise_fk foreign key (enterprise_id) references organization.enterprise (id) on delete set null,
	constraint recieved_tax_document_accounting_voucher_fk foreign key (accounting_voucher_id) references accounting.accounting_voucher (id) on delete set null;

------------------- Received Tax Document Line
alter table taxation.received_tax_document_line
add constraint received_taxdoc_line_taxdoc_fk foreign key (received_tax_document_id) references taxation.received_tax_document (id) on delete cascade;

------------------- Received Tax Document Reference
alter table taxation.received_taxdoc_reference
add constraint received_taxdoc_reference_parent_fk foreign key (parent_id) references taxation.received_tax_document (id),
	constraint received_taxdoc_reference_reference_fk foreign key (reference_id) references taxation.received_tax_document (id);

------------------- Tax Document
alter table taxation.tax_document
add constraint tax_document_payment_type_fk foreign key (default_payment_type_id) references taxation.payment_type (id) on delete set null,
	constraint tax_document_dispatch_type_fk foreign key (dispatch_type_id) references taxation.dispatch_type (id) on delete set null,
	constraint tax_document_reduction_type_fk foreign key (reduction_type_id) references taxation.reduction_type (id) on delete set null,
	constraint tax_document_costs_center_fk foreign key (costs_center_id) references organization.costs_center(id) on delete no action,
	constraint tax_document_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete no action,
	constraint tax_document_enterprise_fk foreign key (enterprise_id) references organization.enterprise (id) on delete set null;

------------------- Tax Document Reference
alter table taxation.tax_document_reference
add constraint parent_tax_document_fk foreign key (parent_id) references taxation.tax_document (id) on delete no action,
	constraint referenced_tax_document_fk foreign key (reference_id) references taxation.tax_document (id) on delete no action;

------------------- Tax Document Weak Reference
alter table taxation.tax_document_weak_reference
add constraint taxdoc_weak_ref_tax_document_fk foreign key (tax_document_id) references taxation.tax_document (id) on delete cascade;

------------------- Tax Document Line
alter table taxation.tax_document_line
add constraint taxdoc_line_taxdoc_fk foreign key (tax_document_id) references taxation.tax_document (id) on delete cascade;

------------------- Tax Document Client
alter table taxation.taxdoc_client
add constraint taxdoc_client_taxdoc_fk foreign key (id) references taxation.tax_document (id) on delete cascade;

------------------- Tax Document Client Address
alter table taxation.taxdoc_client_address
add constraint taxdoc_client_address_fk foreign key (taxdoc_client_id) references taxation.taxdoc_client (id) on delete cascade;

------------------- Tax Document Client Address Type
alter table taxation.taxdoc_client_address_type
add constraint txdoc_cliat_taxdoc_cli_addr_fk foreign key (taxdoc_client_address_id) references taxation.taxdoc_client_address (id) on delete cascade,
	constraint taxdoc_cliat_address_type_fk foreign key (address_type_id) references organization.address_type (id) on delete cascade;

------------------- Payment
alter table taxation.payment
add constraint payment_tax_document_fk foreign key (tax_document_id) references taxation.tax_document (id) on delete cascade,
	constraint payment_payment_type_fk foreign key (payment_type_id) references taxation.payment_type (id) on delete no action;

------------------- Enterprise Economic Activity
alter table taxation.enterprise_economic_activity
add constraint ent_eco_act_enterprise_fk foreign key (enterprise_id) references organization.enterprise (id) on delete cascade,
	constraint ent_eco_act_economic_activity_fk foreign key (economic_activity_id) references taxation.economic_activity (id) on delete cascade;

------------------- Enterprise Comercial Activity
alter table taxation.enterprise_comercial_activity
add constraint ent_com_act_enterprise_fk foreign key (enterprise_id) references organization.enterprise (id) on delete cascade,
	constraint ent_com_act_comercial_activity_fk foreign key (comercial_activity_id) references taxation.comercial_activity (id) on delete cascade;

------------------- Tenant Economic Activity
alter table taxation.tenant_economic_activity
add constraint ten_eco_act_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade,
	constraint ten_eco_act_economic_activity_fk foreign key (economic_activity_id) references taxation.economic_activity (id) on delete cascade;

------------------- Tenant Comercial Activity
alter table taxation.tenant_comercial_activity
add constraint ten_com_act_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade,
	constraint ten_com_act_comercial_activity_fk foreign key (comercial_activity_id) references taxation.comercial_activity (id) on delete cascade;

----------------------------------------- Location
------------------- City
alter table location.city
add constraint city_country_fk foreign key (country_id) references location.country (id) on delete cascade;

------------------- Commune
alter table location.commune
add constraint commune_city_fk foreign key (city_id) references location.city (id) on delete cascade;

alter table location.address
add constraint address_commune_fk foreign key (commune_id) references location.commune (id) on delete cascade;

----------------------------------------- I18n
------------------- Country Locale
alter table i18n.country_locale
add constraint country_locale_country_fk foreign key (country_id) references location.country (id) on delete cascade,
	constraint country_locale_locale_fk foreign key (locale_id) references i18n.locale (id) on delete cascade;

----------------------------------------- System
------------------- Role Permission
alter table [system].role_permission
add constraint role_permission_role_fk foreign key (role_id) references [system].[role] (id) on delete cascade,
	constraint role_permission_permission_fk foreign key (permission_id) references [system].[permission] (id) on delete cascade;

------------------- Account Role
alter table [system].account_role
add constraint account_role_role_fk foreign key (role_id) references [system].[role] (id) on delete cascade,
	constraint account_role_account_fk foreign key (authorized_account_id) references [system].authorized_account (id) on delete cascade;

------------------- Account
alter table [system].account
add constraint account_person_fk foreign key (id) references [system].person(id) on delete cascade;

------------------- Authorized Account
alter table system.authorized_account
add constraint authorized_account_tenant_fk foreign key (tenant_id) references organization.tenant (id),
	constraint authorized_account_account_fk foreign key (account_id) references [system].account (id);

------------------- Password Request
alter table [system].password_request
add constraint password_request_account_fk foreign key (account_id) references [system].account (id) on delete cascade;

------------------- Tax Document Template
alter table [system].tax_document_template
add constraint tax_document_template_account_fk foreign key (account_id) references [system].account (id) on delete cascade,
	constraint tax_document_template_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade;

------------------- Role
alter table [system].[role]
add constraint role_tenant_fk foreign key (tenant_id) references organization.tenant (id);

------------------- Tenant Settings
alter table [system].tenant_settings
add constraint tenant_settings_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade;

------------------- Tenant Modules
alter table system.tenant_module
add constraint tenant_module_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade,
	constraint tenant_module_module_fk foreign key (module_id) references system.module (id) on delete cascade;

----------------------------------------- Accounting
------------------- Accounting Account
alter table accounting.accounting_account
add constraint accounting_account_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade;

------------------- Accounting Voucher
alter table accounting.accounting_voucher
add constraint accounting_voucher_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade;

------------------- Accounting Voucher Line
alter table accounting.accounting_voucher_line
add constraint accounting_voucher_line_accounting_voucher_fk foreign key (accounting_voucher_id) references accounting.accounting_voucher (id) on delete cascade,
	constraint accounting_voucher_line_provider_fk foreign key (enterprise_id) references organization.enterprise (id) on delete set null;

------------------------------------------- Customization
--------------------- Property
--alter table customization.property
--add constraint property_tenant_fk foreign key (tenant_id) references organization.tenant (id) on delete cascade;