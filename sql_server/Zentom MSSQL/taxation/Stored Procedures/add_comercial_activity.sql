﻿
create procedure taxation.add_comercial_activity
(
	@name nvarchar(200),
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.comercial_activity where name = @name);

	if (@id is null)
	begin
		insert into taxation.comercial_activity (name) values (@name);
		set @id = (select id from taxation.comercial_activity where name = @name);
	end;
end;
