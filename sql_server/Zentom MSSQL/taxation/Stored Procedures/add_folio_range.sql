﻿
create procedure taxation.add_folio_range
(
	@start bigint,
	@end bigint,
	@doctype nvarchar(6),
	@is_current_range bit = 0,
	@request_date date,
	@tenant_id uniqueidentifier,
	@status tinyint = 1,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.folio_range where tenant_id = @tenant_id and document_type = @doctype and range_start = @start);

	if (@id is null) 
	begin
		insert into taxation.folio_range (tenant_id, range_start, range_end, document_type, request_date, created_date, unused_folios, [status], is_current_range, current_folio) values
		(@tenant_id, @start, @end, @doctype, @request_date, getdate(), '', @status, @is_current_range, @start);
		set @id = (select id from taxation.folio_range where tenant_id = @tenant_id and document_type = @doctype and range_start = @start);
	end;
end;
