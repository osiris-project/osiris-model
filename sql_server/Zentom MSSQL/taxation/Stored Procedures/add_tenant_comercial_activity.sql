﻿
create procedure taxation.add_tenant_comercial_activity
(
	@name nvarchar(200),
	@tenant_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	declare @comercial_activity uniqueidentifier = (select id from taxation.comercial_activity where name = @name);

	if (@comercial_activity is not null)
	begin
		set @id = (select id from taxation.tenant_comercial_activity where tenant_id = @tenant_id and comercial_activity_id = @comercial_activity);

		if(@id is null)
		begin
			insert into taxation.tenant_comercial_activity (tenant_id, comercial_activity_id) values (@tenant_id, @comercial_activity);
			set @id = (select id from taxation.tenant_comercial_activity where tenant_id = @tenant_id and comercial_activity_id = @comercial_activity);
		end;
	end;
	else
	begin
		print N'There is not economic activity with name ' + @name;
	end;
end;
