﻿
create procedure taxation.set_tenant_comercial_activity
(
	@tenant_id uniqueidentifier,
	@comercial_activity_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.tenant_comercial_activity where tenant_id = @tenant_id and comercial_activity_id = @comercial_activity_id);

	if (@id is null)
	begin
		insert into taxation.tenant_comercial_activity (tenant_id, comercial_activity_id) values (@tenant_id, @comercial_activity_id);
		set @id = (select id from taxation.tenant_comercial_activity where tenant_id = @tenant_id and comercial_activity_id = @comercial_activity_id);
	end;
end;
