﻿
create procedure taxation.add_tenant_economic_activity
(
	@code varchar(6),
	@tenant_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	declare @economic_activity_id uniqueidentifier = (select id from taxation.economic_activity where code = @code);

	if (@economic_activity_id is not null)
	begin
		set @id = (select id from taxation.tenant_economic_activity where tenant_id = @tenant_id and economic_activity_id = @economic_activity_id);

		if (@id is null)
		begin
			insert into taxation.tenant_economic_activity (tenant_id, economic_activity_id) values (@tenant_id, @economic_activity_id);
			set @id = (select id from taxation.tenant_economic_activity where tenant_id = @tenant_id and economic_activity_id = @economic_activity_id);
		end;
	end;
	else
	begin
		print N'There is not economic activity with code ' + @code;
	end;
end;
