﻿
create procedure taxation.add_enterprise_economic_activity
(
	@code varchar(6),
	@enterprise_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	declare @economic_activity_id uniqueidentifier = (select id from taxation.economic_activity where code = @code);

	if (@economic_activity_id is not null)
	begin
		set @id = (select id from taxation.enterprise_economic_activity where enterprise_id = @enterprise_id and economic_activity_id = @economic_activity_id);

		if (@id is null)
		begin
			insert into taxation.enterprise_economic_activity (enterprise_id, economic_activity_id) values (@enterprise_id, @economic_activity_id);
			set @id = (select id from taxation.enterprise_economic_activity where enterprise_id = @enterprise_id and economic_activity_id = @economic_activity_id);
		end;
	end;
	else
	begin
		print N'There is not economic activity with code ' + @code;
	end;
end;
