﻿
create procedure taxation.set_tenant_economic_activity
(
	@tenant_id uniqueidentifier,
	@economic_activity_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.tenant_economic_activity where tenant_id = @tenant_id and economic_activity_id = @economic_activity_id);

	if (@id is null)
	begin
		insert into taxation.tenant_economic_activity (tenant_id, economic_activity_id) values (@tenant_id, @economic_activity_id);
		set @id = (select id from taxation.tenant_economic_activity where tenant_id = @tenant_id and economic_activity_id = @economic_activity_id);
	end;
end;
