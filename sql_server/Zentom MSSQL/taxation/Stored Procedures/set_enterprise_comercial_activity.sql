﻿
create procedure taxation.set_enterprise_comercial_activity
(
	@enterprise_id uniqueidentifier,
	@comercial_activity_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.enterprise_comercial_activity where enterprise_id = @enterprise_id and comercial_activity_id = @comercial_activity_id);

	if (@id is null)
	begin
		insert into taxation.enterprise_comercial_activity (enterprise_id, comercial_activity_id) values (@enterprise_id, @comercial_activity_id);
		set @id = (select id from taxation.enterprise_comercial_activity where enterprise_id = @enterprise_id and comercial_activity_id = @comercial_activity_id);
	end;
end;
