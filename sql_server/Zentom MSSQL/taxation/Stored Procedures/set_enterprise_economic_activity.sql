﻿
create procedure taxation.set_enterprise_economic_activity
(
	@enterprise_id uniqueidentifier,
	@economic_activity_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.enterprise_economic_activity where enterprise_id = @enterprise_id and economic_activity_id = @economic_activity_id);

	if (@id is null)
	begin
		insert into taxation.enterprise_economic_activity (enterprise_id, economic_activity_id) values (@enterprise_id, @economic_activity_id);
		set @id = (select id from taxation.enterprise_economic_activity where enterprise_id = @enterprise_id and economic_activity_id = @economic_activity_id);
	end;
end;
