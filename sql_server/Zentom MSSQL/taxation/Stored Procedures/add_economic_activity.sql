﻿
create procedure taxation.add_economic_activity
(
	@name nvarchar(110),
	@code varchar(6),
	@is_subject_to_tax BIT = 1,
	@tributary_category TINYINT NULL,
	@economic_activity_group_id uniqueidentifier NULL,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from taxation.economic_activity where name = @name and code = @code);

	if (@id is null)
	begin
		insert into taxation.economic_activity (name, code, is_subject_to_tax, tributary_category, economic_activity_group_id)
		values (@name, @code, @is_subject_to_tax, @tributary_category, @economic_activity_group_id);
		set @id = (select id from taxation.economic_activity where name = @name and code = @code);
	end;
end;
