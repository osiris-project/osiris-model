﻿CREATE TABLE [taxation].[service_type] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [code]        INT              NOT NULL,
    [description] NVARCHAR (70)    NOT NULL,
    CONSTRAINT [service_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [service_type_un] UNIQUE NONCLUSTERED ([code] ASC)
);

