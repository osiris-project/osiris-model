﻿CREATE TABLE [taxation].[tax_document_weak_reference] (
    [id]              UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [row_version]     ROWVERSION       NOT NULL,
    [document_type]   VARCHAR (6)      NOT NULL,
    [folio]           BIGINT           NOT NULL,
    [date]            DATE             NOT NULL,
    [reason]          NVARCHAR (90)    DEFAULT ('') NOT NULL,
    [tax_document_id] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [tax_document_weak_reference_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [taxdoc_weak_ref_tax_document_fk] FOREIGN KEY ([tax_document_id]) REFERENCES [taxation].[tax_document] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tax_document_weak_reference_un] UNIQUE NONCLUSTERED ([document_type] ASC, [folio] ASC, [tax_document_id] ASC)
);

