﻿CREATE TABLE [taxation].[taxdoc_client] (
    [id]                  UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [row_version]         ROWVERSION       NOT NULL,
    [tax_id]              VARCHAR (20)     NOT NULL,
    [legal_name]          NVARCHAR (100)   NOT NULL,
    [phone]               VARCHAR (50)     NOT NULL,
    [bank_account_number] VARCHAR (15)     DEFAULT ('') NOT NULL,
    [comercial_activity]  NVARCHAR (40)    NOT NULL,
    CONSTRAINT [taxdoc_client_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [taxdoc_client_taxdoc_fk] FOREIGN KEY ([id]) REFERENCES [taxation].[tax_document] ([id]) ON DELETE CASCADE
);

