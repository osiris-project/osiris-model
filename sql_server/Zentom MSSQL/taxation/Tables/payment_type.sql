﻿CREATE TABLE [taxation].[payment_type] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [code]        NVARCHAR (6)     NOT NULL,
    [description] NVARCHAR (70)    NOT NULL,
    CONSTRAINT [payment_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [payment_type_code_un] UNIQUE NONCLUSTERED ([code] ASC)
);

