﻿CREATE TABLE [taxation].[enterprise_comercial_activity] (
    [id]                    UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]           ROWVERSION       NOT NULL,
    [comercial_activity_id] UNIQUEIDENTIFIER NOT NULL,
    [enterprise_id]         UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [enterprise_comercial_activity_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [ent_com_act_comercial_activity_fk] FOREIGN KEY ([comercial_activity_id]) REFERENCES [taxation].[comercial_activity] ([id]) ON DELETE CASCADE,
    CONSTRAINT [ent_com_act_enterprise_fk] FOREIGN KEY ([enterprise_id]) REFERENCES [organization].[enterprise] ([id]) ON DELETE CASCADE,
    CONSTRAINT [enterprise_comercial_activity_un] UNIQUE NONCLUSTERED ([comercial_activity_id] ASC, [enterprise_id] ASC)
);

