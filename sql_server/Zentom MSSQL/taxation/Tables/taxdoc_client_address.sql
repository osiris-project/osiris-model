﻿CREATE TABLE [taxation].[taxdoc_client_address] (
    [id]               UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]      ROWVERSION       NOT NULL,
    [line1]            NVARCHAR (70)    NOT NULL,
    [line2]            NVARCHAR (150)   DEFAULT ('') NOT NULL,
    [commune]          NVARCHAR (20)    NOT NULL,
    [city]             NVARCHAR (15)    NOT NULL,
    [taxdoc_client_id] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [taxdoc_client_address_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [taxdoc_client_address_fk] FOREIGN KEY ([taxdoc_client_id]) REFERENCES [taxation].[taxdoc_client] ([id]) ON DELETE CASCADE,
    CONSTRAINT [taxdoc_client_address_un] UNIQUE NONCLUSTERED ([line1] ASC, [commune] ASC, [taxdoc_client_id] ASC)
);

