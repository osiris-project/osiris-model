﻿CREATE TABLE [taxation].[received_taxdoc_reference] (
    [id]                UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]       ROWVERSION       NOT NULL,
    [parent_id]         UNIQUEIDENTIFIER NOT NULL,
    [reference_id]      UNIQUEIDENTIFIER NOT NULL,
    [reference_reason]  VARCHAR (15)     NOT NULL,
    [reference_comment] NVARCHAR (90)    NOT NULL,
    [reference_date]    DATE             NULL,
    CONSTRAINT [received_taxdoc_reference_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [received_taxdoc_reference_parent_fk] FOREIGN KEY ([parent_id]) REFERENCES [taxation].[received_tax_document] ([id]),
    CONSTRAINT [received_taxdoc_reference_reference_fk] FOREIGN KEY ([reference_id]) REFERENCES [taxation].[received_tax_document] ([id]),
    CONSTRAINT [received_taxdoc_reference_un] UNIQUE NONCLUSTERED ([parent_id] ASC, [reference_id] ASC)
);

