﻿CREATE TABLE [taxation].[taxdoc_client_address_type] (
    [id]                       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]              ROWVERSION       NOT NULL,
    [address_type_id]          UNIQUEIDENTIFIER NOT NULL,
    [taxdoc_client_address_id] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [taxdoc_client_address_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [taxdoc_cliat_address_type_fk] FOREIGN KEY ([address_type_id]) REFERENCES [organization].[address_type] ([id]) ON DELETE CASCADE,
    CONSTRAINT [txdoc_cliat_taxdoc_cli_addr_fk] FOREIGN KEY ([taxdoc_client_address_id]) REFERENCES [taxation].[taxdoc_client_address] ([id]) ON DELETE CASCADE,
    CONSTRAINT [taxdoc_client_address_type_un] UNIQUE NONCLUSTERED ([address_type_id] ASC, [taxdoc_client_address_id] ASC)
);

