﻿CREATE TABLE [dbo].[received_taxdoc_weak_reference]
(
	[id]              UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL default newsequentialid(),
    [row_version]     ROWVERSION       NOT NULL,
    [document_type]   VARCHAR (6)      NOT NULL,
    [folio]           BIGINT           NOT NULL,
    [date]            DATE             NOT NULL,
    [reason]          NVARCHAR (90)    DEFAULT ('') NOT NULL,
    [received_document_id] UNIQUEIDENTIFIER NOT NULL,
	CONSTRAINT [received_document_weak_reference_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [recdoc_weak_ref_received_document_fk] FOREIGN KEY ([received_document_id]) REFERENCES [taxation].[received_tax_document] ([id]) ON DELETE CASCADE,
    CONSTRAINT [recdoc_weak_reference_un] UNIQUE NONCLUSTERED ([document_type] ASC, [folio] ASC, [received_document_id] ASC)
)
