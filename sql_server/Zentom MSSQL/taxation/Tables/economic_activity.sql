﻿CREATE TABLE [taxation].[economic_activity] (
    [id]                         UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]                ROWVERSION       NOT NULL,
    [name]                       NVARCHAR (110)   NOT NULL,
    [code]                       VARCHAR (6)      NOT NULL,
    [is_subject_to_tax]          BIT              DEFAULT (NULL) NULL,
    [tributary_category]         TINYINT          NULL,
    [economic_activity_group_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [economic_activity_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [economic_activity_un] UNIQUE NONCLUSTERED ([name] ASC, [code] ASC, [economic_activity_group_id] ASC)
);

