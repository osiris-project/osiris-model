﻿CREATE TABLE [taxation].[dispatch_type] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [code]        NVARCHAR (10)    NOT NULL,
    [description] NVARCHAR (70)    NOT NULL,
    CONSTRAINT [dispatch_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [dispatch_type_un] UNIQUE NONCLUSTERED ([code] ASC)
);

