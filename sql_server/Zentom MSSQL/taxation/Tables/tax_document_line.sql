﻿CREATE TABLE [taxation].[tax_document_line] (
    [id]               UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [tax_document_id]  UNIQUEIDENTIFIER NOT NULL,
    [row_version]      ROWVERSION       NOT NULL,
    [line_number]      SMALLINT         NOT NULL,
    [text]             NVARCHAR (80)    NOT NULL,
    [unit_price]       DECIMAL (13, 4)  DEFAULT ((0.0000)) NOT NULL,
    [quantity]         DECIMAL (13, 4)  DEFAULT ((1.0000)) NOT NULL,
    [tax_amount]       DECIMAL (13, 4)  DEFAULT ((0.0000)) NOT NULL,
    [extra_tax_amount] DECIMAL (13, 4)  DEFAULT ((0.0000)) NOT NULL,
    [line_total]       DECIMAL (13, 4)  DEFAULT ((1.0000)) NOT NULL,
    [additional_gloss] NVARCHAR (1000)  DEFAULT ('') NOT NULL,
    [measure_unit]     NVARCHAR (30)    DEFAULT ('') NOT NULL,
    [tax_type]         TINYINT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tax_document_line_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [taxdoc_line_taxdoc_fk] FOREIGN KEY ([tax_document_id]) REFERENCES [taxation].[tax_document] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tax_document_line_un] UNIQUE NONCLUSTERED ([line_number] ASC, [tax_document_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [taxdoc_line_taxdoc_idx]
    ON [taxation].[tax_document_line]([tax_document_id] ASC);

