﻿CREATE TABLE [taxation].[payment] (
    [id]              UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]     ROWVERSION       NOT NULL,
    [amount]          DECIMAL (13, 4)  NOT NULL,
    [expire_date]     DATETIME         NOT NULL,
    [payment_type_id] UNIQUEIDENTIFIER NOT NULL,
    [tax_document_id] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [payment_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [payment_payment_type_fk] FOREIGN KEY ([payment_type_id]) REFERENCES [taxation].[payment_type] ([id]),
    CONSTRAINT [payment_tax_document_fk] FOREIGN KEY ([tax_document_id]) REFERENCES [taxation].[tax_document] ([id]) ON DELETE CASCADE
);

