﻿CREATE TABLE [taxation].[specific_tax] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [name]        NVARCHAR (100)   NOT NULL,
    [code]        INT              NOT NULL,
    [is_retained] BIT              DEFAULT ((0)) NOT NULL,
    [tax_rate]    DECIMAL (4, 3)   NOT NULL,
    CONSTRAINT [specific_tax_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [specific_tax_un] UNIQUE NONCLUSTERED ([name] ASC, [code] ASC)
);

