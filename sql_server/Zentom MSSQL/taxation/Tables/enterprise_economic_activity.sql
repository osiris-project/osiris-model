﻿CREATE TABLE [taxation].[enterprise_economic_activity] (
    [id]                   UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [enterprise_id]        UNIQUEIDENTIFIER NOT NULL,
    [economic_activity_id] UNIQUEIDENTIFIER NOT NULL,
    [row_version]          ROWVERSION       NOT NULL,
    CONSTRAINT [enterprise_economic_activity_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [ent_eco_act_economic_activity_fk] FOREIGN KEY ([economic_activity_id]) REFERENCES [taxation].[economic_activity] ([id]) ON DELETE CASCADE,
    CONSTRAINT [ent_eco_act_enterprise_fk] FOREIGN KEY ([enterprise_id]) REFERENCES [organization].[enterprise] ([id]) ON DELETE CASCADE,
    CONSTRAINT [enterprise_economic_activity_un] UNIQUE NONCLUSTERED ([enterprise_id] ASC, [economic_activity_id] ASC)
);

