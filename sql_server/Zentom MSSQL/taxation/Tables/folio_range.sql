﻿CREATE TABLE [taxation].[folio_range] (
    [id]               UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]      ROWVERSION       NOT NULL,
    [document_type]    VARCHAR (3)      NOT NULL,
    [range_start]      BIGINT           NOT NULL,
    [range_end]        BIGINT           NOT NULL,
    [is_current_range] BIT              DEFAULT ((0)) NOT NULL,
    [current_folio]    BIGINT           NOT NULL,
    [unused_folios]    VARCHAR (MAX)    DEFAULT ('') NOT NULL,
    [created_date]     DATETIME         NOT NULL,
    [request_date]     DATE             NOT NULL,
    [status]           TINYINT          DEFAULT ((0)) NOT NULL,
    [tenant_id]        UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [folio_range_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [taxdoc_folio_range_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [folio_range_un] UNIQUE NONCLUSTERED ([range_start] ASC, [document_type] ASC, [tenant_id] ASC)
);

