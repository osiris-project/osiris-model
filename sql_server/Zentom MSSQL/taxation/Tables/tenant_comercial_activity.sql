﻿CREATE TABLE [taxation].[tenant_comercial_activity] (
    [id]                    UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]           ROWVERSION       NOT NULL,
    [comercial_activity_id] UNIQUEIDENTIFIER NOT NULL,
    [tenant_id]             UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [tenant_comercial_activity_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [ten_com_act_comercial_activity_fk] FOREIGN KEY ([comercial_activity_id]) REFERENCES [taxation].[comercial_activity] ([id]) ON DELETE CASCADE,
    CONSTRAINT [ten_com_act_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tenant_comercial_activity_un] UNIQUE NONCLUSTERED ([comercial_activity_id] ASC, [tenant_id] ASC)
);

