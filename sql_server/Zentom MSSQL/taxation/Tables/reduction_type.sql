﻿CREATE TABLE [taxation].[reduction_type] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [code]        TINYINT          NOT NULL,
    [description] NVARCHAR (70)    NOT NULL,
    CONSTRAINT [reduction_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [reduction_type_code_un] UNIQUE NONCLUSTERED ([code] ASC)
);

