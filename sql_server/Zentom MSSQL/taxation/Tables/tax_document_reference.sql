﻿CREATE TABLE [taxation].[tax_document_reference] (
    [id]                UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [parent_id]         UNIQUEIDENTIFIER NOT NULL,
    [reference_id]      UNIQUEIDENTIFIER NOT NULL,
    [row_version]       ROWVERSION       NOT NULL,
    [reference_reason]  VARCHAR (15)     NOT NULL,
    [reference_comment] NVARCHAR (90)    NOT NULL,
    [date]              DATE             DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [tax_document_reference_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [parent_tax_document_fk] FOREIGN KEY ([parent_id]) REFERENCES [taxation].[tax_document] ([id]),
    CONSTRAINT [referenced_tax_document_fk] FOREIGN KEY ([reference_id]) REFERENCES [taxation].[tax_document] ([id]),
    CONSTRAINT [tax_document_reference_un] UNIQUE NONCLUSTERED ([parent_id] ASC, [reference_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [taxdoc_reference_parent_idx]
    ON [taxation].[tax_document_reference]([parent_id] ASC);


GO
CREATE NONCLUSTERED INDEX [taxdoc_reference_reference_idx]
    ON [taxation].[tax_document_reference]([reference_id] ASC);

