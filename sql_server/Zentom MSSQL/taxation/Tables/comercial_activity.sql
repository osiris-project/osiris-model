﻿CREATE TABLE [taxation].[comercial_activity] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [name]        NVARCHAR (200)   NOT NULL,
    CONSTRAINT [comercial_activity_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [comercial_activity_un] UNIQUE NONCLUSTERED ([name] ASC)
);

