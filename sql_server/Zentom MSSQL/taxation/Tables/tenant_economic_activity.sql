﻿CREATE TABLE [taxation].[tenant_economic_activity] (
    [id]                   UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [tenant_id]            UNIQUEIDENTIFIER NOT NULL,
    [economic_activity_id] UNIQUEIDENTIFIER NOT NULL,
    [row_version]          ROWVERSION       NOT NULL,
    CONSTRAINT [tenant_economic_activity_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [ten_eco_act_economic_activity_fk] FOREIGN KEY ([economic_activity_id]) REFERENCES [taxation].[economic_activity] ([id]) ON DELETE CASCADE,
    CONSTRAINT [ten_eco_act_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tenant_economic_activity_un] UNIQUE NONCLUSTERED ([tenant_id] ASC, [economic_activity_id] ASC)
);

