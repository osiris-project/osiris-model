﻿CREATE TABLE [taxation].[document_type] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [code]        VARCHAR (6)      NOT NULL,
    [name]        NVARCHAR (70)    NOT NULL,
    [tax_rate]    DECIMAL (4, 3)   NOT NULL,
    CONSTRAINT [document_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [document_type_code_un] UNIQUE NONCLUSTERED ([code] ASC, [name] ASC)
);

