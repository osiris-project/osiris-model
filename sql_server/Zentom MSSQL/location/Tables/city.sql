﻿CREATE TABLE [location].[city] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [name]        NVARCHAR (100)   NOT NULL,
    [country_id]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [city_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [city_country_fk] FOREIGN KEY ([country_id]) REFERENCES [location].[country] ([id]) ON DELETE CASCADE,
    CONSTRAINT [city_un] UNIQUE NONCLUSTERED ([name] ASC, [country_id] ASC)
);

