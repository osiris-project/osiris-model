﻿CREATE TABLE [location].[commune] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [name]        NVARCHAR (20)    NOT NULL,
    [post_code]   VARCHAR (7)      DEFAULT ('') NOT NULL,
    [city_id]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [commune_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [commune_city_fk] FOREIGN KEY ([city_id]) REFERENCES [location].[city] ([id]) ON DELETE CASCADE,
    CONSTRAINT [commune_un] UNIQUE NONCLUSTERED ([name] ASC, [city_id] ASC)
);

