﻿CREATE TABLE [location].[address] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [line1]       NVARCHAR (70)    NOT NULL,
    [line2]       NVARCHAR (150)   DEFAULT ('') NOT NULL,
    [commune_id]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [address_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [address_commune_fk] FOREIGN KEY ([commune_id]) REFERENCES [location].[commune] ([id]) ON DELETE CASCADE,
    CONSTRAINT [address_un] UNIQUE NONCLUSTERED ([line1] ASC, [commune_id] ASC)
);

