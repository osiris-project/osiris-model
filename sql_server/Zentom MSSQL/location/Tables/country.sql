﻿CREATE TABLE [location].[country] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [name]        NVARCHAR (100)   NOT NULL,
    [code]        VARCHAR (2)      DEFAULT ('') NOT NULL,
    [phone_code]  VARCHAR (5)      DEFAULT ('') NOT NULL,
    CONSTRAINT [country_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [country_un] UNIQUE NONCLUSTERED ([name] ASC)
);

