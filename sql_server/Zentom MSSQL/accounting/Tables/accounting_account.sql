﻿CREATE TABLE [accounting].[accounting_account] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [code]        VARCHAR (18)     NOT NULL,
    [description] NVARCHAR (100)   DEFAULT ('') NOT NULL,
    [tenant_id]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [accounting_account_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [accounting_account_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [accounting_account_un] UNIQUE NONCLUSTERED ([code] ASC, [tenant_id] ASC)
);

