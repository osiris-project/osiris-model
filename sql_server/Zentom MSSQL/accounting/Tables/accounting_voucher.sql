﻿CREATE TABLE [accounting].[accounting_voucher] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [number]      INT              IDENTITY (1, 1) NOT NULL,
    [date]        DATE             NOT NULL,
    [description] NVARCHAR (60)    DEFAULT (NULL) NULL,
    [status]      TINYINT          DEFAULT ((0)) NOT NULL,
    [tenant_id]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [account_voucher_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [accounting_voucher_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [account_voucher_un] UNIQUE NONCLUSTERED ([date] ASC, [number] ASC, [tenant_id] ASC)
);

