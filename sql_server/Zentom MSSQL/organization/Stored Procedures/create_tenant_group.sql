﻿
create procedure organization.create_tenant_group
(
	@group_name nvarchar(100),
	@headquarters_id uniqueidentifier = null,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from organization.[group] where name = @group_name);

	if @id is null
	begin
		insert into organization.[group] (name, headquarters) values (@group_name, @headquarters_id);
		set @id = (select id from organization.[group] where name = @group_name);
	end;
end;
