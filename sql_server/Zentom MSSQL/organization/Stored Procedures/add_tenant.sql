﻿
CREATE PROCEDURE organization.add_tenant
(
	@tax_id VARCHAR(20),
	@legal_name NVARCHAR(100),
	@display_name NVARCHAR(100) = '',
	@email VARCHAR(1000),
	@phone VARCHAR(50),
	@bank_account_number VARCHAR(15) = '',
	@bill_resolution_date DATE = NULL,
	@bill_resolution_number INT = NULL,
	@is_enabled BIT = 1,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	SET @id = (SELECT id FROM organization.tenant WHERE tax_id = @tax_id);

	IF (@id IS NULL)
	BEGIN
		INSERT INTO organization.tenant (tax_id, legal_name, display_name, email, phone, bank_account_number, bill_resolution_date, bill_resolution_number, is_enabled) VALUES
		(@tax_id, @legal_name, @display_name, @email, @phone, @bank_account_number, @bill_resolution_date, @bill_resolution_number, @is_enabled);
		SET @id = (SELECT id FROM organization.tenant WHERE tax_id = @tax_id);
	END
END
