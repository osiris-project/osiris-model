﻿
create procedure organization.add_tenant_to_group
(
	@group_name nvarchar(100),
	@headquarters_id uniqueidentifier = null,
	@tenant_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	declare @group_id uniqueidentifier;
	exec organization.create_tenant_group @group_name, @headquarters_id, @group_id out;
	set @id = (select id from organization.tenant_group where tenant_id = @tenant_id and group_id = @group_id);

	if @id is null
	begin
		insert into organization.tenant_group (tenant_id, group_id) values (@tenant_id, @group_id);
		set @id = (select id from organization.tenant_group where tenant_id = @tenant_id and group_id = @group_id);
	end;
end;
