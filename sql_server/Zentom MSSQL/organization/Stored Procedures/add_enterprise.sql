﻿
CREATE PROCEDURE organization.add_enterprise
(
	@tax_id VARCHAR(20),
	@legal_name NVARCHAR(100),
	@display_name NVARCHAR(100) = '',
	@email VARCHAR(1000),
	@phone VARCHAR(50),
	@bank_account_number VARCHAR(15) = '',
	@relationship tinyint = 1,
	@tenant_id uniqueidentifier,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	SET @id = (SELECT id FROM organization.enterprise WHERE tax_id = @tax_id);

	IF (@id IS NULL)
	BEGIN
		INSERT INTO organization.enterprise (tax_id, legal_name, display_name, email, phone, bank_account_number, relationship, tenant_id) VALUES
		(@tax_id, @legal_name, @display_name, @email, @phone, @bank_account_number, @relationship, @tenant_id);
		SET @id = (SELECT id FROM organization.enterprise WHERE tax_id = @tax_id);
	END
END
