﻿
create procedure organization.add_employee
(
	@person_id uniqueidentifier,
	@tenant_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
)
as
begin
	set @id = (select id from organization.employee where tenant_id = @tenant_id and person_id = @person_id);

	if (@id is null)
	begin
		insert into organization.employee (tenant_id, person_id) values (@tenant_id, @person_id);
		set @id = (select id from organization.employee where tenant_id = @tenant_id and person_id = @person_id);
	end;
end;
