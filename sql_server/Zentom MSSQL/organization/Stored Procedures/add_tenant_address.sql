﻿
CREATE PROCEDURE organization.add_tenant_address
(
	@tenant_id UNIQUEIDENTIFIER,
	@line1 nvarchar(75),
	@line2 nvarchar(150) = '',
	@commune_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	declare @op table ( id uniqueidentifier );
	set @id = (select id from organization.tenant_address where tenant_id = @tenant_id and line1 = @line1 and commune_id = @commune_id);

	if @id is null
	begin
		insert into organization.tenant_address (line1, line2, tenant_id, commune_id) output inserted.id into @op values
		(@line1, @line2, @tenant_id, @commune_id);
		set @id = (select id from @op);
	end;
END
