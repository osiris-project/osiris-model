﻿
CREATE PROCEDURE organization.enable_tenant (@tax_id VARCHAR(20))
AS
BEGIN
	UPDATE organization.tenant SET is_enabled = 1 WHERE tax_id = @tax_id;
END;
