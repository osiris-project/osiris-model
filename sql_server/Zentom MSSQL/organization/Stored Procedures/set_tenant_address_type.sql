﻿
create procedure organization.set_tenant_address_type
(
	@address_role_id uniqueidentifier,
	@tenant_address_id uniqueidentifier,
	@id uniqueidentifier = null out
)
as
begin
	set @id = (select id from organization.tenant_address_type where tenant_address_id = @tenant_address_id and address_type_id = @address_role_id);

	if (@id is null)
	begin
		insert into organization.tenant_address_type (tenant_address_id, address_type_id) values (@tenant_address_id, @address_role_id);
		set @id = (select id from organization.tenant_address_type where tenant_address_id = @tenant_address_id and address_type_id = @address_role_id);
	end
end;
