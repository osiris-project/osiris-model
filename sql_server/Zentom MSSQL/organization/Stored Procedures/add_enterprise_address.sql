﻿
CREATE PROCEDURE organization.add_enterprise_address
(
	@enterprise_id UNIQUEIDENTIFIER,
	@line1 nvarchar(70) = '',
	@line2 nvarchar(150),
	@commune_id uniqueidentifier,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	declare @op table ( id uniqueidentifier );
	set @id = (select id from organization.enterprise_address where enterprise_id = @enterprise_id and line1 = @line1 and commune_id = @commune_id);

	if @id is null
	begin
		insert into organization.enterprise_address (line1, line2, enterprise_id, commune_id) output inserted.id into @op values
		(@line1, @line2, @enterprise_id, @commune_id);
		set @id = (select id from @op);
	end;
END
