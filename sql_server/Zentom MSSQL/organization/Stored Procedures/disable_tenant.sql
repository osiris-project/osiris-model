﻿
CREATE PROCEDURE organization.disable_tenant (@tax_id VARCHAR(20))
AS
BEGIN
	UPDATE organization.tenant SET is_enabled = 0 WHERE tax_id = @tax_id;
END;
