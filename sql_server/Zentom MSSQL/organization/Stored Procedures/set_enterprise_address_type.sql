﻿
create procedure organization.set_enterprise_address_type
(
	@address_role_id uniqueidentifier,
	@enterprise_address_id uniqueidentifier,
	@id uniqueidentifier = null out
)
as
begin
	set @id = (select id from organization.enterprise_address_type where enterprise_address_id = @enterprise_address_id and address_type_id = @address_role_id);

	if (@id is null)
	begin
		insert into organization.enterprise_address_type (enterprise_address_id, address_type_id) values (@enterprise_address_id, @address_role_id);
		set @id = (select id from organization.enterprise_address_type where enterprise_address_id = @enterprise_address_id and address_type_id = @address_role_id);
	end
end;
