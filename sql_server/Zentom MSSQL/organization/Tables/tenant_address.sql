﻿CREATE TABLE [organization].[tenant_address] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [line1]       NVARCHAR (70)    NOT NULL,
    [line2]       NVARCHAR (150)   DEFAULT ('') NOT NULL,
    [commune_id]  UNIQUEIDENTIFIER NOT NULL,
    [tenant_id]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [tenant_address_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [tenant_address_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tenant_address_un] UNIQUE NONCLUSTERED ([line1] ASC, [commune_id] ASC, [tenant_id] ASC)
);

