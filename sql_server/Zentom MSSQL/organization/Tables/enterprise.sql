﻿CREATE TABLE [organization].[enterprise] (
    [id]                  UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]         ROWVERSION       NOT NULL,
    [tax_id]              VARCHAR (20)     NOT NULL,
    [legal_name]          NVARCHAR (100)   NOT NULL,
    [display_name]        NVARCHAR (100)   DEFAULT ('') NOT NULL,
    [email]               VARCHAR (1000)   NOT NULL,
    [phone]               VARCHAR (50)     NOT NULL,
    [bank_account_number] VARCHAR (15)     DEFAULT ('') NOT NULL,
    [relationship]        TINYINT          DEFAULT ((1)) NOT NULL,
    [tenant_id]           UNIQUEIDENTIFIER NOT NULL,
    [costs_center_id]     UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    CONSTRAINT [enterprise_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [enterprise_costs_center_fk] FOREIGN KEY ([costs_center_id]) REFERENCES [organization].[costs_center] ([id]) ON DELETE SET NULL,
    CONSTRAINT [enterprise_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]),
    CONSTRAINT [enterprise_legal_name_un] UNIQUE NONCLUSTERED ([tenant_id] ASC, [legal_name] ASC),
    CONSTRAINT [enterprise_tax_id_un] UNIQUE NONCLUSTERED ([tenant_id] ASC, [tax_id] ASC)
);



