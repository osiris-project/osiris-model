﻿CREATE TABLE [organization].[product] (
    [id]              UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]     ROWVERSION       NOT NULL,
    [name]            NVARCHAR (100)   NOT NULL,
    [description]     NVARCHAR (140)   DEFAULT ('') NOT NULL,
    [unit_price]      DECIMAL (13, 4)  NOT NULL,
    [is_discontinued] BIT              DEFAULT ((0)) NOT NULL,
    [tenant_id]       UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [product_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [product_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]),
    CONSTRAINT [product_un] UNIQUE NONCLUSTERED ([name] ASC, [tenant_id] ASC)
);

