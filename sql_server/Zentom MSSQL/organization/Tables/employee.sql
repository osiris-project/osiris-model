﻿CREATE TABLE [organization].[employee] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [person_id]   UNIQUEIDENTIFIER NOT NULL,
    [tenant_id]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [employee_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [employee_employer_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]),
    CONSTRAINT [employee_person_fk] FOREIGN KEY ([person_id]) REFERENCES [system].[person] ([id]),
    CONSTRAINT [employee_un] UNIQUE NONCLUSTERED ([tenant_id] ASC, [person_id] ASC)
);

