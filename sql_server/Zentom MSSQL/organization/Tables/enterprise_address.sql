﻿CREATE TABLE [organization].[enterprise_address] (
    [id]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]   ROWVERSION       NOT NULL,
    [line1]         NVARCHAR (70)    NOT NULL,
    [line2]         NVARCHAR (150)   DEFAULT ('') NOT NULL,
    [commune_id]    UNIQUEIDENTIFIER NOT NULL,
    [enterprise_id] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [enterprise_address_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [enterprise_address_enterprise_fk] FOREIGN KEY ([enterprise_id]) REFERENCES [organization].[enterprise] ([id]) ON DELETE CASCADE,
    CONSTRAINT [enterprise_address_un] UNIQUE NONCLUSTERED ([line1] ASC, [commune_id] ASC, [enterprise_id] ASC)
);

