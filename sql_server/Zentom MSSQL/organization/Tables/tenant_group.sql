﻿CREATE TABLE [organization].[tenant_group] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [group_id]    UNIQUEIDENTIFIER NOT NULL,
    [tenant_id]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [tenant_group_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [tenant_group_un] UNIQUE NONCLUSTERED ([tenant_id] ASC, [group_id] ASC)
);

