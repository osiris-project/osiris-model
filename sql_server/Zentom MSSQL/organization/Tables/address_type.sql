﻿CREATE TABLE [organization].[address_type] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [name]        NVARCHAR (100)   NOT NULL,
    [description] NVARCHAR (1000)  DEFAULT ('') NOT NULL,
    CONSTRAINT [address_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [address_type_un] UNIQUE NONCLUSTERED ([name] ASC)
);

