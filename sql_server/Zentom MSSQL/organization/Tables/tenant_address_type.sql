﻿CREATE TABLE [organization].[tenant_address_type] (
    [id]                UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]       ROWVERSION       NOT NULL,
    [tenant_address_id] UNIQUEIDENTIFIER NOT NULL,
    [address_type_id]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [tenant_address_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [tnt_addr_type_addr_fk] FOREIGN KEY ([address_type_id]) REFERENCES [organization].[address_type] ([id]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [tnt_addr_type_tnt_addr_fk] FOREIGN KEY ([tenant_address_id]) REFERENCES [organization].[tenant_address] ([id]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [tenant_address_type_un] UNIQUE NONCLUSTERED ([tenant_address_id] ASC, [address_type_id] ASC)
);

