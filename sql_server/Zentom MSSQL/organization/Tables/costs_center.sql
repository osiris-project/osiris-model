﻿CREATE TABLE [organization].[costs_center] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [code]        VARCHAR (24)     NOT NULL,
    [name]        NVARCHAR (100)   NOT NULL,
    [description] NVARCHAR (MAX)   DEFAULT ('') NOT NULL,
    [tenant_id]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [costs_center_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [costs_center_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]),
    CONSTRAINT [costs_center_un] UNIQUE NONCLUSTERED ([code] ASC, [name] ASC, [tenant_id] ASC)
);



