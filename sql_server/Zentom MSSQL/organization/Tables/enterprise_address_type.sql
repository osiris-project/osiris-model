﻿CREATE TABLE [organization].[enterprise_address_type] (
    [id]                    UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]           ROWVERSION       NOT NULL,
    [enterprise_address_id] UNIQUEIDENTIFIER NOT NULL,
    [address_type_id]       UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [enterprise_address_type_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [ent_addr_type_addr_fk] FOREIGN KEY ([address_type_id]) REFERENCES [organization].[address_type] ([id]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [ent_addr_type_ent_addr_fk] FOREIGN KEY ([enterprise_address_id]) REFERENCES [organization].[enterprise_address] ([id]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [enterprise_address_type_un] UNIQUE NONCLUSTERED ([enterprise_address_id] ASC, [address_type_id] ASC)
);

