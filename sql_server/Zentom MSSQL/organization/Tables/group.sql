﻿CREATE TABLE [organization].[group] (
    [id]           UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]  ROWVERSION       NOT NULL,
    [name]         NVARCHAR (200)   NOT NULL,
    [headquarters] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [group_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [group_un] UNIQUE NONCLUSTERED ([name] ASC)
);

