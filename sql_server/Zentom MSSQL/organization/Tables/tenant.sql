﻿CREATE TABLE [organization].[tenant] (
    [id]                     UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]            ROWVERSION       NOT NULL,
    [tax_id]                 VARCHAR (20)     NOT NULL,
    [legal_name]             NVARCHAR (100)   NOT NULL,
    [display_name]           NVARCHAR (100)   DEFAULT ('') NOT NULL,
    [email]                  VARCHAR (1000)   NOT NULL,
    [phone]                  VARCHAR (50)     NOT NULL,
    [bill_resolution_date]   DATE             NULL,
    [bill_resolution_number] INT              NULL,
    [bank_account_number]    VARCHAR (15)     DEFAULT ('') NOT NULL,
    [is_enabled]             BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [tenant_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [tenant_un] UNIQUE NONCLUSTERED ([legal_name] ASC, [tax_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [tenant_idx]
    ON [organization].[tenant]([tax_id] ASC);

