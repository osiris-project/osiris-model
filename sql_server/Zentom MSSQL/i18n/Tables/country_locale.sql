﻿CREATE TABLE [i18n].[country_locale] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [country_id]  UNIQUEIDENTIFIER NOT NULL,
    [locale_id]   UNIQUEIDENTIFIER NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    CONSTRAINT [country_locale_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [country_locale_country_fk] FOREIGN KEY ([country_id]) REFERENCES [location].[country] ([id]) ON DELETE CASCADE,
    CONSTRAINT [country_locale_locale_fk] FOREIGN KEY ([locale_id]) REFERENCES [i18n].[locale] ([id]) ON DELETE CASCADE,
    CONSTRAINT [country_locale_un] UNIQUE NONCLUSTERED ([country_id] ASC, [locale_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [country_locale_country_idx]
    ON [i18n].[country_locale]([country_id] ASC);


GO
CREATE NONCLUSTERED INDEX [country_locale_locale_idx]
    ON [i18n].[country_locale]([locale_id] ASC);

