﻿CREATE TABLE [i18n].[locale] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [code]        VARCHAR (6)      NOT NULL,
    [full_name]   VARCHAR (50)     DEFAULT ('') NOT NULL,
    CONSTRAINT [locale_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [locale_un] UNIQUE NONCLUSTERED ([code] ASC)
);

