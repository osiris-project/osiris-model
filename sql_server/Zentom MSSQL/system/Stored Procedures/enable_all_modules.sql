﻿
create procedure system.enable_all_modules
(
	@tenant_id uniqueidentifier
)
as
begin
	declare module_cursor cursor
		local static read_only forward_only
	for
		select id from system.module;

	declare @insert_count int = 0;
	
	declare @module_id uniqueidentifier;
	declare @id uniqueidentifier;

	open module_cursor;
	declare @row_count int = @@cursor_rows;
	fetch next from module_cursor into @module_id;
	while @@fetch_status = 0
	begin
		set @id = (select id from system.tenant_module where module_id = @module_id and tenant_id = @tenant_id);

		if(@id is null)
		begin
			insert into system.tenant_module (tenant_id, module_id, settings, [version], is_enabled) values
			(@tenant_id, @module_id, '', '1.0.0', 1);
			set @insert_count = @insert_count + 1;
		end;
		fetch next from module_cursor into @module_id;
	end;
	close module_cursor;
	deallocate module_cursor;

	print N'Inserted rows: ' + (cast(@insert_count as varchar(20)));
	print N'Existing modules: ' + (cast(@row_count as varchar(20)));
end;