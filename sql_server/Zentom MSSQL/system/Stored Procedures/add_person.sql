﻿
create procedure system.add_person
(
	@first_name NVARCHAR(100),
	@last_name NVARCHAR(100),
	@national_id NVARCHAR(20),
	@birth_date DATE = NULL,
	@gender tinyint = 0,
	@id uniqueidentifier = null out
)
as
begin
	set @id = (select id from system.person where national_id = @national_id);

	if @id is null
	begin
		if @gender is null
			set @gender = 0;

		insert into system.person (first_name, last_name, national_id, birth_date, gender) values (@first_name, @last_name, @national_id, @birth_date, @gender);
		set @id = (select id from system.person where national_id = @national_id);
	end;
end;
