﻿
CREATE PROCEDURE [system].set_all_permissions_to_role(@role_id UNIQUEIDENTIFIER)
AS
BEGIN
	DECLARE permission_cursor CURSOR
		LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR
		SELECT id FROM [system].permission;

	DECLARE @insert_count INT = 0;
	DECLARE @row_count INT = @@CURSOR_ROWS;
	DECLARE @permission_id UNIQUEIDENTIFIER;
	DECLARE @id UNIQUEIDENTIFIER;

	OPEN permission_cursor;
	FETCH NEXT FROM permission_cursor INTO @permission_id;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		 SET @id = (SELECT id FROM [system].role_permission WHERE permission_id = @permission_id AND role_id = @role_id);

		 IF (@id IS NULL)
			 BEGIN
				INSERT INTO [system].role_permission (role_id, permission_id) VALUES (@role_id, @permission_id);
				SET @insert_count = @insert_count + 1;
			 END
		 ELSE

		FETCH NEXT FROM permission_cursor INTO @permission_id;
	END
	CLOSE permission_cursor;
	DEALLOCATE permission_cursor;

	PRINT 'Inserted rows: ' + (CAST(@insert_count AS VARCHAR(20)));
	PRINT 'Existing permission count: ' + (CAST(@row_count AS VARCHAR(20)));
END;
