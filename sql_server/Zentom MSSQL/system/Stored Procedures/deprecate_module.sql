﻿
create procedure system.deprecate_module
(
	@module_id uniqueidentifier = null,
	@name varchar(100) = null
) as 
begin
	if (@module_id is not null)
	begin
		update system.module set is_deprecated = 1 where id = @module_id;
	end;
	else if (@name is not null)
	begin
		begin try
			set @module_id = (select id from system.module where name = @name);
			exec system.deprecate_module @module_id, null;
		end try
		begin catch
		end catch;
	end;
end;
