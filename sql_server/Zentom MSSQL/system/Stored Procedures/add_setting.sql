﻿
CREATE PROCEDURE [system].add_setting (
	@setting_name VARCHAR(255),
	@setting_value NVARCHAR(MAX),
	@tenant_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
) AS
BEGIN
	SET @id = (SELECT id FROM [system].tenant_settings WHERE tenant_id = @tenant_id AND setting_name = @setting_name)

	IF (@id IS NULL)
	BEGIN
		INSERT INTO [system].tenant_settings (setting_name, setting_value, tenant_id) VALUES
		(@setting_name, @setting_value, @tenant_id);
		SET @id = (SELECT id FROM [system].tenant_settings WHERE tenant_id = @tenant_id AND setting_name = @setting_name)
	END
END
