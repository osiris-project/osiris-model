﻿
CREATE PROCEDURE [system].add_account_role
(
	@authorized_account_id UNIQUEIDENTIFIER,
	@role_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	SET @id = (SELECT id FROM [system].account_role WHERE authorized_account_id = @authorized_account_id AND role_id = @role_id);

	IF (@id IS NULL)
	BEGIN
		INSERT INTO [system].account_role (authorized_account_id, role_id) VALUES (@authorized_account_id, @role_id);
		SET @id = (SELECT id FROM [system].account_role WHERE authorized_account_id = @authorized_account_id AND role_id = @role_id);
	END
END;
