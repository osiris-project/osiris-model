﻿
CREATE PROCEDURE [system].add_role
(
	@name VARCHAR(100),
	@display_name VARCHAR(100),
	@description VARCHAR(MAX) = '',
	@tenant_id UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER = NULL OUT
)
AS
BEGIN
	SET @id = (SELECT id FROM [system].role WHERE name = @name AND tenant_id = @tenant_id);

	IF (@id IS NULL)
	BEGIN
		INSERT INTO [system].[role] (name, display_name, [description], tenant_id) VALUES (@name, @display_name, @description, @tenant_id);
		SET @id = (SELECT id FROM [system].role WHERE name = @name AND tenant_id = @tenant_id);
	END
END;
