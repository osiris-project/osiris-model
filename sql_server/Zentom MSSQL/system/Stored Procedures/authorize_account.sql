﻿
create procedure system.authorize_account
(
	@tenant_id uniqueidentifier,
	@account_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from system.authorized_account where tenant_id = @tenant_id and account_id = @account_id);

	if @id is null
	begin
		insert into system.authorized_account (tenant_id, account_id) values (@tenant_id, @account_id);
		set @id = (select id from system.authorized_account where tenant_id = @tenant_id and account_id = @account_id);
	end;
end;
