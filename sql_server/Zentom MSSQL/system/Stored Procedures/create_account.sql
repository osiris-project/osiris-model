﻿
create procedure system.create_account
(
	@login nvarchar(100),
	@email varchar(1000) = null,
	@password binary(128),
	@salt binary(500),
	@person_id uniqueidentifier,
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from system.account where id = @person_id);

	if @id is null
	begin
		insert into system.account ([login], email, [password], salt, id) values (@login, @email, @password, @salt, @person_id);
		set @id = (select id from system.account where id = @person_id);
	end;
end;
