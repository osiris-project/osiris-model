﻿
create procedure system.add_tenant_module
(
	@module_id uniqueidentifier,
	@tenant_id uniqueidentifier,
	@is_enabled bit = 1,
	@settings nvarchar(max) = '',
	@version varchar(100) = '1.0.0',
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from system.tenant_module where tenant_id = @tenant_id and module_id = @module_id);

	if (@id is null)
	begin
		insert into system.tenant_module (tenant_id, module_id, is_enabled, settings, [version]) values (@tenant_id, @module_id, @is_enabled, @settings, @version);
		set @id = (select id from system.tenant_module where tenant_id = @tenant_id and module_id = @module_id);
	end;
end;
