﻿
create procedure [system].create_permission
(
	@name varchar(100),
	@desc nvarchar(300),
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from [system].permission where name = @name);

	if (@id is null)
	begin
		insert into [system].permission (name, [description]) values (@name, @desc);
		set @id = (select id from [system].permission where name = @name);
	end;
end;
