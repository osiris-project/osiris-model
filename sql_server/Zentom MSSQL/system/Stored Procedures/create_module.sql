﻿
create procedure system.create_module
(
	@name varchar(100),
	@description nvarchar(250),
	@id uniqueidentifier = null out
) as
begin
	set @id = (select id from system.module where name = @name);

	if (@id is null)
	begin
		insert into system.module (name, [description]) values (@name, @description);
		set @id = (select id from system.module where name = @name);
	end;
end;
