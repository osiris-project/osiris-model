﻿CREATE TABLE [system].[tax_document_template] (
    [id]            UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [row_version]   ROWVERSION       NOT NULL,
    [name]          NVARCHAR (255)   NOT NULL,
    [document_type] NVARCHAR (20)    NOT NULL,
    [file_path]     VARCHAR (MAX)    NOT NULL,
    [created_at]    DATETIME         NOT NULL,
    [account_id]    UNIQUEIDENTIFIER NOT NULL,
    [tenant_id]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [template_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [tax_document_template_account_fk] FOREIGN KEY ([account_id]) REFERENCES [system].[account] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tax_document_template_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [template_un] UNIQUE NONCLUSTERED ([name] ASC, [account_id] ASC, [tenant_id] ASC)
);

