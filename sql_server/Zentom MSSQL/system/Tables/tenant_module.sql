﻿CREATE TABLE [system].[tenant_module] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [module_id]   UNIQUEIDENTIFIER NOT NULL,
    [tenant_id]   UNIQUEIDENTIFIER NOT NULL,
    [is_enabled]  BIT              DEFAULT ((0)) NOT NULL,
    [settings]    NVARCHAR (MAX)   DEFAULT ('') NOT NULL,
    [version]     VARCHAR (100)    DEFAULT ('1.0.0') NOT NULL,
    CONSTRAINT [tenant_module_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [tenant_module_module_fk] FOREIGN KEY ([module_id]) REFERENCES [system].[module] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tenant_module_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tenant_module_un] UNIQUE NONCLUSTERED ([module_id] ASC, [tenant_id] ASC)
);

