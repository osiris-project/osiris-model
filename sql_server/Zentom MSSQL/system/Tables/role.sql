﻿CREATE TABLE [system].[role] (
    [id]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]    ROWVERSION       NOT NULL,
    [name]           NVARCHAR (100)   NOT NULL,
    [display_name]   NVARCHAR (100)   NOT NULL,
    [description]    NVARCHAR (300)   DEFAULT ('') NOT NULL,
    [parent_role_id] UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    [tenant_id]      UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [role_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [role_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]),
    CONSTRAINT [role_un] UNIQUE NONCLUSTERED ([tenant_id] ASC, [name] ASC)
);

