﻿CREATE TABLE [system].[role_permission] (
    [id]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [role_id]       UNIQUEIDENTIFIER NOT NULL,
    [permission_id] UNIQUEIDENTIFIER NOT NULL,
    [row_version]   ROWVERSION       NOT NULL,
    CONSTRAINT [role_permission_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [role_permission_permission_fk] FOREIGN KEY ([permission_id]) REFERENCES [system].[permission] ([id]) ON DELETE CASCADE,
    CONSTRAINT [role_permission_role_fk] FOREIGN KEY ([role_id]) REFERENCES [system].[role] ([id]) ON DELETE CASCADE,
    CONSTRAINT [role_permission_un] UNIQUE NONCLUSTERED ([role_id] ASC, [permission_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [role_permission_role_idx]
    ON [system].[role_permission]([role_id] ASC);


GO
CREATE NONCLUSTERED INDEX [role_permission_permission_idx]
    ON [system].[role_permission]([permission_id] ASC);

