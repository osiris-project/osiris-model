﻿CREATE TABLE [system].[permission] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [name]        NVARCHAR (100)   NOT NULL,
    [description] NVARCHAR (300)   DEFAULT ('') NOT NULL,
    CONSTRAINT [permission_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [permission_name_un] UNIQUE NONCLUSTERED ([name] ASC)
);

