﻿CREATE TABLE [system].[module] (
    [id]               UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]      ROWVERSION       NOT NULL,
    [name]             VARCHAR (100)    NOT NULL,
    [description]      NVARCHAR (250)   DEFAULT ('') NOT NULL,
    [is_deprecated]    BIT              DEFAULT ((0)) NOT NULL,
    [default_settings] NVARCHAR (MAX)   DEFAULT ('') NOT NULL,
    CONSTRAINT [module_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [module_un] UNIQUE NONCLUSTERED ([name] ASC)
);

