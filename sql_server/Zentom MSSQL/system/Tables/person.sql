﻿CREATE TABLE [system].[person] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [first_name]  NVARCHAR (100)   NOT NULL,
    [last_name]   NVARCHAR (100)   NOT NULL,
    [national_id] VARCHAR (20)     NOT NULL,
    [birth_date]  DATE             DEFAULT (NULL) NULL,
    [gender]      TINYINT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [person_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [person_un] UNIQUE NONCLUSTERED ([national_id] ASC)
);

