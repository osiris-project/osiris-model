﻿CREATE TABLE [system].[tenant_settings] (
    [id]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version]   ROWVERSION       NOT NULL,
    [setting_name]  VARCHAR (255)    NOT NULL,
    [setting_value] NVARCHAR (MAX)   NOT NULL,
    [tenant_id]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [tenant_settings_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [tenant_settings_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]) ON DELETE CASCADE,
    CONSTRAINT [tenant_settings_un] UNIQUE NONCLUSTERED ([tenant_id] ASC, [setting_name] ASC)
);

