﻿CREATE TABLE [system].[account] (
    [id]          UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [login]       NVARCHAR (100)   NOT NULL,
    [email]       VARCHAR (1000)   NOT NULL,
    [password]    BINARY (128)     NOT NULL,
    [salt]        BINARY (500)     NOT NULL,
    CONSTRAINT [account_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [account_person_fk] FOREIGN KEY ([id]) REFERENCES [system].[person] ([id]) ON DELETE CASCADE,
    CONSTRAINT [account_un] UNIQUE NONCLUSTERED ([login] ASC)
);

