﻿CREATE TABLE [system].[password_request] (
    [id]                UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [row_version]       ROWVERSION       NOT NULL,
    [reset_expire_date] DATETIME         NULL,
    [request_date]      DATETIME         NOT NULL,
    [ipv4]              VARCHAR (12)     NOT NULL,
    [ipv6]              VARCHAR (39)     NULL,
    [account_id]        UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [password_reset_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [password_request_account_fk] FOREIGN KEY ([account_id]) REFERENCES [system].[account] ([id]) ON DELETE CASCADE
);

