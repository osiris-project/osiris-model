﻿CREATE TABLE [system].[authorized_account] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [row_version] ROWVERSION       NOT NULL,
    [tenant_id]   UNIQUEIDENTIFIER NOT NULL,
    [account_id]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [authorized_account_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [authorized_account_account_fk] FOREIGN KEY ([account_id]) REFERENCES [system].[account] ([id]),
    CONSTRAINT [authorized_account_tenant_fk] FOREIGN KEY ([tenant_id]) REFERENCES [organization].[tenant] ([id]),
    CONSTRAINT [authorized_account_un] UNIQUE NONCLUSTERED ([tenant_id] ASC, [account_id] ASC)
);

