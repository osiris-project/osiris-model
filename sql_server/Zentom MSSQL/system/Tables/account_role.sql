﻿CREATE TABLE [system].[account_role] (
    [id]                    UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [authorized_account_id] UNIQUEIDENTIFIER NOT NULL,
    [role_id]               UNIQUEIDENTIFIER NOT NULL,
    [row_version]           ROWVERSION       NOT NULL,
    CONSTRAINT [account_role_pk] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [account_role_account_fk] FOREIGN KEY ([authorized_account_id]) REFERENCES [system].[authorized_account] ([id]) ON DELETE CASCADE,
    CONSTRAINT [account_role_role_fk] FOREIGN KEY ([role_id]) REFERENCES [system].[role] ([id]) ON DELETE CASCADE,
    CONSTRAINT [account_role_un] UNIQUE NONCLUSTERED ([authorized_account_id] ASC, [role_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [account_role_account_idx]
    ON [system].[account_role]([authorized_account_id] ASC);


GO
CREATE NONCLUSTERED INDEX [account_role_role_idx]
    ON [system].[account_role]([role_id] ASC);

