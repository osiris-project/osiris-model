﻿create view system.module_view as
select
	tm.id,
	tm.settings,
	t.legal_name,
	m.name
from system.tenant_module tm
left join organization.tenant t on t.id = tm.tenant_id
left join system.module m on tm.module_id = m.id;